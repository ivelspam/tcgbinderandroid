package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Location.LocationUtils;

public class AllInfo_getAllInfo extends APIRequest{
    static private String TAG = "##AllInfo_getAllInfo";

    public class Param {
        static final String
                            deviceID = MongoKeys.deviceID,
                            deviceType = MongoKeys.deviceType,
                            FCMToken = MongoKeys.FCMToken,
                            latitude = MongoKeys.latitude,
                            longitude = MongoKeys.longitude;
    }

    public class Response {
        public static final String userFound = "userFound",
                            userNotFound = "userNotFound";
    }

    public class ResponseFields {

    }

    public AllInfo_getAllInfo(Context context){
        Location location = LocationUtils.getLastKnownLocation(context);

//        keyValues.put(Param.deviceID, MyFirebaseInstanceIDService.getAndroid_id(context));
//        keyValues.put(Param.deviceType, "android");
//        keyValues.put(Param.FCMToken, MyFirebaseInstanceIDService.getToken());
        keyValues.put(Param.latitude, location.getLatitude());
        keyValues.put(Param.longitude, location.getLongitude());

        APIUrl = APIValues.Links.allInfo_getAllInfo;
    }

}
