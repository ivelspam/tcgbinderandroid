package com.tcgbinder.app.model.SQLiteTable.SQLiteTables;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import com.tcgbinder.app.controller.BuildCardDatabaseInterface;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.SQLiteType;
import com.tcgbinder.app.model.Extensions.ExtensionsHack;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class CardInfoTable {
    static private String TAG = "##CardInfoTable";

    private static CardInfoTable sCardsTable;

    public static final String TABLE_NAME = "cardinfo",
            KEY_colors = MongoKeys.colors,
            KEY_foil = MongoKeys.foil,
            KEY_id = MongoKeys._id,
            KEY_eur = MongoKeys.eur,
            KEY_image_normal = "image_normal",
            KEY_name = MongoKeys.name,
            KEY_nonfoil = MongoKeys.nonfoil,
            KEY_price_updated_at = "price_updated_at",
            KEY_scryfall_uri = "scryfall_uri",
            KEY_set_code = MongoKeys.set_code,
            KEY_set_name = MongoKeys.set_name,
            KEY_usd = MongoKeys.usd;

    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                        KEY_colors + " TEXT, " +
                        KEY_foil + " Boolean, " +
                        KEY_id + " TEXT PRIMARY KEY, " +
                        KEY_eur + " NUMBER, " +
                        KEY_image_normal + " TEXT, " +
                        KEY_name + " TEXT, " +
                        KEY_nonfoil + " INTEGER, " +
                        KEY_price_updated_at + " DATETZ, " +
                        KEY_scryfall_uri + " TEXT, " +
                        KEY_set_code + " INTEGER, " +
                        KEY_set_name + " TEXT, " +
                        KEY_usd + " NUMBER " +
                    ")";

    SQLiteDatabase mDatabase;

    private CardInfoTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static CardInfoTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sCardsTable == null) {
            sCardsTable = new CardInfoTable(sqLiteDatabase);
        }
        return sCardsTable;
    }

    public void rebuildTable() {
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable();
    }

    public void createTable() {
        mDatabase.execSQL(TABLE_CREATION);
    }

    //INSERT ++++++++++++++++++++++++++++++++++++++++++++++++++++



    //SELECT ++++++++++++++++++++++++++++++++++++++++++++++++++++
    public List<CardInfo> selectByNameToken(String token){



        List<CardInfo> cardsInfo = new ArrayList<>();

//        String query = "SELECT " + TABLE_NAME + ".*, ClientUpdatedAt, foilQuantity, nonFoilQuantity FROM " + TABLE_NAME  +
//                leftJoin(MyCardsTable.TABLE_NAME, KEY_id, MyCardsTable.KEY_id) +
//                " WHERE " + getColumn(KEY_name) + " LIKE " +"'" + token + "%' LIMIT 30";



        String preparedStatementString =  "SELECT " + TABLE_NAME + ".*, ClientUpdatedAt, foilQuantity, nonFoilQuantity FROM " + TABLE_NAME  +
                leftJoin(MyCardsTable.TABLE_NAME, KEY_id, MyCardsTable.KEY_id) +
                " WHERE " + getColumn(KEY_name) + " LIKE ? LIMIT 30";

        Cursor cursor = mDatabase.rawQuery(preparedStatementString, new String[]{token + "%"});
//        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " LIMIT 10", null);

        while (cursor.moveToNext()) {
            cardsInfo.add(new CardInfo(cursor));
        }
        cursor.close();
        return cardsInfo;
    }

    public CardInfo selectCardInfoBy_id(String _id){

        List<CardInfo> cardsInfo = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_id + "='" + _id + "'";

        Cursor cursor = mDatabase.rawQuery(query, null);

        CardInfo cardInfo = null;
        
        
        while (cursor.moveToNext()){
            cardInfo =  new CardInfo(cursor);
        }

        cursor.close();
        return cardInfo;
    }

    public List<CardInfo> selectByNameTokenWanted(String token){

        List<CardInfo> cardsInfo = new ArrayList<>();

        String query = "SELECT " + TABLE_NAME + ".*, ClientUpdatedAt, foilQuantity, nonFoilQuantity " + " FROM " + TABLE_NAME  +
                leftJoin(MyWantedTable.TABLE_NAME, KEY_id, MyWantedTable.KEY_id) +
                " WHERE " + getColumn(KEY_name) + " LIKE ? LIMIT 40";

        Cursor cursor = mDatabase.rawQuery(query, new String[]{token + "%"});

        while (cursor.moveToNext()) {
            cardsInfo.add(new CardInfo(cursor));
        }
        cursor.close();
        return cardsInfo;
    }


    public List<WantedCard> selectMyWantedGeneralByToken(String token){

        List<WantedCard> wantedCards = new ArrayList<>();
        String query = "SELECT " + TABLE_NAME + ".*, ClientUpdatedAt, foilQuantity, nonFoilQuantity " + " FROM " + TABLE_NAME  +
                leftJoin(MyWantedGeneralTable.TABLE_NAME, KEY_name, MyWantedGeneralTable.KEY_name) +
                " WHERE " + getColumn(KEY_name) + " LIKE ? " +
                " GROUP BY " + getColumn(KEY_name);

        Cursor cursor = mDatabase.rawQuery(query, new String[]{token + "%"});

        while (cursor.moveToNext()) {
            wantedCards.add(new WantedCard(cursor));
        }
        cursor.close();
        return wantedCards;
    }

    public void readFilesTable(Context context, BuildCardDatabaseInterface buildCardDatabaseInterface) {
        rebuildTable();

        int divisions = 1000;
        int index = 0;
        int temp = 0;
        try {

            InputStream is = null;
            try {
                is = context.getAssets().open("cards.json");
                JsonReader reader = new JsonReader (new InputStreamReader(is, "UTF-8"));

                reader.beginArray();

                while (reader.hasNext()) {

                    String colors = "";
                    int foil = 0;
                    String _id = "";
                    String image_normal = "";
                    String name = "";
                    int nonfoil = 0;
                    String set_code = "";
                    String set_name = "";
                    String scryfall_uri = "";

                    reader.beginObject();
                    while(reader.hasNext()){
                        String key = reader.nextName();
                        if (key.equals("id")) {
                            _id = reader.nextString();
                        } else if (key.equals("name")) {
                            name = reader.nextString();
                        }else if (key.equals("colors")) {
                            reader.beginArray();
                            StringBuilder sb = new StringBuilder("");
                            while(reader.hasNext()){
                                sb.append(reader.nextString());
                            }
                            colors = sb.toString();

                            reader.endArray();
                        }else if (key.equals("foil")) {
                            if(reader.nextBoolean()){
                                foil = 1;
                            }
                        }else if (key.equals("nonfoil")) {
                            if(reader.nextBoolean()){
                                nonfoil = 1;
                            }
                        }else if (key.equals("set")) {
                            set_code = reader.nextString();
                        }else if (key.equals("set_name")) {
                            set_name = reader.nextString();
                        }else if (key.equals("scryfall_uri")) {
                            scryfall_uri = reader.nextString();
                        }else if (key.equals("image_uris")) {
                            reader.beginObject();
                            while(reader.hasNext()){
                                if (reader.nextName().equals("normal")) {
                                    image_normal = reader.nextString();
                                }else{
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                        }else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();



                    if(temp == 0){
                        mDatabase.beginTransaction();

                    }
                    index++;
                    temp++;

                    insertOrReplaceCard(colors, foil, _id, image_normal, name, nonfoil, set_code, set_name, scryfall_uri);

                    if(temp == divisions){
                        mDatabase.setTransactionSuccessful();
                        mDatabase.endTransaction();
                        temp = 0;
                        buildCardDatabaseInterface.updateCardDatabaseValue(index);
                    }
                }

                reader.endArray();

                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mDatabase.setTransactionSuccessful();
        } finally {
            mDatabase.endTransaction();
        }
    }

    public void insertOrReplaceCard(String colors, int foil, String _id, String image_normal, String name, int nonfoil, String set_code, String set_name, String scryfall_uri){

        StringBuilder sb = new StringBuilder();

        sb.append("INSERT OR REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                                MongoKeys.colors + ", " +
                                MongoKeys.foil + ", " +
                                MongoKeys._id + ", " +
                                "image_normal, " +
                                MongoKeys.name + ", " +
                                MongoKeys.nonfoil + ", " +
                                MongoKeys.set_code + ", " +
                                MongoKeys.set_name + ", " +
                                "scryfall_uri"
        );

        sb.append(") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        mDatabase.execSQL(sb.toString(), new String[]{
                colors,
                Integer.toString(foil),
                _id,
                image_normal,
                name,
                Integer.toString(nonfoil),
                set_code,
                set_name,
                scryfall_uri
        });
    }


    //UPDATE

    public void updatePricesFromJSON(JSONArray cardsArray){
        mDatabase.beginTransaction();
        try {

            for (int i = 0; i < cardsArray.length(); i++) {
                updatePriceFromJSON(cardsArray.getJSONObject(i));
            }

            mDatabase.setTransactionSuccessful();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            mDatabase.endTransaction();
        }
    }

    public void updatePriceFromJSON(JSONObject jsonObject){
        try {
                String usd = "null";
                String eur = "null";

                if (jsonObject.has(MongoKeys.usd)) { usd = jsonObject.getString(MongoKeys.usd); }

                if (jsonObject.has(MongoKeys.eur)) { eur = jsonObject.getString(MongoKeys.eur); }

                String query = "UPDATE " + TABLE_NAME + " SET " +
                        KEY_usd + "=" + usd + ", " + KEY_eur + "=" + eur + ", " + KEY_price_updated_at + "=" + SQLiteType.DefaultSTRFTIME +
                        " WHERE " + KEY_id + "=?";

                mDatabase.execSQL(query, new String[]{jsonObject.getString(MongoKeys._id)});
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }


    public int getQuantity() {
        createTable();

        Cursor cursor = mDatabase.rawQuery("SELECT COUNT(*) as count FROM " + TABLE_NAME, null);
        int quantity = 0;

        while(cursor.moveToNext()){

            if(cursor.isNull(cursor.getColumnIndex("count"))){
                quantity = 0;
            }else{
                quantity = cursor.getInt(cursor.getColumnIndex("count"));
            }
        }

        cursor.close();
        return quantity;
    }


    public String getColumnsNames() {
        Cursor dbCursor = mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
        String[] columnNames = dbCursor.getColumnNames();

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < columnNames.length; i++){
            sb.append(columnNames[i] + " ,");
        }

        return sb.toString();
    }

    public String getColumn(String field){
        return TABLE_NAME + "." + field;
    }

    public String getPoint(String field, String columnAlias){
        return TABLE_NAME + "." + field + " AS " + columnAlias;
    }

    public String leftJoin(String table, String ownfield, String otherField){
        return " LEFT JOIN " + table + " ON " + TABLE_NAME + "." +ownfield + "=" + table + "." + otherField;
    }
}
