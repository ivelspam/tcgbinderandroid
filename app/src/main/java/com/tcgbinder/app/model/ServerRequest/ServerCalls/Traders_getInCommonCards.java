package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class Traders_getInCommonCards extends APIRequest {
    private static final String TAG = "##Traders_getInCommonCards";

    public class ResponseFields {
        public final static String  wanted = "wanted",
                                    cards = "tradde";
    }

    public class Param {
        public final static String  trader_id = "trader_id";
    }

    public class Response {
        public final static String
                found = "found",
                notFound = "notFound";
    }

    public Traders_getInCommonCards(String trader_id){
        keyValues.put(Param.trader_id, trader_id);
        APIUrl = APIValues.Links.traders_getInCommonCards;
    }


}
