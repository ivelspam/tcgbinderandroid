package com.tcgbinder.app.model.Objects;

import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;

import org.json.JSONException;
import org.json.JSONObject;

public class CartCard {

    private final String TAG = "##" + this.getClass().getSimpleName();



    String card_id;
    Integer foilPriceType;
    Integer foilQuantity;
    String _id;
    Double foilMyPrice;
    Double nonfoilMyPrice;
    String name;
    Integer nonfoilQuantity;
    Integer nonfoilPriceType;
    String set_code;
    String set_name;
    Double usd;

    public CartCard(JSONObject jsonObject) {
        try {
            card_id = jsonObject.getString(MongoKeys.card_id);
            foilPriceType = jsonObject.getInt(MongoKeys.foilPriceType);
            foilQuantity = jsonObject.getInt(MongoKeys.foilQuantity);
            _id = jsonObject.getString(MongoKeys._id);
            foilMyPrice = jsonObject.getDouble(MongoKeys.foilMyPrice);
            nonfoilMyPrice = jsonObject.getDouble(MongoKeys.nonfoilMyPrice);
            name = jsonObject.getString(MongoKeys.name);
            nonfoilQuantity = jsonObject.getInt(MongoKeys.nonfoilQuantity);
            nonfoilPriceType = jsonObject.getInt(MongoKeys.nonfoilPriceType);
            set_code = jsonObject.getString(MongoKeys.set_code);
            set_name = jsonObject.getString(MongoKeys.set_name);

            if(jsonObject.has(MongoKeys.usd)){
                usd =  jsonObject.getDouble(MongoKeys.usd);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public CartCard(JSONObject jsonObject, CardInfo cardInfo) {
        try {

            card_id = jsonObject.getString(MongoKeys.card_id);
            foilPriceType = jsonObject.getInt(MongoKeys.foilPriceType);
            foilQuantity = jsonObject.getInt(MongoKeys.foilQuantity);
            _id = jsonObject.getString(MongoKeys._id);
            foilMyPrice = jsonObject.getDouble(MongoKeys.foilMyPrice);
            nonfoilMyPrice = jsonObject.getDouble(MongoKeys.nonfoilMyPrice);
            name = cardInfo.getName();
            nonfoilQuantity = jsonObject.getInt(MongoKeys.nonfoilQuantity);
            nonfoilPriceType = jsonObject.getInt(MongoKeys.nonfoilPriceType);
            set_code = cardInfo.getSet_code();
            set_name = cardInfo.getSet_name();
            usd = cardInfo.getUsd();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getCard_id() {
        return card_id;
    }

    public Integer getFoilPriceType() {
        return foilPriceType;
    }

    public Integer getFoilQuantity() {
        return foilQuantity;
    }

    public String get_id() {
        return _id;
    }

    public Double getFoilMyPrice() {
        return foilMyPrice;
    }

    public Double getNonfoilMyPrice() {
        return nonfoilMyPrice;
    }

    public String getName() {
        return name;
    }

    public Integer getNonfoilQuantity() {
        return nonfoilQuantity;
    }

    public Integer getNonfoilPriceType() {
        return nonfoilPriceType;
    }

    public String getSet_code() {
        return set_code;
    }

    public String getSet_name() {
        return set_name;
    }

    public Double getUsd() {
        return usd;
    }


    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public void setFoilPriceType(Integer foilPriceType) {
        this.foilPriceType = foilPriceType;
    }

    public void setFoilQuantity(Integer foilQuantity) {
        this.foilQuantity = foilQuantity;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setFoilMyPrice(Double foilMyPrice) {
        this.foilMyPrice = foilMyPrice;
    }

    public void setNonfoilMyPrice(Double nonfoilMyPrice) {
        this.nonfoilMyPrice = nonfoilMyPrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNonfoilQuantity(Integer nonfoilQuantity) {
        this.nonfoilQuantity = nonfoilQuantity;
    }

    public void setNonfoilPriceType(Integer nonfoilPriceType) {
        this.nonfoilPriceType = nonfoilPriceType;
    }

    public void setSet_code(String set_code) {
        this.set_code = set_code;
    }

    public void setSet_name(String set_name) {
        this.set_name = set_name;
    }

    public void setUsd(Double usd) {
        this.usd = usd;
    }


    public JSONObject getCardCardInJSONObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MongoKeys.card_id, card_id);
            jsonObject.put(MongoKeys.foilQuantity, foilQuantity);
            jsonObject.put(MongoKeys.foilPriceType, foilPriceType);
            jsonObject.put(MongoKeys.foilMyPrice, foilMyPrice);
            jsonObject.put(MongoKeys._id, _id);
            jsonObject.put(MongoKeys.name, name);
            jsonObject.put(MongoKeys.nonfoilQuantity, nonfoilQuantity);
            jsonObject.put(MongoKeys.nonfoilPriceType, nonfoilPriceType);
            jsonObject.put(MongoKeys.nonfoilMyPrice, nonfoilMyPrice);
            jsonObject.put(MongoKeys.set_code, set_code);
            jsonObject.put(MongoKeys.set_name, set_name);
            jsonObject.put(MongoKeys.usd, usd);

        } catch (JSONException e) {

            e.printStackTrace();
        }
        return jsonObject;
    }

}