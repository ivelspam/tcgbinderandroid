package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class Traders_getCardsByPage extends APIRequest {

    private static final String TAG = "##Traders_getCardsByPage";


    public class ResponseFields {
        public final static String  cards = "cards";
    }

    public class Param {
        public final static String
                trader_id = "trader_id",
                page = "page",
                division = "division";
    }

    public class Response {
        public final static String
                endOfList = "endOfList",
                found = "found";
    }

    public Traders_getCardsByPage(String trader_id, int page, int division){

        keyValues.put(Param.trader_id, trader_id);
        keyValues.put(Param.page, page);
        keyValues.put(Param.division, division);

        token = false;
        APIUrl = APIValues.Links.traders_getCardsByPage;
    }

}