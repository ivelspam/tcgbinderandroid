package com.tcgbinder.app.model.Objects;

import android.database.Cursor;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyCardsTable;

import org.json.JSONException;
import org.json.JSONObject;


public class WantedCard {
    private static final String TAG = "##CardInfo";
    String name;
    int foilQuantity;
    int nonfoilQuantity;
    int traderFoilQuantity = 0;
    int traderNonfoilQuantity = 0;

    public WantedCard(Cursor cursor){
        if(!cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity))){ foilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity)); }
        name = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_name));
        if(!cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity))){ nonfoilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity)); }
    }

    public WantedCard(JSONObject jsonObject){
        try {
            name = jsonObject.getString(MongoKeys.name);
            foilQuantity = jsonObject.getInt(MongoKeys.foilQuantity);
            nonfoilQuantity = jsonObject.getInt(MongoKeys.nonfoilQuantity);
            traderFoilQuantity = jsonObject.getInt("traderFoilQuantity");
            traderNonfoilQuantity = jsonObject.getInt("traderNonfoilQuantity");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public int getFoilQuantity() {
        return foilQuantity;
    }

    public String getName() {
        return name;
    }


    public int getNonfoilQuantity() {
        return nonfoilQuantity;
    }

    public void setCardsQuantity(int foilQuantity, int nonfoilQuantity) {
        this.foilQuantity = foilQuantity;
        this.nonfoilQuantity = nonfoilQuantity;
    }

    public void setFoilQuantity(int foilQuantity) {
        this.foilQuantity = foilQuantity;
    }


    public int getTraderFoilQuantity() {
        return traderFoilQuantity;
    }

    public int getTraderNonfoilQuantity() {
        return traderNonfoilQuantity;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setNonfoilQuantity(int nonfoilQuantity) {
        this.nonfoilQuantity = nonfoilQuantity;
    }


}
