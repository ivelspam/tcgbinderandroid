package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class FCMToken_removeTokenFromSystem extends APIRequest{

    public class Param {
        public final static String
                FCMToken  = MongoKeys.FCMToken;
    }

    public class Response {
        public final static String
                deleted  = "deleted";
    }

    public class ResponseFields {

    }

    public FCMToken_removeTokenFromSystem(Context context){
//        keyValues.put(Param.FCMToken, MyFirebaseInstanceIDService.getToken());
        APIUrl = APIValues.Links.fcmToken_removeTokenFromSystem;

    }
}



