package com.tcgbinder.app.model;

import android.content.Context;

import com.auth0.android.jwt.JWT;
import com.tcgbinder.app.model.Constants.TokenValues;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class TokenUtilities {
    static public String getToken(Context context){
        return SharedPreferencesUtils.getInstance(context).getString(SharedPreferencesUtils.PrefKeys.token);
    }


    static public String get_id(Context context){
        String token = SharedPreferencesUtils.getInstance(context).getString(SharedPreferencesUtils.PrefKeys.token);
        JWT jwt = new JWT(token);
        return jwt.getClaim(TokenValues._id).asString();
    }
}
