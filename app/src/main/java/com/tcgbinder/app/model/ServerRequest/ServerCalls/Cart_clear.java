package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class Cart_clear extends APIRequest {
    private static final String TAG = "##Cart_clear";


    public class ResponseFields {
    }

    public class Param {
        public final static String  trader_id = "trader_id";
    }

    public class Response {
        public final static String
                cleared = "cleared";
    }

    public Cart_clear(String trader_id){
        keyValues.put(Param.trader_id, trader_id);
        APIUrl = APIValues.Links.cart_getCards;
    }


}
