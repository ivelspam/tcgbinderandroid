package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class Google_login extends APIRequest{

    public class ResponseFields {

    }

    public class Param {
        public final static String
                googleIdToken = "googleIdToken";
    }

    public class Response {
        public final static String
                synced = "synced",
                foundUser = "foundUser",
                created = "created";
    }

    public Google_login(String googleIdToken){
        keyValues.put(Param.googleIdToken, googleIdToken);
        APIUrl = APIValues.Links.google_login;
        token = false;
    }

}
