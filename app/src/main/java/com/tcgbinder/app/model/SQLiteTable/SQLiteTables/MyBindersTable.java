package com.tcgbinder.app.model.SQLiteTable.SQLiteTables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.MyBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyBindersTable {
    static private String TAG = "##CardInfoTable";

    private static MyBindersTable sMyBindersTable;

    public static final String TABLE_NAME = "mybinderstable",
            KEY_alwaysOpen = MongoKeys.alwaysOpen,
            KEY_name = MongoKeys.name,
            KEY_id = MongoKeys._id;

    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                        KEY_alwaysOpen + " NUMBER, " +
                        KEY_id + " TEXT UNIQUE, " +
                        KEY_name + " TEXT " +
                    ")";

    SQLiteDatabase mDatabase;

    private MyBindersTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static MyBindersTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sMyBindersTable == null) {
            sMyBindersTable = new MyBindersTable(sqLiteDatabase);
        }
        return sMyBindersTable;
    }

    public void rebuildTable() {
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable();
    }

    public void createTable() {
        mDatabase.execSQL(TABLE_CREATION);
    }


    //INSERT

    //SELECT

    public List<MyBinder> selectAllBinders(){

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME , null);
        List<MyBinder> myBinders = new ArrayList<>();
        while (cursor.moveToNext()) {
            myBinders.add(new MyBinder(cursor));
        }
        cursor.close();
        return myBinders;
    }

    //UPDATE

    public void updateAlwaysShare(String binder_id, boolean valueBoolean){
        int alwaysOpen = 0;
        if(valueBoolean){alwaysOpen = 1;}
        String query = "UPDATE " + TABLE_NAME + " SET " + KEY_alwaysOpen + "=" + Integer.toString(alwaysOpen) + " WHERE " + KEY_id + "=?";
        mDatabase.execSQL(query, new String[]{binder_id});
    }

    public void updateAllBindersTable(JSONArray jsonArray){
            rebuildTable();

            mDatabase.beginTransaction();

            for(int i = 0; i < jsonArray.length(); i++){
                try {
                    insertOrReplaceBinder(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            mDatabase.setTransactionSuccessful();
            mDatabase.endTransaction();
    }

    public void insertOrReplaceBinder(JSONObject binderJSONObject){
        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
            KEY_alwaysOpen + "," +
            KEY_id + "," +
            KEY_name
        );

        sb.append(") VALUES (?, ?, ?)");

        int temp = 0;
        try {
            if(binderJSONObject.getBoolean(MongoKeys.alwaysOpen)){
                    temp = 1;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            mDatabase.execSQL(sb.toString(), new String[]{
                    Integer.toString(temp),
                    binderJSONObject.getString(MongoKeys._id),
                    binderJSONObject.getString(MongoKeys.name)
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }

    public String getQuantity() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null));
    }

    static public String makeAlias(String field, String columnAlias){
        return TABLE_NAME + "." + field + " AS " + columnAlias;
    }

    static public String leftJoin(String table, String ownfield, String otherField){
        return " LEFT JOIN " + table + " ON " + TABLE_NAME + "." +ownfield + "=" + table + "." + otherField;
    }
}
