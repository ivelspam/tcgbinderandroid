package com.tcgbinder.app.model.Objects;

import android.database.Cursor;
import android.util.Log;

import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Extensions.ExtensionsHack;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyCardsTable;

import java.util.Calendar;
import java.util.Date;

public class CardInfo {
    private static final String TAG = "##CardInfo";

    String binderName;
    String colors;
    boolean foil = false;
    int foilQuantity;
    Calendar priceUpdatedAt;
    String _id;
    String imageNormal;
    String name;
    boolean nonfoil = false;
    int nonfoilQuantity;
    String set_code;
    String set_name;
    String scryfall_uri;
    Double usd;


    public CardInfo(Cursor cursor){
        if(cursor.getColumnIndex("binder_name") != -1){
            if(!cursor.isNull(cursor.getColumnIndex("binder_name"))){ binderName = cursor.getString(cursor.getColumnIndex("binder_name"));}
        }

        colors = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_colors));

        if(!cursor.isNull(cursor.getColumnIndex(CardInfoTable.KEY_eur))){ usd = cursor.getDouble(cursor.getColumnIndex(CardInfoTable.KEY_eur));}
        if(cursor.getInt(cursor.getColumnIndex(CardInfoTable.KEY_foil)) == 1){ foil = true; }
        if(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity) != -1 && !cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity))){ foilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity)); }
        _id = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_id));

        imageNormal = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_image_normal));

        name = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_name));

        if(cursor.getInt(cursor.getColumnIndex(CardInfoTable.KEY_nonfoil)) == 1){ nonfoil = true; }

        if(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity) != -1 && !cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity))){

            nonfoilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity)); }

        if(!cursor.isNull(cursor.getColumnIndex(CardInfoTable.KEY_price_updated_at))){ priceUpdatedAt = DateExtension.CalendarMySQLDatetimeTZToCalendar( cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_price_updated_at))); }

        set_code = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_set_code));
        set_name = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_set_name));
        scryfall_uri = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_scryfall_uri));
        if(!cursor.isNull(cursor.getColumnIndex(CardInfoTable.KEY_usd))){ usd = cursor.getDouble(cursor.getColumnIndex(CardInfoTable.KEY_usd)); }

    }

    public String getColors() {
        return colors;
    }

    public boolean isFoil() {
        return foil;
    }

    public int getFoilQuantity() {
        return foilQuantity;
    }

    public String get_id() {
        return _id;
    }

    public String getImageNormal() {
        return imageNormal;
    }

    public String getName() {
        return name;
    }

    public boolean isNonfoil() {
        return nonfoil;
    }

    public int getNonfoilQuantity() {
        return nonfoilQuantity;
    }

    public String getSet_code() {
        return set_code;
    }

    public String getSet_name() {
        return set_name;
    }

    public String getScryfall_uri() {
        return scryfall_uri;
    }

    public String getBinderName() {
        return binderName;
    }

    public void setCardsQuantity(int foilQuantity, int nonfoilQuantity) {
        this.foilQuantity = foilQuantity;
        this.nonfoilQuantity = nonfoilQuantity;
    }

    public Double getUsd() {
        return usd;
    }

    public Calendar getPriceUpdatedAt() {
        return priceUpdatedAt;
    }

    public void setBinderName(String binderName) {
        this.binderName = binderName;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public void setFoil(boolean foil) {
        this.foil = foil;
    }

    public void setFoilQuantity(int foilQuantity) {
        this.foilQuantity = foilQuantity;
    }

    public void setPriceUpdatedAt(Calendar priceUpdatedAt) {
        this.priceUpdatedAt = priceUpdatedAt;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setImageNormal(String imageNormal) {
        this.imageNormal = imageNormal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNonfoil(boolean nonfoil) {
        this.nonfoil = nonfoil;
    }

    public void setNonfoilQuantity(int nonfoilQuantity) {
        this.nonfoilQuantity = nonfoilQuantity;
    }

    public void setSet_code(String set_code) {
        this.set_code = set_code;
    }

    public void setSet_name(String set_name) {
        this.set_name = set_name;
    }

    public void setScryfall_uri(String scryfall_uri) {
        this.scryfall_uri = scryfall_uri;
    }

    public void setUsd(Double usd) {
        this.usd = usd;
    }
}
