package com.tcgbinder.app.model.SQLiteTable.SQLiteTables;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tcgbinder.app.model.Constants.CalendarFormats;
import com.tcgbinder.app.model.Constants.MagicNumbers;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Extensions.ExtensionsHack;
import com.tcgbinder.app.model.Objects.ChatMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

public class ChatMessageTable {
    static private String TAG = "##ChatMessageTable";

    private static ChatMessageTable sChatMessageTable;

    public static final String TABLE_NAME = "chatmessage",
            KEY_content = MongoKeys.content,
            KEY_createdAt = MongoKeys.createdAt,
            KEY_sender_id = MongoKeys.sender_id,
            KEY_receiver_id = MongoKeys.receiver_id,
            KEY_state = MongoKeys.state,
            KEY_aUUID = MongoKeys.aUUID;

    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                        KEY_content + " TEXT, " +
                        KEY_createdAt + " Boolean, " +
                        KEY_receiver_id + " TEXT, " +
                        KEY_sender_id + " TEXT, " +
                        KEY_state + " NUMBER DEFAULT 0, " +
                        KEY_aUUID + " TEXT UNIQUE" +
                    ")";

    SQLiteDatabase mDatabase;

    private ChatMessageTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static ChatMessageTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sChatMessageTable == null) {
            sChatMessageTable = new ChatMessageTable(sqLiteDatabase);
        }
        return sChatMessageTable;
    }

    public void rebuildTable() {
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable();
    }

    public void createTable() {
        mDatabase.execSQL(TABLE_CREATION);
    }


    //INSERT ++++++++++++++++++++++++++++++++++++++++++++++++++++

    public void insertOrReplaceChatMessages(JSONArray jsonArray){
        mDatabase.beginTransaction();

        for(int i = 0; i < jsonArray.length(); i++){
            try {
                insertOrReplaceChatMessagesFromMySQL(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public void insertOrReplaceChatMessage(ChatMessage chatMessage){

        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                KEY_content + "," +
                KEY_createdAt + "," +
                KEY_receiver_id + "," +
                KEY_sender_id + "," +
                KEY_state + "," +
                KEY_aUUID
        );

        sb.append(") VALUES (?, ?, ?, ?, ?, ?)");
        mDatabase.execSQL(sb.toString(), new String[]{
            chatMessage.getContent(),
            DateExtension.CalendarGetFormated(chatMessage.getCreatedAt(), CalendarFormats.MySQLTZ),
            chatMessage.getReceiver_id(),
            chatMessage.getSender_id(),
            Integer.toString(chatMessage.getState()),
            chatMessage.getaUUID()
        });
    }

    public void insertOrReplaceChatMessagesFromMySQL(JSONObject chatMessageJSON){

        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                KEY_content + "," +
                KEY_createdAt + "," +
                KEY_receiver_id + "," +
                KEY_sender_id + "," +
                KEY_state + "," +
                KEY_aUUID
        );

        sb.append(") VALUES (?, ?, ?, ?, ?, ?)");

        try {
            mDatabase.execSQL(sb.toString(), new String[]{
                    chatMessageJSON.getString(MongoKeys.content),
                    chatMessageJSON.getString(MongoKeys.createdAt),
                    chatMessageJSON.getString(MongoKeys.receiver_id),
                    chatMessageJSON.getString(MongoKeys.sender_id),
                    chatMessageJSON.getString(MongoKeys.state),
                    chatMessageJSON.getString(MongoKeys.aUUID),
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //SELECT ++++++++++++++++++++++++++++++++++++++++++++++++++++
    public List<ChatMessage> selectAllChatsFromTrader(String trader_id){
        String query = "SELECT * FROM "+ TABLE_NAME +" WHERE " + KEY_sender_id + "='" + trader_id + "' OR " + KEY_receiver_id + "='" + trader_id + "' ORDER BY " + KEY_createdAt +" ASC";

        Cursor cursor = mDatabase.rawQuery(query, null);
        List<ChatMessage> chatMessages = new ArrayList<>();

        while(cursor.moveToNext()){
            chatMessages.add(new ChatMessage(cursor));
        }
        cursor.close();
        return chatMessages;
    }

    public boolean selectAllChatsByUUID(String AUUID){
        String query = "SELECT * FROM "+ TABLE_NAME +" WHERE " + KEY_aUUID + "='" + AUUID + "'";

        Cursor cursor = mDatabase.rawQuery(query, null);
        List<ChatMessage> chatMessages = new ArrayList<>();

        while(cursor.moveToNext()){
            return true;
        }
        cursor.close();
        return false;
    }

    public List<ChatMessage> selectAllChatByState(int state){
        String query = "SELECT * FROM "+ TABLE_NAME +" WHERE " + KEY_state + "=" + state;

        Cursor cursor = mDatabase.rawQuery(query, null);
        List<ChatMessage> chatMessages = new ArrayList<>();

        while(cursor.moveToNext()){
            chatMessages.add(new ChatMessage(cursor));
        }
        cursor.close();
        return chatMessages;
    }

    public String selectNewestDate(String trader_id){
        String query = "SELECT MAX("+ KEY_createdAt +") AS maxDate FROM "+ TABLE_NAME +" WHERE " + KEY_sender_id + "='" + trader_id + "' OR " + KEY_receiver_id + "='" + trader_id + "'";
        Cursor cursor = mDatabase.rawQuery(query, null);
        String latestCreatedAt = MagicNumbers.LOWEST_MYSQL_TS;

        while(cursor.moveToNext()){
            if(!cursor.isNull(cursor.getColumnIndex("maxDate"))){
                latestCreatedAt = cursor.getString(cursor.getColumnIndex("maxDate"));
            }
        }

        cursor.close();
        return latestCreatedAt;
    }

    //UPDATE

    public String getColumn(String field){
        return TABLE_NAME + "." + field;
    }

    public String getPoint(String field, String columnAlias){
        return TABLE_NAME + "." + field + " AS " + columnAlias;
    }

    public String leftJoin(String table, String ownfield, String otherField){
        return " LEFT JOIN " + table + " ON " + TABLE_NAME + "." +ownfield + "=" + table + "." + otherField;
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }
}
