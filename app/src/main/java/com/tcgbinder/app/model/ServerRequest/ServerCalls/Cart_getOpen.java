package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class Cart_getOpen extends APIRequest {


    private static final String TAG = "##Cart_getOpen";


    public class ResponseFields {
        public final static String openCarts = "openCarts";
    }

    public class Param {
    }

    public class Response {
        public final static String
                done = "done";
    }

    public Cart_getOpen(){
        APIUrl = APIValues.Links.cart_getOpen;
    }



}
