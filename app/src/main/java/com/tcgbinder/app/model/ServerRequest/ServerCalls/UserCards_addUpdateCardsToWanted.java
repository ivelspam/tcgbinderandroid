package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class UserCards_addUpdateCardsToWanted extends APIRequest {
    public class ResponseFields {

    }

    public class Param {
        public static final String
                card_id = "card_id",
                foilQuantity = MongoKeys.foilQuantity,
                nonfoilQuantity = MongoKeys.nonfoilQuantity;
    }

    public class Response {
        public static final String
                addedWanted = "addedWanted",
                updatedWanted = "updatedWanted";

    }

    public UserCards_addUpdateCardsToWanted(String card_id, int foilQuantity, int nonfoilQuantity){
        keyValues.put(Param.card_id, card_id);
        keyValues.put(Param.foilQuantity, foilQuantity);
        keyValues.put(Param.nonfoilQuantity, nonfoilQuantity);

        APIUrl = APIValues.Links.userCards_addUpdateCardsToWanted;

    }
}
