package com.tcgbinder.app.model.SQLiteTable.SQLiteTables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.SQLiteColumns;
import com.tcgbinder.app.model.Constants.SQLiteType;
import com.tcgbinder.app.model.Extensions.ExtensionsHack;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class MyWantedGeneralTable {

    static private String TAG = "##MyWantedTable";

    private static MyWantedGeneralTable sMyWantedGeneralTable;

    public static final String TABLE_NAME = "mywantedgeneral",

            KEY_clientUpdatedAT = SQLiteColumns.clientUpdatedAt,
            KEY_name = MongoKeys.name,
            KEY_foilQuantity = MongoKeys.foilQuantity,
            KEY_nonFoilQuantity = MongoKeys.nonfoilQuantity;

    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                    KEY_clientUpdatedAT + " " + SQLiteType.TimestampDefault +", " +
                    KEY_foilQuantity + " TEXT," +
                    KEY_nonFoilQuantity + " TEXT," +
                    KEY_name + " TEXT PRIMARY KEY" +
                    ")";

    SQLiteDatabase mDatabase;

    private MyWantedGeneralTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static MyWantedGeneralTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sMyWantedGeneralTable == null) {
            sMyWantedGeneralTable = new MyWantedGeneralTable(sqLiteDatabase);
        }
        return sMyWantedGeneralTable;
    }

    public void rebuildTable() {
//        Log.i(TAG, "rebuildTable");
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable();
    }

    public void createTable(){
        mDatabase.execSQL(TABLE_CREATION);

    }

    //INSERT

    public void insertOrReplaceAllCards(JSONArray jsonArray){
        rebuildTable();

        mDatabase.beginTransaction();

        for(int i = 0; i < jsonArray.length(); i++){
            try {
                insertOrReplaceCardsFromMySQL(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public void insertOrReplaceCardsFromMySQL(JSONObject cardsJSONObject){

        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME + " (");

        sb.append(
                KEY_name + "," +
                KEY_foilQuantity + "," +
                KEY_nonFoilQuantity
        );

        sb.append(") VALUES ( ?, ?, ?)");

        try {
            mDatabase.execSQL(sb.toString(), new String[]{
                    cardsJSONObject.getString(MongoKeys.name),
                    Integer.toString(cardsJSONObject.getInt(MongoKeys.foilQuantity)),
                    Integer.toString(cardsJSONObject.getInt(MongoKeys.nonfoilQuantity))
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //DELETE

    public void removeCard(String card_id){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_name + "=?";

        mDatabase.execSQL(query, new String[]{card_id});
    }

    //SELECT

    public List<WantedCard> selectAllCards(){
        String query =  "SELECT * FROM " + TABLE_NAME + " ORDER BY " + KEY_name;

        Cursor cursor = mDatabase.rawQuery( query, null);
        List<WantedCard> myCards = new ArrayList<>();

        while (cursor.moveToNext()) {
            myCards.add(new WantedCard(cursor));
        }
        cursor.close();

        return myCards;
    }



    //UPDATE

    public void insertOrReplaceCards(String name, int foilQquantity, int nonfoilQuantity){
        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
            KEY_name + "," +
            KEY_foilQuantity + "," +
            KEY_nonFoilQuantity
        );

        sb.append(") VALUES ( ?, ?, ?)");

        mDatabase.execSQL(sb.toString(), new String[]{
            name,
            Integer.toString(foilQquantity),
            Integer.toString(nonfoilQuantity)
        });
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }

    public String getQuantity() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null));
    }

    static public String getPoint(String field, String columnAlias){
        return TABLE_NAME + "." + field + " AS " + columnAlias;
    }

    static public String leftJoin(String table, String ownfield, String otherField){
        return " LEFT JOIN " + table + " ON " + TABLE_NAME + "." +ownfield + "=" + table + "." + otherField;
    }

}


