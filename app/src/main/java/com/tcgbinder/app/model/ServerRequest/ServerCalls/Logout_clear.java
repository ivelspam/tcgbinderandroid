package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;

import com.tcgbinder.app.model.Constants.APIValues;

public class Logout_clear extends APIRequest {

    private static final String TAG = "##Logout_clear";

    public class ResponseFields {

    }

    public class Param {
        public final static String
                fcmToken = "fcmToken";
    }

    public class Response {
        public final static String
                loggedout = "loggedout";
    }

    public Logout_clear(Context context){

        APIUrl = APIValues.Links.logout_clear;
    }

}