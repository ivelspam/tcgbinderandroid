package com.tcgbinder.app.model.Constants;

import android.content.Context;

import com.tcgbinder.app.model.TokenUtilities;

public class SocketIOConnections {

    public static String clientReceiveEventMessage(int eventID){
        return "clientReceiveEventMessage" + Integer.toString(eventID);
    }

    public static String makeChatRoomId(Context context, String trader_id){

        String user_id = TokenUtilities.get_id(context);
        int compare = user_id.compareTo(trader_id);
        String chatRoomId = "chat_" + user_id + "_"+ trader_id ;
        if(compare > 0){
            chatRoomId = "chat_" + trader_id + "_" + user_id;
        }
        return chatRoomId;
    }

    public static String makeCartRoom(Context context, String trader_id){

        String user_id = TokenUtilities.get_id(context);
        int compare = user_id.compareTo(trader_id);
        String tradeRoomId = "cart_" + user_id + "_"+ trader_id ;
        if(compare > 0){
            tradeRoomId = "cart_" + trader_id + "_" + user_id;
        }
        return tradeRoomId;
    }

    static public String sendChatMessage = "sendChatMessage";
    static public String addWantCartCard = "addWantCartCard";
    static public String updateCartCard = "updateCartCard";
    static public String removeCartCard = "removeCartCard";

    static public String clearCart = "clearCart";

}