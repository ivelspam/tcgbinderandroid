package com.tcgbinder.app.model;

import android.app.Activity;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.tcgbinder.app.controller.TransactionLoginScreen;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.ServerRequest.QueryForUpdatedSingleton;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Logout_clear;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONObject;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class Logout {
    private static final String TAG = "##Logout";

    static public void logout(final Context context) {
        Activity activity = (Activity)context;
        ExecutorService executorService = Executors.newCachedThreadPool();

        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.cancel(ExampleJobService.ID);

        Callable<Boolean> logoutCallable = new Callable<Boolean>(){

            @Override
            public Boolean call() throws Exception {
                JSONObject jsonObject = new ServerAPI(context).serverRequestPOST(new Logout_clear(context));
                Motherbase.getInstance(context).restartDatabase();
                return true;
            }
        };

        Future<Boolean> future = executorService.submit(logoutCallable);
        executorService.shutdown();


        try {
            future.get();

            SharedPreferencesUtils.getInstance(context).clear();

            LoginManager.getInstance().logOut();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestIdToken("337582233400-6r68du6l0u8gm0gcl2t9b0fj0lo4gc5e.apps.googleusercontent.com")
                    .build();
            GoogleSignInClient GoogleSignInClient = GoogleSignIn.getClient(context, gso);
            GoogleSignInClient
                    .signOut()
                    .addOnCompleteListener((Activity) context, new OnCompleteListener<Void>() {

                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                        }
                    });

            QueryForUpdatedSingleton.getInstance().endTask();
            SharedPreferencesUtils.getInstance(context).clear();

            Intent intent = new Intent(context, TransactionLoginScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            activity.finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}

