package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class CartCard_addCardCart extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public final static String
                card_id = "card_id",
                foilQuantity = "foilQuantity",
                nonfoilQuantity = "nonfoilQuantity",
                trader_id = "trader_id";

    }

    public class Response {
        public final static String
                changed = "changed";
    }

    public CartCard_addCardCart(String card_id, Integer foilQuantity, Integer nonfoilQuantity, String trader_id){
        keyValues.put(Param.card_id, card_id);
        keyValues.put(Param.foilQuantity, foilQuantity);
        keyValues.put(Param.nonfoilQuantity, nonfoilQuantity);
        keyValues.put(Param.trader_id, trader_id);

        APIUrl = APIValues.Links.user_openForTrading;
    }
}
