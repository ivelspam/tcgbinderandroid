package com.tcgbinder.app.model;

import android.app.job.JobParameters;
import android.app.job.JobService;

import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_updateLocation;

public class ExampleJobService extends JobService {
    private static final String TAG = "##ExampleJobService";
    public static final int ID = 123;


    private boolean jobCancelled = false;


    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        doBackGroundWork(jobParameters);
        return true;
    }

    private void doBackGroundWork(final JobParameters parameters){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(jobCancelled){
                    return;
                }
                new ServerAPI(getApplicationContext()).serverRequestPOST(new User_updateLocation(getApplicationContext()));
                jobFinished(parameters, false);
            }
        }).start();
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        jobCancelled = true;
        return false;
    }

}
