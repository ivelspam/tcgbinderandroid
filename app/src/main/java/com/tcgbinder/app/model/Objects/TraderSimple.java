package com.tcgbinder.app.model.Objects;

import com.tcgbinder.app.model.Constants.MongoKeys;

import org.json.JSONException;
import org.json.JSONObject;

public class TraderSimple {

    private static final String TAG = "##TraderWithQuantity";

    String _id;
    String name;
    Double distance;
    String tradeInfo = "";

    public TraderSimple(JSONObject jsonObject){

        try {
            _id = jsonObject.getString(MongoKeys._id);
            name = jsonObject.getString(MongoKeys.name);

            if(jsonObject.has(MongoKeys.tradeInfo)){
                tradeInfo = jsonObject.getString(MongoKeys.tradeInfo);
            }

            distance = jsonObject.getDouble(MongoKeys.distance);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getName() {
        return name;
    }

    public Double getDistance() {
        return distance;
    }

    public String get_id() {return _id;}
    public String getTradeInfo() {return tradeInfo;}

}
