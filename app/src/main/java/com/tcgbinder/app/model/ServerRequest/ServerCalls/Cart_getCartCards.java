package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class Cart_getCartCards extends APIRequest {
    private static final String TAG = "##Cart_getCartCards";


    public class ResponseFields {
        public final static String cards = "cards";
    }

    public class Param {
        public final static String  trader_id = "trader_id";
    }

    public class Response {
        public final static String
                changed = "changed";
    }

    public Cart_getCartCards(String trader_id){
        keyValues.put(Param.trader_id, trader_id);
        APIUrl = APIValues.Links.cart_getCards;
    }


}
