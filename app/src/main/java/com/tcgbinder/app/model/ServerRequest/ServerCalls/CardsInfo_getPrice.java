package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class CardsInfo_getPrice extends APIRequest {

    public class ResponseFields {
        public final static String
                price = "price";
    }

    public class Param {
        public final static String
                card_id = "card_id";
    }

    public class Response {
        public final static String
                found = "found";
    }

    public CardsInfo_getPrice(String card_id) {
        token = false;
        keyValues.put(Param.card_id, card_id);
        APIUrl = APIValues.Links.cardsInfo_getPrice;
    }
}
