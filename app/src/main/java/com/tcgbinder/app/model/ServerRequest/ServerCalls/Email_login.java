package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class Email_login extends APIRequest{

    public class ResponseFields {

    }

    public class Param {
        public final static String
                email = MongoKeys.email,
                password = MongoKeys.password;
    }

    public class Response {
        public final static String
                noMatch = "noMatch",
                match = "match";
    }

    public Email_login(String email, String password){
        keyValues.put(Param.email, email);
        keyValues.put(Param.password, password);
        APIUrl = APIValues.Links.email_login;
        token = false;
    }

}
