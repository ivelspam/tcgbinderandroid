package com.tcgbinder.app.model.ServerRequest.ServerCalls;

public class APIRequest {
    public final static String message = "message";

    public String APIUrl;
    public KeyValues keyValues = new KeyValues();
    public Boolean token = true;

    public String getAPIUrl(){
        return APIUrl;
    }
    public String getBody(){
        return keyValues.getBody();
    }
    public String getQuery(){
        return keyValues.getQuery();
    }

}
