package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class UserCards_deleteWantedCard extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public static final String
                card_id = MongoKeys.card_id;
    }

    public class Response {
        public static final String
                wantedDeleted = "wantedDeleted";
    }

    public UserCards_deleteWantedCard(String card_id){
        keyValues.put(Param.card_id, card_id);

        APIUrl = APIValues.Links.userCards_deleteWantedCard;

    }
}
