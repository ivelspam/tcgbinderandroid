package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class UserCards_deleteWantedGeneralCard extends APIRequest {
    public class ResponseFields {

    }

    public class Param {
        public static final String
                card_id = "card_id";
    }

    public class Response {
        public static final String
                wantedGeneralDeleted = "wantedGeneralDeleted";
    }



    public UserCards_deleteWantedGeneralCard(String card_id){
        keyValues.put(Param.card_id, card_id);

        APIUrl = APIValues.Links.userCards_deleteWantedGeneralCard;
    }
}
