package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class UserCards_setCardToBinder extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public static final String
                card_id = "card_id",
                binder_id = "binder_id";
    }

    public class Response {
        public static final String
                cardBinderChanged = "cardBinderChanged";

    }

    public UserCards_setCardToBinder(String binder_id, String card_id){
        keyValues.put(Param.card_id, card_id);
        keyValues.put(Param.binder_id, binder_id);

        APIUrl = APIValues.Links.userCards_setCardToBinder;

    }
}
