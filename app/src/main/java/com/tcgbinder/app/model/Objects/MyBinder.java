package com.tcgbinder.app.model.Objects;

import android.database.Cursor;

import com.tcgbinder.app.model.Constants.MongoKeys;

public class MyBinder {

    String _id;
    String name;
    boolean alwaysOpen = false;

    public  MyBinder(Cursor cursor){
        _id = cursor.getString(cursor.getColumnIndex(MongoKeys._id));
        name = cursor.getString(cursor.getColumnIndex(MongoKeys.name));


        if(cursor.getInt(cursor.getColumnIndex(MongoKeys.alwaysOpen)) == 1){
            alwaysOpen = true;
        }

    }

    public  MyBinder(){
        name = "No Binder";
    }

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public boolean isAlwaysOpen() {
        return alwaysOpen;
    }
}
