package com.tcgbinder.app.model.Constants;

public class ServerAddress {

    //PRODUCTION

    public static final String APIADDRESS = "https://tcgbinder.com/api/";
    public static final String SOCKETADDRESS = "https://tcgbinder.com:3000";




    //DEVELOPMENT
//    public static final String APIADDRESS = "http://192.168.29.248:8080/";
//    public static final String SOCKETADDRESS = "http://192.168.29.248:3000";

}
