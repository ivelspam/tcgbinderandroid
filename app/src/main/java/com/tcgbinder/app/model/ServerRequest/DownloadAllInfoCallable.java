package com.tcgbinder.app.model.ServerRequest;

import android.content.Context;
import android.util.Log;

import com.tcgbinder.app.model.ServerRequest.ServerCalls.AllInfo_getAllInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;

public class DownloadAllInfoCallable implements Callable<JSONObject> {

    static private String TAG = "##DownloadAllInfo";
    Context mContext;

    public DownloadAllInfoCallable(Context context){
        mContext = context;
    }

    @Override
    public JSONObject call() {
        JSONObject jsonObject = new ServerAPI(mContext).serverRequestPOST(new AllInfo_getAllInfo(mContext));
//        Motherbase.getInstance(mContext).rebuildDatabase();


//        try {
//            if(Toasts.responseIsGood(jsonObject, mContext)){
//                switch (jsonObject.getString(APIRequest.message)){
//                    case AllInfo_getAllInfo.Response.userFound:
//                        //appUserHobbies
//                            ServerRequestUpdateSQLiteMethods.insertOrReplaceAppUserHobbies(jsonObject, mContext);
//                        //appUserInfo
//                            SharedPreferencesUtils.getInstance(mContext).buildUser(jsonObject);
//                        //chats
//                            ServerRequestUpdateSQLiteMethods.insertOrReplaceChats(jsonObject, mContext);
//                        //events
//                            ServerRequestUpdateSQLiteMethods.insertOrReplaceEvents(jsonObject, mContext);
//                        //friends
//                            ServerRequestUpdateSQLiteMethods.insertOrReplaceFriends(jsonObject, mContext);
//                        //hobbiesTable
//                            ServerRequestUpdateSQLiteMethods.insertHobbiesAndHobbyClosure(jsonObject, mContext);
//                        //inEvent
//                            ServerRequestUpdateSQLiteMethods.insertOrReplaceInEvent(jsonObject, mContext);
//                        break;
//                    case AllInfo_getAllInfo.Response.userNotFound:
//                        break;
//                    default:
//                        Log.i(TAG, "SWITCH CASE NOT FOUND");
//                }
//            }
//    } catch (JSONException e) {
//            e.printStackTrace();
//        }



        return jsonObject;
    }



}
