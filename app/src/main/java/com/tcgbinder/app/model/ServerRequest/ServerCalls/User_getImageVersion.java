package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class User_getImageVersion extends APIRequest {


    public class ResponseFields {
        public final static String
                ImageVersion = MongoKeys.imageVersion;

    }

    public class Param {
        public final static String
                AppUserID = "AppUserID";
    }

    public class Response {
        public final static String
                foundImage = "foundImage",
                notFoundImage = "notFoundImage";
    }

    public User_getImageVersion(int appUserID){
        keyValues.put(Param.AppUserID, appUserID);
        APIUrl = APIValues.Links.appuser_getImageVersion;
        token = false;
    }
}
