package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class UserCards_getAllUserCards extends APIRequest {

    public class ResponseFields {
        public final static String
                cards = "cards",
                wanted = "wanted",
                cardsGeneral = "cardsGeneral";
    }

    public class Param {

    }

    public class Response {
        public final static String
                gotAllCards = "gotAllCards";
    }

    public UserCards_getAllUserCards() {
        APIUrl = APIValues.Links.userCards_getAllUserCards;
    }
}