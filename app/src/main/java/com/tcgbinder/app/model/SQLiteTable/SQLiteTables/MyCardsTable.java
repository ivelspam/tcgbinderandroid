package com.tcgbinder.app.model.SQLiteTable.SQLiteTables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.SQLiteColumns;
import com.tcgbinder.app.model.Constants.SQLiteType;
import com.tcgbinder.app.model.Extensions.ExtensionsHack;
import com.tcgbinder.app.model.Objects.CardInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyCardsTable {
    static private String TAG = "##MyCardsTable";

    private static MyCardsTable sMyCardsTable;

    public static final String TABLE_NAME = "mycardstable",

            KEY_clientUpdatedAT = SQLiteColumns.clientUpdatedAt,
            KEY_id = MongoKeys._id,
            KEY_foilQuantity = MongoKeys.foilQuantity,
            KEY_nonFoilQuantity = MongoKeys.nonfoilQuantity;

    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                        KEY_clientUpdatedAT + " " + SQLiteType.TimestampDefault +", " +
                        KEY_foilQuantity + " TEXT," +
                        KEY_nonFoilQuantity + " TEXT," +
                        KEY_id + " TEXT PRIMARY KEY" +
                    ")";

    SQLiteDatabase mDatabase;

    private MyCardsTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static MyCardsTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sMyCardsTable == null) {
            sMyCardsTable = new MyCardsTable(sqLiteDatabase);
        }
        return sMyCardsTable;
    }

    public void rebuildTable() {
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTable();
    }

    public void createTable(){
        mDatabase.execSQL(TABLE_CREATION);

    }


    //INSERT

    public void insertOrReplaceAllCards(JSONArray jsonArray){
        rebuildTable();

        mDatabase.beginTransaction();

        for(int i = 0; i < jsonArray.length(); i++){
            try {
                insertOrReplaceCardsFromMySQL(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public void insertOrReplaceCardsFromMySQL(JSONObject cardsJSONObject){
        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                KEY_id + "," +
                KEY_foilQuantity + "," +
                KEY_nonFoilQuantity
            );

        sb.append(") VALUES ( ?, ?, ?)");

        try {
            mDatabase.execSQL(sb.toString(), new String[]{cardsJSONObject.getString(MongoKeys._id),
                    Integer.toString(cardsJSONObject.getInt(MongoKeys.foilQuantity)),
                    Integer.toString(cardsJSONObject.getInt(MongoKeys.nonfoilQuantity))
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public List<CardInfo> selectAllCards(){
        String query =  "SELECT " + CardInfoTable.TABLE_NAME + ".*, ClientUpdatedAt, foilQuantity, nonFoilQuantity FROM " + TABLE_NAME +
                        leftJoin(CardInfoTable.TABLE_NAME, MyCardsTable.KEY_id, CardInfoTable.KEY_id ) +
                        " ORDER BY " + CardInfoTable.KEY_name;

        Cursor cursor = mDatabase.rawQuery( query, null);
        List<CardInfo> myCards = new ArrayList<>();

        while (cursor.moveToNext()) {
            myCards.add(new CardInfo(cursor));
        }
        cursor.close();
        return myCards;
    }

    public List<CardInfo> selectAllFromBinder(String binder_id){

        List<CardInfo> myCards = new ArrayList<>();

        if(TextUtils.isEmpty(binder_id)){
            return myCards;
        }

        String query =  "SELECT " + CardInfoTable.TABLE_NAME + ".*, binder_id, ClientUpdatedAt, foilQuantity, nonFoilQuantity, " + MyBindersTable.makeAlias(MyBindersTable.KEY_name, "binder_name") + " FROM " + TABLE_NAME +
                        leftJoin(CardInfoTable.TABLE_NAME,MyCardsTable.KEY_id, CardInfoTable.KEY_id ) +
                        " WHERE " + MyBindersTable.TABLE_NAME +"." + MyBindersTable.KEY_id + "='" + binder_id + "'" +
                        " ORDER BY " + CardInfoTable.KEY_name;

        Cursor cursor = mDatabase.rawQuery( query, null);

        while (cursor.moveToNext()) {
            myCards.add(new CardInfo(cursor));
        }
        cursor.close();
        return myCards;
    }

    //REMOVE
    public void removeCard(String card_id){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_id + "=?";
        mDatabase.execSQL(query, new String[]{card_id});
    }

    //UPDATE

    public void insertOrReplaceCards(String binder_id , String _id, int foilQquantity, int nonfoilQuantity){
        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                        KEY_id + "," +
                        KEY_foilQuantity + "," +
                        KEY_nonFoilQuantity
        );

        sb.append(") VALUES (?, ?, ?)");

        mDatabase.execSQL(sb.toString(), new String[]{
                _id,
                Integer.toString(foilQquantity),
                Integer.toString(nonfoilQuantity)
        });
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }


    public String getQuantity() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null));
    }

    static public String getPoint(String field, String columnAlias){
        return TABLE_NAME + "." + field + " AS " + columnAlias;
    }

    static public String leftJoin(String table, String ownfield, String otherField){
        return " LEFT JOIN " + table + " ON " + TABLE_NAME + "." +ownfield + "=" + table + "." + otherField;
    }

}

