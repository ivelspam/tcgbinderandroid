package com.tcgbinder.app.model.Extensions;

import android.util.Log;

import com.tcgbinder.app.model.Constants.CalendarFormats;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateExtension {

    private static final String TAG = "##DateExtension";

    public static Calendar CalendarMySQLDatetimeTZToCalendar(String dateString){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.MySQLTZ);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal;
    }

    public static Calendar CalendarDateToCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String MySQLToPattern(String dateString, String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.MySQLTZ);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar calendar = Calendar.getInstance();
        simpleDateFormat.setCalendar(calendar);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern);
        return simpleDateFormat2.format(calendar.getTime());
    }

    public static String MySQLToDefaultPattern(String dateString){
        String replacedString = dateString.replace("T", " ").replace("Z", "");
        return replacedString;
    }

    static public Calendar CalendarToNearestRoundUp(int nextMinute){
        Calendar calendar = Calendar.getInstance();
        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % nextMinute;
        calendar.add(Calendar.MINUTE, mod == 0 ? nextMinute : nextMinute - mod);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    static public String CalendarGetFormated(Calendar calendar, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(calendar.getTime());
    }

    static public Boolean CalendarCompareTwoMySQLTSFirstBiggerThanSecond(String first, String second){

        SimpleDateFormat format = new SimpleDateFormat( CalendarFormats.MySQLTZ);

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(first);
            date2 = format.parse(second);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1.compareTo(date2) > 0;
    }

    static public Boolean CalendarOlderThanOneDay(Calendar first){
        if(first == null) return true;
        Date d1 = first.getTime();
        Date currentDate = new Date();
        Long diff = currentDate.getTime() - d1.getTime();
        return diff > 1000 * 60 * 60 * 24;
    }
}
