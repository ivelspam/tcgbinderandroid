package com.tcgbinder.app.model.Constants;

import java.util.HashMap;
import java.util.Map;

public class TokenValues {

    static public final String accountStatus = MongoKeys.accountStatus,
                                email = MongoKeys.email,
                                _id = MongoKeys._id;

    public enum AccountStatusState {
        NotConfirmed(0),
        Confirmed(1),
        Deleted(2);

        private int value;
        private static Map map = new HashMap<>();

        private AccountStatusState(int value) {
            this.value = value;
        }

        static {
            for (AccountStatusState pageType : AccountStatusState.values()) {
                map.put(pageType.value, pageType);
            }
        }

        public static AccountStatusState valueOf(int pageType) {
            return (AccountStatusState) map.get(pageType);
        }

        public int getValue() {
            return value;
        }

        public String getValueString() {
            return Integer.toString(value);
        }
    }
}
