package com.tcgbinder.app.model.Constants;

public class CalendarFormats {

    static public final String shortFormat = "h:mm a",
                        dayhour = "E, h:mm a",
                        MySQLTZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

}
