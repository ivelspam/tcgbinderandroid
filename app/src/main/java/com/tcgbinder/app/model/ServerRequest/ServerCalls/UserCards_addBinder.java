package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

import java.io.File;

public class UserCards_addBinder extends APIRequest {

    public class ResponseFields {
        public static final String
                binders = "binders";
    }

    public class Param {
        public static final String
                name = "name";
    }

    public class Response {
        public static final String
                added = "added";

    }

    public UserCards_addBinder(String name){
        keyValues.put(Param.name, name);

        APIUrl = APIValues.Links.userCards_addBinder;

    }
}
