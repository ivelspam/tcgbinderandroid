package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

public class Traders_getAllInfo extends APIRequest {
    private static final String TAG = "##ChatRoom_getNewFromUser";


    public class ResponseFields {
        public final static String chatMessages = "chatMessages";
    }

    public class Param {
        public final static String  trader_id = "trader_id",
                                    latestCreatedAt = "latestCreatedAt";

    }

    public class Response {
        public final static String
                done = "done";
    }

    public Traders_getAllInfo(Context context, String trader_id){

        keyValues.put(Param.trader_id, trader_id);
        keyValues.put(Param.latestCreatedAt, Motherbase.getInstance(context).getChatMessageTable().selectNewestDate(trader_id));
        APIUrl = APIValues.Links.traders_getAllInfo;

    }


}
