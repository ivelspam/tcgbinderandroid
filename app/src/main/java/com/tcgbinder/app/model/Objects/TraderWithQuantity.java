package com.tcgbinder.app.model.Objects;

import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;

import org.json.JSONException;
import org.json.JSONObject;

public class TraderWithQuantity {
    private static final String TAG = "##TraderWithQuantity";

    String _id;
    String name;
    Double distance;
    String tradeInfo = "";
    int nonfoilQuantity = 0;
    int foilQuantity = 0;
    int anySetNonfoilQuantity = 0;
    int anySetFoilQuantity = 0;
    boolean hasCards = false;


    public TraderWithQuantity(JSONObject jsonObject){

        try {
            _id = jsonObject.getString(MongoKeys._id);
            name = jsonObject.getString(MongoKeys.name);

            if(jsonObject.has(MongoKeys.tradeInfo)){
                tradeInfo = jsonObject.getString(MongoKeys.tradeInfo);
            }

            distance = jsonObject.getDouble(MongoKeys.distance);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void setAnySetQuantity(JSONObject jsonObject){
        try {
            if(jsonObject.has("anySetFoilQuantity")){
                anySetFoilQuantity = jsonObject.getInt("anySetFoilQuantity");
            }

            if(jsonObject.has("anySetNonfoilQuantity")){
                anySetNonfoilQuantity = jsonObject.getInt("anySetNonfoilQuantity");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(anySetFoilQuantity > 0 || anySetNonfoilQuantity > 0){
            hasCards = true;
        }
    }

    public void setSetQuantity(JSONObject jsonObject){
        try {
            if(jsonObject.has(MongoKeys.foilQuantity)){
                foilQuantity = jsonObject.getInt(MongoKeys.foilQuantity);
            }

            if(jsonObject.has(MongoKeys.nonfoilQuantity)){
                nonfoilQuantity = jsonObject.getInt(MongoKeys.nonfoilQuantity);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(foilQuantity > 0 || nonfoilQuantity > 0){
            hasCards = true;
        }

    }


    public boolean isHasCards() {
        return hasCards;
    }

    public String getName() {
        return name;
    }

    public Double getDistance() {
        return distance;
    }

    public int getNonfoilQuantity() {
        return nonfoilQuantity;
    }

    public int getAnySetNonfoilQuantity() {
        return anySetNonfoilQuantity;
    }

    public int getAnySetFoilQuantity() {
        return anySetFoilQuantity;
    }

    public int getFoilQuantity() {
        return foilQuantity;
    }

    public String get_id() {return _id;}
    public String getTradeInfo() {return tradeInfo;}

}
