package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class User_UpdateDistanceAndMeasurementSystem extends APIRequest{

    public class ResponseFields {}

    public class Param {
        public final static String
                MeasureSystem  = MongoKeys.measurementSystem,
                Distance = MongoKeys.distance;
    }

    public class Response {
        public final static String
                updated = "updated",
                notUpdated = "notUpdated";
    }

    public enum MeasurementSystem {
        km("km"),
        mi("mi");

        private String value;
        public String getValue() { return this.value; }
        MeasurementSystem(String value){ this.value = value; }
    }
    public User_UpdateDistanceAndMeasurementSystem(MeasurementSystem type, int distance){
        keyValues.put(Param.MeasureSystem, type.value);
        keyValues.put(Param.Distance, distance);
        APIUrl = APIValues.Links.user_UpdateDistanceAndMeasurementSystem;
    }

}
