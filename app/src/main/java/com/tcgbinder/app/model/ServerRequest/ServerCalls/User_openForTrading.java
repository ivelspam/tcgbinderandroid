package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Location.LocationUtils;

public class User_openForTrading extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public final static String
                openForTrading = "openForTrading",
                latitude = "latitude",
                longitude = "longitude";
    }

    public class Response {
        public final static String
                changed = "changed";
    }

    public User_openForTrading(boolean openForTrading, Context context){

        Location location = LocationUtils.getLastKnownLocation(context);

        keyValues.put(Param.openForTrading, openForTrading);
        keyValues.put(Param.latitude, location.getLatitude());
        keyValues.put(Param.longitude, location.getLongitude());

        APIUrl = APIValues.Links.user_openForTrading;
    }
}
