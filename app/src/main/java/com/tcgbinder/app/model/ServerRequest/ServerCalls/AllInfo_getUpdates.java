package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class AllInfo_getUpdates extends APIRequest{
    static private String TAG = "##AllInfo_getAllInfo";
    public Boolean token = true;

    public class Response {
        public final static String  foundUpdates = "foundUpdates",
                                    noUpdates = "noUpdates";
    }

    public class ResponseFields {
        public final static String
                            AppUserChange = "AppUserChange",
                            chats = "chats",
                            inEvent = "inEvent",
                            eventsUpdate = "eventsUpdate",
                            events = "events",
                            friends = "friends";
    }

    public class Param {
        public final static String
                    Latitude = MongoKeys.latitude,
                    Longitude = MongoKeys.longitude,
                    updatedAt = MongoKeys.updatedAt;
    }

    public AllInfo_getUpdates(Context context){
        Location location = LocationUtils.getLastKnownLocation(context);

        SharedPreferencesUtils sharedPreferencesUtils = SharedPreferencesUtils .getInstance(context);
        keyValues.put(Param.Latitude,  Double.toString(location.getLatitude()));
        keyValues.put(Param.Longitude,  Double.toString(location.getLongitude()));
        keyValues.put(Param.updatedAt, sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.updatedAt));


        APIUrl = APIValues.Links.allInfo_getUpdates;
    }

}
