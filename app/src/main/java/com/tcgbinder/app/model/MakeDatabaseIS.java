package com.tcgbinder.app.model;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Looper;
import android.util.JsonReader;
import android.util.Log;

import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MakeDatabaseIS extends IntentService {
    private static final String TAG = "##MakeDatabaseIS";

    public MakeDatabaseIS() {
        super("MakeDatabaseIS");
    }

    protected void onHandleIntent(Intent intent) {
//        Log.i(TAG, "444444444444444444444444444444");
//        int tableQuantity = Motherbase.getInstance(getBaseContext()).getCardInfoTable().getQuantity();
//        int quantityOnFile = getQuantityOfFile();
//        Log.i(TAG, "55555555555555555555555555555555");
//
//        Log.i(TAG, "QUANTITY: " + getQuantityOfFile() + " " + tableQuantity);
//        if(tableQuantity < getQuantityOfFile()){
//
//            Log.i(TAG, "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK");
//            Motherbase.getInstance(getBaseContext()).getCardInfoTable().readFilesTable(getBaseContext());
//        }
    }

    public int getQuantityOfFile() {

            InputStream is = null;
            int quantity = 0;

        try {
                is = getBaseContext().getAssets().open("cards.json");
                JsonReader reader = new JsonReader(new InputStreamReader(is, "UTF-8"));

                reader.beginArray();


                while (reader.hasNext()) {
                    quantity++;

                    String colors = "";
                    int foil = 0;
                    String _id = "";
                    String image_normal = "";
                    String name = "";
                    int nonfoil = 0;
                    String set_code = "";
                    String set_name = "";
                    String scryfall_uri = "";

                    reader.beginObject();
                    while (reader.hasNext()) {
                        String key = reader.nextName();
                        if (key.equals("id")) {
                            _id = reader.nextString();
                        } else if (key.equals("name")) {
                            name = reader.nextString();
                        } else if (key.equals("colors")) {
                            reader.beginArray();
                            StringBuilder sb = new StringBuilder("");
                            while (reader.hasNext()) {
                                sb.append(reader.nextString());
                            }
                            colors = sb.toString();

                            reader.endArray();
                        } else if (key.equals("foil")) {
                            if (reader.nextBoolean()) {
                                foil = 1;
                            }
                        } else if (key.equals("nonfoil")) {
                            if (reader.nextBoolean()) {
                                nonfoil = 1;
                            }
                        } else if (key.equals("set")) {
                            set_code = reader.nextString();
                        } else if (key.equals("set_name")) {
                            set_name = reader.nextString();
                        } else if (key.equals("scryfall_uri")) {
                            scryfall_uri = reader.nextString();
                        } else if (key.equals("image_uris")) {
                            reader.beginObject();
                            while (reader.hasNext()) {
                                if (reader.nextName().equals("normal")) {
                                    image_normal = reader.nextString();
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                        } else {
                            reader.skipValue();
                        }
                    }
                    reader.endObject();

                }

                reader.endArray();

                reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return quantity;
    }
}
