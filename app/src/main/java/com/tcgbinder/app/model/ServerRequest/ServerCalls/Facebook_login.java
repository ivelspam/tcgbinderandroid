package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class Facebook_login extends APIRequest{

    public class ResponseFields {
    }

    public class Param {
        public final static String
                facebookUserId = "facebookUserId",
                facebookUserToken = "facebookUserToken";
    }

    public class Response {
        public final static String
                synced = "synced",
                foundUser = "foundUser",
                created = "created";
    }

    public Facebook_login(String facebookUserId, String facebookUserToken){
        keyValues.put(Param.facebookUserId, facebookUserId);
        keyValues.put(Param.facebookUserToken, facebookUserToken);
        APIUrl = APIValues.Links.facebook_login;
        token = false;
    }

}
