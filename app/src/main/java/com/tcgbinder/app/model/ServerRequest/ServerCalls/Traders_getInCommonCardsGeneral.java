package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class Traders_getInCommonCardsGeneral extends APIRequest {
    private static final String TAG = "##Traders_getInCommonCardsGeneral";

    public class ResponseFields {
        public final static String cards = "cards";
    }

    public class Param {
        public final static String trader_id = "trader_id";
    }

    public class Response {
        public final static String done = "done";
    }

    public Traders_getInCommonCardsGeneral(String trader_id){
        keyValues.put(Param.trader_id, trader_id);
        APIUrl = APIValues.Links.traders_getWantedInCommonGeneral;
    }
}
