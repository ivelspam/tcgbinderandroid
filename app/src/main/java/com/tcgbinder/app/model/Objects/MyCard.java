package com.tcgbinder.app.model.Objects;

import android.database.Cursor;

import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyCardsTable;

public class MyCard {

    String colors;
    boolean foil = false;
    int foilQuantity = 0;
    String _id;
    String imageNormal;
    String name;
    boolean nonfoil = false;
    int nonfoilQuantity = 0;
    String set_code;
    String set_name;
    String scryfall_uri;

    public MyCard(Cursor cursor){

        colors = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_colors));
        if(cursor.getInt(cursor.getColumnIndex(CardInfoTable.KEY_foil)) == 1){ foil = true; }
        if(!cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity))){ nonfoilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_foilQuantity)); }
        _id = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_id));
        imageNormal = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_image_normal));
        name = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_name));
        if(cursor.getInt(cursor.getColumnIndex(CardInfoTable.KEY_nonfoil)) == 1){ nonfoil = true; }
        if(!cursor.isNull(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity))){ foilQuantity = cursor.getInt(cursor.getColumnIndex(MyCardsTable.KEY_nonFoilQuantity)); }
        set_code = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_set_code));
        set_name = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_set_name));
        scryfall_uri = cursor.getString(cursor.getColumnIndex(CardInfoTable.KEY_scryfall_uri));
    }

}
