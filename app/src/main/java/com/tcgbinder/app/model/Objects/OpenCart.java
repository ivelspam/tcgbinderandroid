package com.tcgbinder.app.model.Objects;

import com.tcgbinder.app.model.Constants.MongoKeys;

import org.json.JSONException;
import org.json.JSONObject;

public class OpenCart {
    String _id;
    int giving;
    String name;
    int receiving;

    public OpenCart(JSONObject jsonObject) {
        try {
            _id = jsonObject.getString(MongoKeys._id);
            name = jsonObject.getString(MongoKeys.name);
            giving = jsonObject.getInt("giving");
            receiving = jsonObject.getInt("receiving");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getName() {
        return name;
    }

    public String get_id() {
        return _id;
    }

    public int getGiving() {
        return giving;
    }

    public int getReceiving() {
        return receiving;
    }
}
