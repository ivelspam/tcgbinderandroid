package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

import java.io.File;

public class UserCards_addUpdateCards extends APIRequest {
    public class ResponseFields {

    }

    public class Param {
        public static final String
                binder_id = "binder_id",
                card_id = "card_id",
                foilQuantity = MongoKeys.foilQuantity,
                nonfoilQuantity = MongoKeys.nonfoilQuantity;
    }

    public class Response {
        public static final String
                added = "added",
                updated = "updated";

    }



    public UserCards_addUpdateCards(String card_id, int foilQuantity, int nonfoilQuantity){
        keyValues.put(Param.card_id, card_id);
        keyValues.put(Param.foilQuantity, foilQuantity);
        keyValues.put(Param.nonfoilQuantity, nonfoilQuantity);

        APIUrl = APIValues.Links.userCards_addUpdateCards;

    }
}
