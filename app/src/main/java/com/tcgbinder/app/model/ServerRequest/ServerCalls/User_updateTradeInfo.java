package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class User_updateTradeInfo extends APIRequest {

    public class Param {
        public static final String
                tradeInfo = "tradeInfo";
    }

    public class Response {
        public static final String
                updated = "updated";
    }

    public class ResponseFields {

    }

    public User_updateTradeInfo(String tradeInfo){
        keyValues.put(Param.tradeInfo, tradeInfo);
          APIUrl = APIValues.Links.user_updateTradeInfo;
    }
}
