package com.tcgbinder.app.model.SQLiteTable;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.ChatMessageTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyBindersTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyCardsTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyWantedGeneralTable;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.MyWantedTable;


public class Motherbase extends SQLiteOpenHelper {
    static private String TAG = "##Motherbase";

    private static Motherbase sMotherbase;

    private static final String  DATABASE_NAME = "tcgbinder.db",
            TABLE_NAME = "chatMessage";

    Context mContext;
    private Motherbase(Context context) {
        super(context, DATABASE_NAME, null, 1);
        mContext = context;
    }

    public static Motherbase getInstance(Context context) {
        if (sMotherbase == null) {
            sMotherbase = new Motherbase(context.getApplicationContext());
        }
        return sMotherbase;
    }


    @Override
    public void onCreate(SQLiteDatabase database) {
//        database.execSQL("DROP TABLE IF EXISTS " + FriendTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + ChatTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + EventTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + HobbyTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + HobbyClosureTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + AppUserHobbyTable.TABLE_NAME);


//        database.execSQL(FriendTable.TABLE_CREATION);
//        database.execSQL(ChatTable.TABLE_CREATION);
//        database.execSQL(EventTable.TABLE_CREATION);
//        database.execSQL(HobbyTable.TABLE_CREATION);
//        database.execSQL(HobbyClosureTable.TABLE_CREATION);
//        database.execSQL(AppUserHobbyTable.TABLE_CREATION);

    }

    public void resetDatabase(){
        onCreate(getWritableDatabase());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public CardInfoTable getCardInfoTable(){
        return CardInfoTable.getInstance(getWritableDatabase());
    }

    public MyCardsTable getMyCardsTable(){
        return MyCardsTable.getInstance(getWritableDatabase());
    }

    public MyBindersTable getMyBindersTable(){
        return MyBindersTable.getInstance(getWritableDatabase());
    }
    public MyWantedTable getMyWantedCardsTable(){
        return MyWantedTable.getInstance(getWritableDatabase());
    }

    public MyWantedGeneralTable getMyWantedGeneralTable(){
        return MyWantedGeneralTable.getInstance(getWritableDatabase());
    }


    public ChatMessageTable getChatMessageTable(){
        return ChatMessageTable.getInstance(getWritableDatabase());
    }

    public void rebuildDatabase(){
//        CardInfoTable.getInstance(getWritableDatabase()).rebuildTable();
        MyBindersTable.getInstance(getWritableDatabase()).rebuildTable();
        MyCardsTable.getInstance(getWritableDatabase()).rebuildTable();
        MyWantedTable.getInstance(getWritableDatabase()).rebuildTable();
        MyWantedGeneralTable.getInstance(getWritableDatabase()).rebuildTable();
//        ChatMessageTable.getInstance(getWritableDatabase()).rebuildTable();

    }

    public void createTableIfDontExists(){
        CardInfoTable.getInstance(getWritableDatabase()).createTable();
        MyBindersTable.getInstance(getWritableDatabase()).createTable();
        MyCardsTable.getInstance(getWritableDatabase()).createTable();
        MyWantedTable.getInstance(getWritableDatabase()).createTable();
        ChatMessageTable.getInstance(getWritableDatabase()).createTable();
        MyWantedGeneralTable.getInstance(getWritableDatabase()).createTable();

    }

    public void clearDatabaseForNewInfo(){
//        ChatTable.getInstance(getWritableDatabase()).rebuildTable();
//        FriendTable.getInstance(getWritableDatabase()).rebuildTable();
//        EventTable.getInstance(getWritableDatabase()).rebuildTable();
//        AppUserHobbyTable.getInstance(getWritableDatabase()).rebuildTable();
    }

    public void restartDatabase(){
        rebuildDatabase();
    }


}
