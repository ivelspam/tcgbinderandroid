package com.tcgbinder.app.model.ServerRequest;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.auth0.android.jwt.JWT;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.TokenValues;
import com.tcgbinder.app.model.Extensions.DateExtension;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class SharedPreferencesUtils {

    private static final String SETTINGS_NAME = "userInfo",
                                TAG = "##SharedPreferencesU";

    static private SharedPreferencesUtils sSharedPreferencesUtils;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean mBulkUpdate = false;

    public static SharedPreferencesUtils getInstance(Context context){

        if(sSharedPreferencesUtils == null){
            sSharedPreferencesUtils = new SharedPreferencesUtils(context.getApplicationContext());
        }
        return sSharedPreferencesUtils;

    }

    private SharedPreferencesUtils(Context context){
        mSharedPreferences = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
    }

    public enum PrefKeys{
        accountStatus,
        distance,
        email,
        _id,
        imageVersion,
        openForTrading,
        name,
        measurementSystem,
        token,
        sharingAllCards,
        updatedAt,
        tradeInfo


    }


    //PUTTERS
    public void put(PrefKeys prefKeys, String val) {
        doEdit();
        mEditor.putString(prefKeys.name(), val);
        doCommit();
    }

    public void put(PrefKeys prefKeys, Boolean val) {
        doEdit();
            mEditor.putBoolean(prefKeys.name(), val);
        doCommit();
    }

    public void put(PrefKeys prefKeys, int val) {
        doEdit();
        mEditor.putInt(prefKeys.name(), val);
        doCommit();
    }

    //GETTERS
    public String getString(PrefKeys prefKeys) {
        return mSharedPreferences.getString(prefKeys.name(), null);
    }

    public String getString(PrefKeys prefKeys, String defValeu) {
        return mSharedPreferences.getString(prefKeys.name(), defValeu);
    }

    public int getInt(PrefKeys prefKeys) {
        return mSharedPreferences.getInt(prefKeys.name(), -1);
    }

    public int getInt(PrefKeys prefKeys, int defValue) {
        return mSharedPreferences.getInt(prefKeys.name(), defValue);
    }

    public boolean getBoolean(PrefKeys prefKeys) {
        return mSharedPreferences.getBoolean(prefKeys.name(), false);
    }

    public void clear() {
        doEdit();
        mEditor.clear();
        doCommit();
    }

    public boolean contains(PrefKeys prefKey){
        return mSharedPreferences.contains(prefKey.name());
    }


    public void delete(PrefKeys prefKeys) {
        doEdit();
        mEditor.remove(prefKeys.name());
        doCommit();

    }

    private void doEdit() {
        if (!mBulkUpdate && mEditor == null) {
            mEditor = mSharedPreferences.edit();
        }
    }

    private void doCommit() {
        if (!mBulkUpdate && mEditor != null) {
            mEditor.commit();
            mEditor = null;
        }
    }

    public String toString(){


        Map<String,?> keys = mSharedPreferences.getAll();
        StringBuilder stringBuilder = new StringBuilder("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        stringBuilder.append("SharedPreferences Values: \n");

        for(Map.Entry<String,?> entry : keys.entrySet()){
            stringBuilder.append(entry.getKey() + ": " + entry.getValue().toString() + "\n");
        }
        stringBuilder.append("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        return stringBuilder.toString();
    }

   public void updateUser(JSONObject resultJSONObject, String token) throws JSONException {

        if(token != null){
            JWT jwt = new JWT(token);
            put(PrefKeys._id, jwt.getClaim(TokenValues._id).asString());
            put(PrefKeys.accountStatus, jwt.getClaim(TokenValues.accountStatus).asString());
            put(PrefKeys.accountStatus, token);
        }

        if(resultJSONObject != null){
            if(resultJSONObject.has(MongoKeys.distance)){
                put(PrefKeys.distance, resultJSONObject.getInt(MongoKeys.distance));
            }

            if(resultJSONObject.has(MongoKeys.email)){
                put(PrefKeys.email, resultJSONObject.getString(MongoKeys.email));
            }

            if(resultJSONObject.has(MongoKeys.imageVersion)){
                put(PrefKeys.imageVersion, resultJSONObject.getInt(MongoKeys.imageVersion));
            }

            if(resultJSONObject.has(MongoKeys.measurementSystem)){
                put(PrefKeys.measurementSystem, resultJSONObject.getString(MongoKeys.measurementSystem));
            }

            if(resultJSONObject.has(MongoKeys.name)){
                put(PrefKeys.name, resultJSONObject.getString(MongoKeys.name));
            }
            if(resultJSONObject.has(MongoKeys.openForTrading)){
                put(PrefKeys.openForTrading, resultJSONObject.getBoolean(MongoKeys.openForTrading));
            }

            if(resultJSONObject.has(MongoKeys.sharingAllCards)){
                put(PrefKeys.sharingAllCards, resultJSONObject.getBoolean(MongoKeys.sharingAllCards));
            }

            if(resultJSONObject.has(MongoKeys.updatedAt)){
                put(PrefKeys.updatedAt, resultJSONObject.getString(MongoKeys.updatedAt));
            }

            if(resultJSONObject.has(MongoKeys.tradeInfo)){
                put(PrefKeys.tradeInfo, resultJSONObject.getString(MongoKeys.tradeInfo));
            }
        }

    }

}
