package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class UserCards_addUpdateCardsToWantedGeneral extends APIRequest {
    public class ResponseFields {

    }

    public class Param {
        public static final String
                name = "name",
                foilQuantity = MongoKeys.foilQuantity,
                nonfoilQuantity = MongoKeys.nonfoilQuantity;;
    }

    public class Response {
        public static final String
                updatedWantedGeneral = "updatedWantedGeneral";
    }



    public UserCards_addUpdateCardsToWantedGeneral(String card_id, int foilQuantity, int nonfoilQuantity){
        keyValues.put(Param.name, card_id);
        keyValues.put(Param.foilQuantity, foilQuantity);
        keyValues.put(Param.nonfoilQuantity, nonfoilQuantity);
        APIUrl = APIValues.Links.userCards_addUpdateCardsToWantedGeneral;

    }
}
