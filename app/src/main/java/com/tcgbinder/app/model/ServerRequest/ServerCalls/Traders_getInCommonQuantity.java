package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class Traders_getInCommonQuantity extends APIRequest {
    private static final String TAG = "##Traders_getInCommonQuantity";

    public class ResponseFields {
        public final static String  info = "info";
    }

    public class Param {
        public final static String  trader_id = "trader_id";
    }

    public class Response {
        public final static String
                found = "found",
                nonFound = "notFound";
    }

    public Traders_getInCommonQuantity(String trader_id){
        keyValues.put(Param.trader_id, trader_id);
        APIUrl = APIValues.Links.traders_getInCommonQuantity;
    }


}
