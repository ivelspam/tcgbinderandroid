package com.tcgbinder.app.model.Constants;

public class APIValues {

    public class Links {
        public static final String
            allInfo_getAllInfo = "allInfo_getAllInfo",
            allInfo_getUpdates = "allInfo_getUpdates",
            allInfo_updates = "allInfo_getUpdates",

            appuser_checkIfExists = "appuser_checkIfExists",
            appuser_findByAppUserID = "appuser_findByAppUserID",
            appuser_getImageVersion = "appuser_getImageVersion",
            appuser_UpdateDistanceAndMeasurementSystem = "appuser_UpdateDistanceAndMeasurementSystem",

            cardsInfo_getPrice = "cardsInfo_getPrice",


            cart_getCards = "cart_getCards",
            cart_getOpen = "cart_getOpen",

            chatRoom_getNewFromUser = "chatRoom_getNewFromUser",

            email_addUser = "email_addUser",
            email_isConfirmed = "email_isConfirmed",
            email_sendAnotherToken = "Email_sendAnotherToken",

            fcmToken_removeTokenFromSystem = "fcmToken_removeTokenFromSystem",

            email_login = "email_login",

            facebook_login = "facebook_login",

            google_login = "google_login",

            logout_clear = "logout_clear",


        upload_profilePicture = "upload_profilePicture",

            traders_getByDistance = "traders_getByDistance",
            traders_getCardsByPage = "traders_getCardsByPage",
            traders_getInCommonCards = "traders_getInCommonCards",
            traders_getWantedInCommonGeneral = "traders_getWantedInCommonGeneral",
            traders_getInCommonQuantity = "traders_getInCommonQuantity",
            traders_getAllInfo = "traders_getAllInfo",
            traders_getCloserTradersAndOpenCarts = "traders_getCloserTradersAndOpenCarts",
            traders_getCloserTradersWithInfoAndCart = "traders_getCloserTradersWithInfoAndCart",

            user_UpdateDistanceAndMeasurementSystem = "user_UpdateDistanceAndMeasurementSystem",
            user_openForTrading = "user_openForTrading",
            user_updateLocation = "user_updateLocation",
            user_findAUserByUsername = "user_findAUserByUsername",
            user_updateTradeInfo = "user_updateTradeInfo",
            user_deleteAccount = "user_deleteAccount",


            userCards_getTraderCardsByToken = "userCards_getTraderCardsByToken",
            userCards_addUpdateCards = "UserCards_addUpdateCards",
            userCards_addUpdateCardsToWantedGeneral = "UserCards_addUpdateCardsToWantedGeneral",
            userCards_addUpdateCardsToWanted = "UserCards_addUpdateCardsToWanted",
            userCards_deleteWantedGeneralCard = "UserCards_deleteWantedGeneralCard",
            userCards_updateMeasurementAndDistance = "userCards_updateMeasurementAndDistance",
            userCards_removeCard = "UserCards_deleteCard",
            userCards_deleteWantedCard = "UserCards_deleteWantedCard",
            userCards_addBinder = "UserCards_addBinder",
            userCards_setCardToBinder = "UserCards_setCardToBinder",
            userCards_setAlwaysShare = "UserCards_setAlwaysShare",
            userCards_getAllUserCards = "userCards_getAllUserCards";

    }
}
