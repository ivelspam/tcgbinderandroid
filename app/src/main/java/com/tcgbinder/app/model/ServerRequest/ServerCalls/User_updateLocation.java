package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Location.LocationUtils;

public class User_updateLocation extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public final static String
                latitude = "latitude",
                longitude = "longitude";
    }

    public class Response {
        public final static String
                changed = "changed";
    }

    public User_updateLocation(Context context){

        Location location = LocationUtils.getLastKnownLocation(context);

        keyValues.put(Param.latitude, location.getLatitude());
        keyValues.put(Param.longitude, location.getLongitude());

        APIUrl = APIValues.Links.user_updateLocation;
    }

}
