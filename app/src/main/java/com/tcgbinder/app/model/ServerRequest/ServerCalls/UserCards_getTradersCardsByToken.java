package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class UserCards_getTradersCardsByToken extends APIRequest {

    public class ResponseFields {
        public static final String
                cards = "cards";
    }

    public class Param {
        public static final String
                token = "token",
                trader_id = "trader_id";
    }

    public class Response {
        public static final String
                done = "done";

    }

    public UserCards_getTradersCardsByToken(String token, String trader_id){
        keyValues.put(Param.token, token);
        keyValues.put(Param.trader_id, trader_id);

        APIUrl = APIValues.Links.userCards_getTraderCardsByToken;
        this.token = false;
    }
}
