package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;

public class Email_addUser extends APIRequest {

    public class ResponseFields {
        public final static String
                appuseremailuser = "alreadyExists",
                token = "token";
    }

    public class Param {
        public final static String
                email = MongoKeys.email,
                name = MongoKeys.name,
                password = MongoKeys.password;
    }

    public class Response {
        public final static String
                accountExists = "accountExists",
                registered = "registered";
    }

    public Email_addUser(String name, String email, String password) {
        token = false;
        keyValues.put(Param.email, email);
        keyValues.put(Param.name, name);
        keyValues.put(Param.password, password);
        APIUrl = APIValues.Links.email_addUser;
        token = false;
    }
}
