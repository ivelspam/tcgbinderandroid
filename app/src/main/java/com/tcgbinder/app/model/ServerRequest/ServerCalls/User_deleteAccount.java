package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;
public class User_deleteAccount extends APIRequest {

    public class Param {}

    public class Response {
        public static final String
                accountDeleted = "accountDeleted";
    }

    public class ResponseFields {}

    public User_deleteAccount(){
        APIUrl = APIValues.Links.user_deleteAccount;
    }
}
