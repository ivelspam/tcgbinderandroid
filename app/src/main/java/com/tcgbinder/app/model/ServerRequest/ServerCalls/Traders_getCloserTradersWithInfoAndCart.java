package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.tcgbinder.app.model.Constants.APIValues;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class Traders_getCloserTradersWithInfoAndCart extends APIRequest {

    public class ResponseFields {
        public final static String
                closerTraders = "closerTraders",
                setCardsQuantity = "setCardsQuantity",
                anyCardsQuantity = "anyCardsQuantity",
                openCarts = "openCarts";
    }

    public class Param {
        public final static String
            distance = MongoKeys.distance,
            latitude = MongoKeys.latitude,
            longitude = MongoKeys.longitude;
    }

    public class Response {
        public final static String
                gotAllTradersInfo = "gotAllTradersInfo";
    }

    public Traders_getCloserTradersWithInfoAndCart(Context context){

        Location location = LocationUtils.getLastKnownLocation(context);
        keyValues.put(Param.latitude,  location.getLatitude());
        keyValues.put(Param.longitude, location.getLongitude());
        keyValues.put(Param.distance, SharedPreferencesUtils.getInstance(context).getInt(SharedPreferencesUtils.PrefKeys.distance));

        APIUrl = APIValues.Links.traders_getCloserTradersWithInfoAndCart;
    }

}
