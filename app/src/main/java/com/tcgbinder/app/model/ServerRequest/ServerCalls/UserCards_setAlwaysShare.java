package com.tcgbinder.app.model.ServerRequest.ServerCalls;

import com.tcgbinder.app.model.Constants.APIValues;

public class UserCards_setAlwaysShare extends APIRequest{


    public class ResponseFields {}

    public class Param {
        public final static String
                binder_id = "binder_id",
                valueBoolean = "valueBoolean";
    }

    public class Response {
        public final static String
                globalModified = "globalModified",
                binderModified = "binderModified";
    }

    public UserCards_setAlwaysShare(String binder_id, boolean alwaysShare){
        keyValues.put(Param.binder_id, binder_id);
        keyValues.put(Param.valueBoolean, alwaysShare);

        APIUrl = APIValues.Links.userCards_setAlwaysShare;

    }

}
