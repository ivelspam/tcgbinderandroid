package com.tcgbinder.app.controller.MainActivity.Traders.Trade.UserCards;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivityInterface;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_getTradersCardsByToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TraderCardsFragment extends Fragment implements TabLayout.OnTabSelectedListener {
    private final String TAG = "##" + this.getClass().getSimpleName();

    private TabLayout vTL;
    private ConstraintLayout containerCL;
    private UserCardsFilteredFragment mUserCardsFilteredFragment = new UserCardsFilteredFragment();
    private UserCardsPagination mUserCardsPagination = new UserCardsPagination();

    private TradeActivityInterface mTradeActivityInterface;

    public TraderCardsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserCardsPagination.setListenerer(mTradeActivityInterface);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trader_cards, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        build_view();
    }

    private void build_view(){
        vTL = getView().findViewById(R.id.fragmentTraderCards_TL);
        containerCL = getView().findViewById(R.id.fragmentTraderCards_containerCL);

        vTL.addTab(vTL.newTab().setText("Search"));
        vTL.addTab(vTL.newTab().setText("All Cards"));

        vTL.addOnTabSelectedListener(this);
        getChildFragmentManager().beginTransaction().replace(R.id.fragmentTraderCards_containerCL, mUserCardsFilteredFragment).commit();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getText().toString()){
            case "Search":
                getChildFragmentManager().beginTransaction().replace(R.id.fragmentTraderCards_containerCL, mUserCardsFilteredFragment).commit();
                break;
            case "All Cards":
                getChildFragmentManager().beginTransaction().replace(R.id.fragmentTraderCards_containerCL, mUserCardsPagination).commit();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

}
