package com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;

public class FilterWantedGeneralATL extends AsyncTaskLoader<List<WantedCard>> {
    private static final String TAG = "##FilterCardATL";

    String mToken;

    public FilterWantedGeneralATL(Context context, String token) {
        super(context);
        mToken = token;
    }

    @Override
    public List<WantedCard> loadInBackground() {

        List<WantedCard> temp = Motherbase.getInstance(getContext()).getCardInfoTable().selectMyWantedGeneralByToken(mToken);
        return temp;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
