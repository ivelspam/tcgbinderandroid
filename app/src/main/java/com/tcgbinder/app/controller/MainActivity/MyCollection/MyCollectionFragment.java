package com.tcgbinder.app.controller.MainActivity.MyCollection;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog.AddCardDialogFragment;
import com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog.AddCardDialogInterface;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivity;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_getAllUserCards;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;


public class MyCollectionFragment extends Fragment implements View.OnClickListener,CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener, LoaderManager.LoaderCallbacks, AddCardDialogInterface {
    private static final String TAG = "##MyCollectionFragment";

    private final int LOADER_Cards = 2;
    private final int LOADER_WantedCards = 3;


    private SwipeRefreshLayout vWrapperSRL;
    private Button vAddCardBT;
    private EditText filterET;
    private RecyclerView vCardsRV;
    private TextView vFilterCardsTV;
    private ConstraintLayout vFilterOptionsCL;
    private RadioGroup vCardsOptionsRG;
    private CardsAA mCardsAA;
    private TextView vTotalValue, vTotalNonfoil;
    private Switch vAlwaysShareSWT;

    LoaderManager mLoaderManager;

    public MyCollectionFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoaderManager = getLoaderManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_collection_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildView();
    }

    public void startMyAsynctask(View view){
        mLoaderManager.initLoader(1, null, this);
    }

    private void buildView(){
        vAddCardBT = getView().findViewById(R.id.fragmentMyCollection_addCardBT);
        vFilterCardsTV = getView().findViewById(R.id.fragmentMyCollection_filterCardsTV);
        vFilterOptionsCL = getView().findViewById(R.id.fragmentMyCollection_filterOptionsCL);
        vCardsRV = getView().findViewById(R.id.fragmentMyCollection_cardsRV);
        vCardsOptionsRG = getView().findViewById(R.id.fragmentMyCollection_cardsOptionsRG);
        vTotalValue = getView().findViewById(R.id.fragmentMyCollection_totalValueTV);
        vTotalNonfoil = getView().findViewById(R.id.fragmentMyCollection_totalNonfoilTV);
        vAlwaysShareSWT = getView().findViewById(R.id.fragmentMyCollection_alwaysShareSWT);
        vWrapperSRL = getView().findViewById(R.id.fragmentMyCollection_wrapperSRL);

        //INITIAL STATE
        updateStates();

        //Listeners
        vFilterCardsTV.setOnClickListener(this);
        vAddCardBT.setOnClickListener(this);
        vCardsOptionsRG.setOnCheckedChangeListener(this);
        vAlwaysShareSWT.setOnCheckedChangeListener(this);

        vWrapperSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetAllCardsUpdateAT().execute();
            }
        });

        //Adapters
        mCardsAA = new CardsAA(getContext(), CardsAA.OptionEnum.all, this);
        vCardsRV.setAdapter(mCardsAA);
        vCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));

        //Loaders
        mLoaderManager.restartLoader(LOADER_Cards, null, MyCollectionFragment.this);

    }

    //VIEW STATES

    private void updateStates(){
        addCardState();
        updateQuantityAndPrices();
        updateShareState();

    }

    private void addCardState(){
        if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_allCardsRB){
            vAddCardBT.setEnabled(true);
        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedRB){
            vAddCardBT.setEnabled(true);
        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedGeneralRB){
            vAddCardBT.setEnabled(true);
        }
    }

    private void updateQuantityAndPrices(){

        if(mCardsAA == null){
            return;
        }

        double total = 0;
        int totalNonfoil = 0;
        switch (vCardsOptionsRG.getCheckedRadioButtonId()){
            case R.id.fragmentMyCollection_allCardsRB:
            case R.id.fragmentMyCollection_wantedRB:
                DecimalFormat df = new DecimalFormat("0.00");
                for(int i = 0; i < mCardsAA.getmCardsInfo().size(); i++){
                    totalNonfoil += mCardsAA.getmCardsInfo().get(i).getNonfoilQuantity();

                    if(mCardsAA.getmCardsInfo().get(i).getUsd() != null){
                        total += mCardsAA.getmCardsInfo().get(i).getNonfoilQuantity() * mCardsAA.getmCardsInfo().get(i).getUsd();
                    }
                }
                vTotalValue.setText("Total: $" + df.format( total));
                vTotalNonfoil.setText("Total Nonfoil: " + totalNonfoil);
                break;
            case R.id.fragmentMyCollection_wantedGeneralRB:
                for(int i = 0; i < mCardsAA.getmWantedCards().size(); i++){
                    totalNonfoil += mCardsAA.getmWantedCards().get(i).getNonfoilQuantity();
                }
                vTotalNonfoil.setText("Total Nonfoil: " + totalNonfoil);
        }

    }

    private void updateShareState(){
        if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_allCardsRB){
//            vAlwaysShareSWT.setEnabled(true);
//            vAlwaysShareSWT.setChecked(SharedPreferencesUtils.getInstance(getContext()).getBoolean(SharedPreferencesUtils.PrefKeys.sharingAllCards));

        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedRB){
//            vAlwaysShareSWT.setVisibility(View.GONE);
        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedGeneralRB){
//            vAlwaysShareSWT.setVisibility(View.GONE);
        }
    }

    //LISTENERES+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_allCardsRB){
            vTotalValue.setVisibility(View.VISIBLE);
        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedRB){
            vTotalValue.setVisibility(View.VISIBLE);
        }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedGeneralRB){
            vTotalValue.setVisibility(View.GONE);
        }

        mLoaderManager.restartLoader(LOADER_Cards, null, MyCollectionFragment.this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean valueBoolean) {

    }

    @Override
    public void onClick(View view) {

        if(view == vFilterCardsTV){
            ViewGroup.LayoutParams params = vFilterOptionsCL.getLayoutParams();

            if(params.height != ViewGroup.LayoutParams.WRAP_CONTENT){
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                vFilterOptionsCL.setLayoutParams(params);
            }else{
                params.height = 0;
                vFilterOptionsCL.setLayoutParams(params);
            }
        }else if (view == vAddCardBT){

            Bundle args = new Bundle();
            CardsAA.OptionEnum optionEnum = CardsAA.OptionEnum.searchAll;

            if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_allCardsRB){
                vAlwaysShareSWT.setEnabled(false);
                optionEnum = CardsAA.OptionEnum.searchAll;
            }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedRB){
                optionEnum = CardsAA.OptionEnum.searchWanted;
            }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedGeneralRB){
                optionEnum = CardsAA.OptionEnum.searchWantedGeneral;
            }

            args.putString("OptionEnum", optionEnum.toString());

            AddCardDialogFragment addCardDialogFragment = new AddCardDialogFragment();
            addCardDialogFragment.setArguments(args);
            addCardDialogFragment.setTargetFragment(MyCollectionFragment.this, 0);
            addCardDialogFragment.show(getFragmentManager(), "custom");
        }
    }

    //LOADER+++++++++++++++++++++++++++++++++++++++++++++++++

    @NonNull
    @Override
    public Loader onCreateLoader(int id, @Nullable Bundle args) {
        int option = 0;
        switch (id){
            case (LOADER_Cards) :
                if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_allCardsRB){
                    option = GetCardsATL.TYPE_ALLCARDS;
                    mCardsAA.setmOptionEnum(CardsAA.OptionEnum.all);
                }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedRB){
                    mCardsAA.setmOptionEnum(CardsAA.OptionEnum.wanted);
                    option = GetCardsATL.TYPE_WANTED;
                }else if(vCardsOptionsRG.getCheckedRadioButtonId() == R.id.fragmentMyCollection_wantedGeneralRB){
                    mCardsAA.setmOptionEnum(CardsAA.OptionEnum.wantedGeneral);
                    return new GetWantedGeneralCardsATL(getContext());
                }
        }
        return new GetCardsATL(getContext(), option);
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {

        switch (vCardsOptionsRG.getCheckedRadioButtonId()){
            case R.id.fragmentMyCollection_allCardsRB:
            case R.id.fragmentMyCollection_wantedRB:
                mCardsAA.updateListAA((List<CardInfo>) data);
                break;

            case R.id.fragmentMyCollection_wantedGeneralRB:
                mCardsAA.updateWantedListAA((List<WantedCard>) data);

        }
        updateStates();
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        switch (loader.getId()){
            case (LOADER_Cards) :
                mCardsAA.clearCardsAA();
            break;
        }
    }

    //ASYNCTASKS
    private class GetAllCardsUpdateAT extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {

            JSONObject jsonObject = new ServerAPI(getContext()).serverRequestGET(new UserCards_getAllUserCards());

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case UserCards_getAllUserCards.Response.gotAllCards:
                        Motherbase.getInstance(getContext()).getMyWantedGeneralTable().insertOrReplaceAllCards(jsonObject.getJSONArray(MongoKeys.wantedCards));
                        Motherbase.getInstance(getContext()).getMyCardsTable().insertOrReplaceAllCards(jsonObject.getJSONArray(MongoKeys.cards));
                        Motherbase.getInstance(getContext()).getMyWantedCardsTable().insertOrReplaceAllCards(jsonObject.getJSONArray(MongoKeys.wanted));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            vWrapperSRL.setRefreshing(false);
            mLoaderManager.restartLoader(LOADER_Cards, null, MyCollectionFragment.this);

        }
    }

    //INTERFACE++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @Override
    public void changeList() {
        mLoaderManager.restartLoader(LOADER_Cards, null, this);
    }

}
