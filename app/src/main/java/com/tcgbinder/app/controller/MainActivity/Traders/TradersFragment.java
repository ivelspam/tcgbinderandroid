package com.tcgbinder.app.controller.MainActivity.Traders;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Objects.OpenCart;
import com.tcgbinder.app.model.Objects.TraderSimple;
import com.tcgbinder.app.model.Objects.TraderWithQuantity;

import java.util.ArrayList;
import java.util.List;

public class TradersFragment extends Fragment implements TradersFragmentInterface, LoaderManager.LoaderCallbacks<TradersFragmentValues> {
    private static final String TAG = "##MyCollectionFragment";

    private TradersFragmentValues mTradersFragmentValues = new TradersFragmentValues();
    private SwipeRefreshLayout vParentSRL;
    private TradersListFragment mTradersListFragment = new TradersListFragment();
    private TradersListWithValuesFragment mTradersListWithValuesFragment = new TradersListWithValuesFragment();
    private OpenCartsFragment mOpenCartsFragment = new OpenCartsFragment();
    private TabLayout vTabsTL;

    public TradersFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_traders, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTradersListWithValuesFragment.insertInterface(this);
        mTradersListFragment.insertInterface(this);
        mOpenCartsFragment.insertInterface(this);
        setupViewPager();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // Add Fragments to Tabs
    private void setupViewPager() {
        ViewPager viewPager  = getView().findViewById(R.id.fragmentTraders_viewPager);
        vTabsTL = getView().findViewById(R.id.fragmentTraders_tabLayout);
        vParentSRL = getView().findViewById(R.id.fragmentTraders_parentSRL);

        //LISTENERS
        vParentSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLoaderManager().restartLoader(0, null, TradersFragment.this);
            }
        });

        //ADAPTERS
        vTabsTL.setupWithViewPager(viewPager);
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(mTradersListWithValuesFragment, "In Common");
        adapter.addFragment(mTradersListFragment, "All Traders");
        adapter.addFragment(mOpenCartsFragment, "Trading");
        viewPager.setAdapter(adapter);
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public List<TraderWithQuantity> getTradersWithQuantities() {
        return mTradersFragmentValues.tradersWithQuantities;
    }

    @Override
    public List<TraderSimple> getTradersSimple() {
        return mTradersFragmentValues.tradersSimples;
    }

    @Override
    public List<OpenCart> getOpenCarts() {
        return mTradersFragmentValues.openCarts;
    }

    @Override
    public Loader<TradersFragmentValues> onCreateLoader(int id, Bundle args) {
        return new GetFragmentTradersValuesATL(getContext());
    }

    @Override
    public void onLoadFinished(Loader<TradersFragmentValues> loader, TradersFragmentValues data) {
        vTabsTL.getTabAt(2).setText("Trading  (" + data.openCarts.size() +")");

        mTradersFragmentValues = data;
        mTradersListFragment.updateView();
        vParentSRL.setRefreshing(false);
        mOpenCartsFragment.updateValues();
        mTradersListWithValuesFragment.updateView();
    }

    @Override
    public void onLoaderReset(Loader<TradersFragmentValues> loader) {

    }

    static class Adapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
