package com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;

public class FilterCardATL extends AsyncTaskLoader<List<CardInfo>> {
    private static final String TAG = "##FilterCardATL";

    String mToken;

    public FilterCardATL(Context context, String token) {
        super(context);
        mToken = token;
    }

    @Override
    public List<CardInfo> loadInBackground() {
        List<CardInfo> temp = Motherbase.getInstance(getContext()).getCardInfoTable().selectByNameToken(mToken);
        return temp;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
