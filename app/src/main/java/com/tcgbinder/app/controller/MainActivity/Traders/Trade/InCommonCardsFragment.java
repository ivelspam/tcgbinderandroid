package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Objects.CardInfo;

import java.util.ArrayList;
import java.util.List;

public class InCommonCardsFragment extends Fragment implements View.OnClickListener{
    private final String TAG = "##" + this.getClass().getSimpleName();

    private TradeActivityInterface mTradeActivityInterface;
    RecyclerView vMyCardsRV, vTradersCardsRV;

//    List<CardInfo> mWantedCards = new ArrayList<>();
//    List<CardInfo> mTradeCards = new ArrayList<>();
    TraderAA mTraderAA = new TraderAA();
    WantedAA mWantedAA;

    public InCommonCardsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_in_common_cards, container, false);
    }

    public void updateValues(){
        if(mTradeActivityInterface == null || mTradeActivityInterface.getTradesActivityValues() == null){
            return;
        }

        mTraderAA.notifyDataSetChanged();
        mWantedAA.updateCards(mTradeActivityInterface.getTradesActivityValues().getmWantedCards());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mWantedAA = new WantedAA(getContext(), mTradeActivityInterface);

        build_view();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTradeActivityInterface = null;
    }

    private void build_view(){
        vMyCardsRV = getView().findViewById(R.id.fragmentInCommonCards_myCardsRV);
        vTradersCardsRV = getView().findViewById(R.id.fragmentInCommonCards_traderCardsRV);


//        ADAPTERS
        vMyCardsRV.setAdapter(mWantedAA);
        vMyCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));


        vTradersCardsRV.setAdapter(mTraderAA);
        vTradersCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        updateValues();
    }

    @Override
    public void onClick(View view) {

    }

    //ADAPTERS++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    class TraderAA extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_trader_card, parent, false);
            return new cardInfoHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((cardInfoHolder) holder).bind(mTradeActivityInterface.getTradesActivityValues().getmTradeCards().get(position), position);
        }

        @Override
        public int getItemCount() {
            return mTradeActivityInterface.getTradesActivityValues().getmTradeCards().size();
        }

        private class cardInfoHolder extends RecyclerView.ViewHolder {

            TextView setCodeTV, nameTV, nonfoilPriceTV, foiPriceTV, nonfoilQuantityET, foilQuantityET;
            CardInfo mCardInfo;
            int mPosition;


            public cardInfoHolder(View itemView) {
                super(itemView);
                setCodeTV = itemView.findViewById(          R.id.adapterRowTraderCards_setCodeIV);
                nameTV = itemView.findViewById(             R.id.adapterRowTraderCards_nameTV);
                foilQuantityET = itemView.findViewById(     R.id.adapterRowTraderCards_foilQuantityET);
                nonfoilQuantityET = itemView.findViewById(  R.id.adapterRowTraderCards_nonfoilQuantityET);
                nonfoilPriceTV = itemView.findViewById(     R.id.adapterRowTraderCards_nonFoilPriceTV);
                foiPriceTV = itemView.findViewById(         R.id.adapterRowTraderCards_foilPriceTV);
            }

            public void bind(final CardInfo cardInfo, final int position) {
                mPosition = position;
                //initial state
                mCardInfo = cardInfo;

                nameTV.setText(cardInfo.getName());
                setCodeTV.setText(cardInfo.getSet_code());
                nonfoilQuantityET.setText(Integer.toString(cardInfo.getNonfoilQuantity()), TextView.BufferType.EDITABLE);

                foilQuantityET.setText(Integer.toString(cardInfo.getFoilQuantity()), TextView.BufferType.EDITABLE);

                if (cardInfo.getUsd() != null) {
                    nonfoilPriceTV.setText(Double.toString(cardInfo.getUsd()));
                }else{
                    nonfoilPriceTV.setText("N/A");
                }
            }
        }
    }

}
