package com.tcgbinder.app.controller.MainActivity.Traders;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivity;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.TraderWithQuantity;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_UpdateDistanceAndMeasurementSystem;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class TradersListWithValuesFragment extends Fragment{
    private static final String TAG = "##TradersListFragment";

    TraderSimpleAA traderSimpleAA = new TraderSimpleAA();
    TradersFragmentInterface mTradersFragmentInterface;


    public TradersListWithValuesFragment() {

    }

    TextView vDistanceTV;
    SeekBar vSeekbar;
    RadioGroup vMeasurementRG;
    int vDistance;
    RecyclerView vListRV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vDistance = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.distance);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_traders_with_values_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildView();

    }

    public void insertInterface(TradersFragmentInterface tradersFragmentInterface){
        mTradersFragmentInterface = tradersFragmentInterface;
    }

    private void buildView() {
        vDistanceTV = getView().findViewById(R.id.fragmentTradersList_distanceTV);
        vMeasurementRG = getView().findViewById(R.id.fragmentTradersList_measurementRG);
        vSeekbar = getView().findViewById(R.id.fragmentTradersList_distanceSB);
        vListRV = getView().findViewById(R.id.fragmentTradersList_traderSimpleRV);

        //INITIAL STATE
//        vMeasurementRG.check(R.id.fragmentTradersList_kmRB);
        String measurement = SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.measurementSystem);
        seekBarState();
        distanceState();

        if (measurement.equals("mi")) {
            vMeasurementRG.check(R.id.fragmentTradersList_miRB);
        } else {
            vMeasurementRG.check(R.id.fragmentTradersList_kmRB);
        }

        distanceState();
        //Listeners
        vMeasurementRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                distanceState();
                seekBarState();
                new UpdateDistanceAndMeasurement().execute();
            }
        });
        vSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int progress = seekBar.getProgress();

                int MIN = 1;
                if (progress < MIN) {
                    vDistance = 1;
                } else {
                    vDistance = progress;
                }
                distanceState();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                new UpdateDistanceAndMeasurement().execute();
            }
        });

        //ADAPTERS
        vListRV.setAdapter(traderSimpleAA);
        vListRV.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    void seekBarState() {
        vSeekbar.setProgress(vDistance);
    }

    void updateView(){
        traderSimpleAA.notifyDataSetChanged();
    }

    void distanceState() {
        if (vMeasurementRG.getCheckedRadioButtonId() == R.id.fragmentTradersList_kmRB) {
            vDistanceTV.setText(Integer.toString((int) Math.round(vDistance * 1.6)));
        } else if (vMeasurementRG.getCheckedRadioButtonId() == R.id.fragmentTradersList_miRB) {
            vDistanceTV.setText(Integer.toString(vDistance));
        }
    }

    //ASYNC TASKS
    class UpdateDistanceAndMeasurement extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {

            int tempDistance = vDistance;
            User_UpdateDistanceAndMeasurementSystem.MeasurementSystem type = User_UpdateDistanceAndMeasurementSystem.MeasurementSystem.mi;
            if (vMeasurementRG.getCheckedRadioButtonId() == R.id.fragmentTradersList_kmRB) {
                type = User_UpdateDistanceAndMeasurementSystem.MeasurementSystem.km;
            }

            JSONObject jsonObject = new ServerAPI(getContext()).serverRequestPOST(new User_UpdateDistanceAndMeasurementSystem(type, Integer.parseInt(vDistanceTV.getText().toString())));

            try {
                switch (jsonObject.getString(APIRequest.message)) {
                    case User_UpdateDistanceAndMeasurementSystem.Response.updated:
                        SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.measurementSystem, type.getValue());
                        SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.distance, tempDistance);

                        break;
                    case User_UpdateDistanceAndMeasurementSystem.Response.notUpdated:

                        break;
                    default:

                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

        }
    }


    class TraderSimpleAA extends RecyclerView.Adapter {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_trader_with_quantities, parent, false);
            return new traderSimpleHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((traderSimpleHolder) holder).bind(mTradersFragmentInterface.getTradersWithQuantities().get(position));

        }

        @Override
        public int getItemCount() {
            return mTradersFragmentInterface.getTradersWithQuantities().size();
        }

        private class traderSimpleHolder extends RecyclerView.ViewHolder {

            TextView nameTV, distanceTV, nonfoilQuantityTV, foilQuantityTV, tradeInfoTV,  anySetNonfoilQuantityWR, anySetFoilQuantityWR;
            ConstraintLayout vParentCL;


            public traderSimpleHolder(View itemView) {
                super(itemView);
                nameTV = itemView.findViewById(R.id.adapterRowTraderSimple_nameTV);
                distanceTV = itemView.findViewById(R.id.adapterRowTraderSimple_distanceTV);
                nonfoilQuantityTV = itemView.findViewById(R.id.adapterRowTraderSimple_nonfoilQuantityTV);
                foilQuantityTV = itemView.findViewById(R.id.adapterRowTraderSimple_foilQuantityTV);
                anySetNonfoilQuantityWR = itemView.findViewById(R.id.adapterRowTraderSimple_anySetNonfoilQuantityTV);
                anySetFoilQuantityWR = itemView.findViewById(R.id.adapterRowTraderSimple_anySetFoilQuantityTV);
                vParentCL = itemView.findViewById(R.id.fragmentTradersList_parentCL);
                tradeInfoTV = itemView.findViewById(R.id.adapterRowTraderSimple_tradeInfoTV);
            }

            public void bind(final TraderWithQuantity traderSimple) {
                nameTV.setText(traderSimple.getName());

                if (vMeasurementRG.getCheckedRadioButtonId() == R.id.fragmentTradersList_kmRB) {
                    if(traderSimple.getDistance() <= 1){
                        distanceTV.setText("Less than " + 2 + " KM.");
                    }else{
                        distanceTV.setText("About " + (int)(traderSimple.getDistance().intValue() * 1.6) + " KM.");
                    }
                } else if (vMeasurementRG.getCheckedRadioButtonId() == R.id.fragmentTradersList_miRB) {
                    if(traderSimple.getDistance() <= 1){
                        distanceTV.setText("Less than " + 1 + " Miles.");
                    }else{
                        distanceTV.setText("About " + traderSimple.getDistance().intValue() + " Mile.");
                    }
                }

                nonfoilQuantityTV.setText("Nonfoil Quantity: " + Integer.toString(traderSimple.getNonfoilQuantity()));
                foilQuantityTV.setText("Foil Quantity: " + Integer.toString(traderSimple.getFoilQuantity()));
                anySetNonfoilQuantityWR.setText("Any Set Nonfoil Quantity: " + Integer.toString(traderSimple.getAnySetNonfoilQuantity()));
                anySetFoilQuantityWR.setText("Any Set Foil Quantity: " + Integer.toString(traderSimple.getAnySetFoilQuantity()));
                tradeInfoTV.setText(traderSimple.getTradeInfo());
                vParentCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), TradeActivity.class);
                        intent.putExtra(MongoKeys._id, traderSimple.get_id());
                        intent.putExtra(MongoKeys.name, traderSimple.getName());
                        startActivityForResult(intent, 0);
                    }
                });
            }
        }
    }

}