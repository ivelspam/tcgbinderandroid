package com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MyCollection.CardsAA;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;

import java.util.List;

public class AddCardDialogFragment extends DialogFragment implements LoaderManager.LoaderCallbacks{
    private static final String TAG = "##AddCardDialogFragment";

    int FIND_NEW_CARDS = 0;
    int FIND_NEW_WANTED_CARDS = 1;

    EditText vFilterET;

    RecyclerView vFoundCardsRV;

    public AddCardDialogInterface mAddCardDialogInterface;
    CardsAA.OptionEnum mOptionEnum;
    CardsAA mCardsAA;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_add_card, container, false);
        mOptionEnum = CardsAA.OptionEnum.valueOf(getArguments().getString("OptionEnum"));

        return view;
    }

    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;
        int height = size.y;

        window.setLayout((int) (width * 1.0), (int) (height * 0.8));
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildView();
    }

    void buildView() {
        vFilterET = getView().findViewById(R.id.dialogFragment_filterET);
        vFoundCardsRV = getView().findViewById(R.id.dialogFragment_foundCardsRV);

        //LISTENERS

        vFilterET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(mOptionEnum == CardsAA.OptionEnum.searchWanted){
                    getLoaderManager().restartLoader(FIND_NEW_WANTED_CARDS, null, AddCardDialogFragment.this);
                }else if(mOptionEnum == CardsAA.OptionEnum.searchAll){
                    getLoaderManager().restartLoader(FIND_NEW_CARDS, null, AddCardDialogFragment.this);
                }else if(mOptionEnum == CardsAA.OptionEnum.searchWantedGeneral){
                    getLoaderManager().restartLoader(FIND_NEW_CARDS, null, AddCardDialogFragment.this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //ADAPTERS
        mCardsAA = new CardsAA(getContext(), mOptionEnum, mAddCardDialogInterface);
        vFoundCardsRV.setAdapter(mCardsAA);
        vFoundCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    //VIEW STATE



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mAddCardDialogInterface = (AddCardDialogInterface) getTargetFragment();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException : " + e.getMessage());
        }
    }


    //LOADER+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @NonNull
    @Override
    public Loader onCreateLoader(int id, @Nullable Bundle args) {
        switch (mOptionEnum){
            case searchAll:
                return new FilterCardATL(getContext(), vFilterET.getText().toString());
            case searchWanted:
                return new FilterWantedATL(getContext(), vFilterET.getText().toString());
            case searchWantedGeneral:
                return new FilterWantedGeneralATL(getContext(), vFilterET.getText().toString());


        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {
        switch (mOptionEnum){
            case searchAll:
            case searchWanted:
                mCardsAA.updateListAA((List<CardInfo>) data);
                break;
            case searchWantedGeneral:
                mCardsAA.updateWantedListAA((List<WantedCard>) data);
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {

    }
}
