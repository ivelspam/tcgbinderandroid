package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Objects.CartCard;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CartFragment extends Fragment implements View.OnClickListener {



    enum CartCardType {
        giving,
        receiving
    }

    private final String TAG = "##" + this.getClass().getSimpleName();

    private TradeActivityInterface mTradeActivityInterface;

    TextView vGivintCardsTV, vGivingTotalPrice, vReceivingCardsTV, vReceivingTotalPrice, vTotalNetPriceTV;
    RecyclerView vReceivingCardsRV, vGivingCardsRV;
    Button clearCartBT;
    CartCartAA mGivingCardsAA = new CartCartAA(CartCardType.giving);
    CartCartAA mReceivingCardsAA = new CartCartAA(CartCardType.receiving);

    public CartFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    private void build_view(){
        vReceivingCardsRV = getView().findViewById(R.id.fragmentCart_receivingCardsRV);
        vGivingCardsRV = getView().findViewById(R.id.fragmentCart_givingCardsRV);

        vGivintCardsTV = getView().findViewById(R.id.fragmentCart_givingCardsTV);
        vGivingTotalPrice = getView().findViewById(R.id.fragmentCart_givingTotalPriceTV);
        vReceivingCardsTV = getView().findViewById(R.id.fragmentCart_receivingCardsTV);
        vReceivingTotalPrice = getView().findViewById(R.id.fragmentCart_receivingTotalPriceTV);
        vTotalNetPriceTV = getView().findViewById(R.id.fragmentCart_totalNetPriceTV);
        clearCartBT = getView().findViewById(R.id.fragmentCard_clearCartBT);


        //listeners
        clearCartBT.setOnClickListener(this);

//        ADAPTERS
        vReceivingCardsRV.setAdapter(mReceivingCardsAA);
        vReceivingCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        vGivingCardsRV.setAdapter(mGivingCardsAA);
        vGivingCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));



        updateValues();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTradeActivityInterface = null;
    }

    public void updateValues(){
        if(mTradeActivityInterface == null || mTradeActivityInterface.getTradesActivityValues() == null){
            return;
        }

        mGivingCardsAA.notifyDataSetChanged();
        mReceivingCardsAA.notifyDataSetChanged();
        setGivingTotals();

    }

    void setGivingTotals(){
        int totalCardsGiving = 0;
        double totalCardsGivingPrice = 0;

        for(CartCard cartCard : mTradeActivityInterface.getTradesActivityValues().getGiving()){
           totalCardsGiving += cartCard.getFoilQuantity();
            totalCardsGiving += cartCard.getNonfoilQuantity();

            if(cartCard.getNonfoilPriceType() == 0) {
                cartCard.getNonfoilQuantity();
                cartCard.getUsd();
                if(cartCard.getUsd() != null){
                    totalCardsGivingPrice += cartCard.getUsd() * cartCard.getNonfoilQuantity();
                }
            }else{
                totalCardsGivingPrice += cartCard.getFoilMyPrice() * cartCard.getNonfoilQuantity();
            }
            totalCardsGivingPrice += cartCard.getFoilMyPrice() * cartCard.getFoilQuantity();
        }

        NumberFormat formatter = new DecimalFormat("#0.00");

        vGivintCardsTV.setText("Giving Price: " + Integer.toString(totalCardsGiving));
        vGivingTotalPrice.setText("Giving Price: " + formatter.format(totalCardsGivingPrice));

        int totalCardsReceiving = 0;
        double totalCardsPriceReceiving = 0;


        for(CartCard cartCard :  mTradeActivityInterface.getTradesActivityValues().getReceiving()){

            totalCardsReceiving += cartCard.getFoilQuantity();
            totalCardsReceiving += cartCard.getNonfoilQuantity();

            if(cartCard.getNonfoilPriceType() == 0) {
                if(cartCard.getUsd() != null){
                    totalCardsPriceReceiving += cartCard.getUsd() * cartCard.getNonfoilQuantity();
                }
            }else{
                totalCardsPriceReceiving += cartCard.getNonfoilMyPrice() * cartCard.getNonfoilQuantity();
            }

            totalCardsPriceReceiving += cartCard.getFoilMyPrice() * cartCard.getFoilQuantity();
        }

        vReceivingCardsTV.setText("Receiving Cards: " +Integer.toString(totalCardsReceiving));
        vReceivingTotalPrice.setText("Receiving Price: " + formatter.format(totalCardsPriceReceiving));

        vTotalNetPriceTV.setText("Net Price: " +formatter.format(totalCardsPriceReceiving - totalCardsGivingPrice));

    }

    //LISTENERS+++++++++++++++++++++++++++++++++++++++++++++++++++++++

    @Override
    public void onClick(View view) {
        if(view == clearCartBT){
            mTradeActivityInterface.socketSendClearCart();
        }
    }

    //ADAPTERS++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    class CartCartAA extends RecyclerView.Adapter {

        CartCardType mCartCartType;

        public CartCartAA(CartCardType cartCardType){
            mCartCartType = cartCardType;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_cart_card, parent, false);
            return new cardInfoHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (mCartCartType){
                case giving:
                    ((cardInfoHolder) holder).bind(mTradeActivityInterface.getTradesActivityValues().getGiving().get(position), position);
                break;
                case receiving:
                    ((cardInfoHolder) holder).bind(mTradeActivityInterface.getTradesActivityValues().getReceiving().get(position), position);
                break;
            }
        }

        @Override
        public int getItemCount() {

            switch (mCartCartType){
                case giving:
                    return mTradeActivityInterface.getTradesActivityValues().getGiving().size();
                case receiving:
                    return mTradeActivityInterface.getTradesActivityValues().getReceiving().size();
                default:
                    return 0;
            }

        }

        private class cardInfoHolder extends RecyclerView.ViewHolder {

            TextView nameTV, foilTCGPriceTV, nonfoilTCGPriceTV, setNameTV;
            ImageView setIV;
            RadioGroup nonfoilPriceRG, foilPriceRG;
            RadioButton nonfoilTCGPriceRB, nonfoilMyPriceRB, foilTCGPriceRB, foilMyPriceRB;
            EditText nonfoilQuantityET, foilQuantityET, foilMyPriceET, nonfoilMyPriceET;
            CartCard mCardInfo;
            Button vUpdateBT, vRemoveBT;
            int mPosition;

            public cardInfoHolder(View itemView) {
                super(itemView);
                setIV = itemView.findViewById(R.id.adapterRowReceivingCartCard_setCodeIV);
                nameTV = itemView.findViewById(R.id.adapterRowReceivingCartCard_nameTV);
                setNameTV = itemView.findViewById(R.id.adapterRowReceivingCartCard_setNameTV);
                foilMyPriceET = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilMyPriceET);
                foilPriceRG = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilPriceRG);
                foilQuantityET = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilQuantityET);
                nonfoilMyPriceET = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonFoilMyPriceET);
                nonfoilQuantityET = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonfoilQuantityET);
                nonfoilTCGPriceTV = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonFoilTCGPriceTV);
                nonfoilPriceRG  = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonfoilPriceRG);
                foilTCGPriceTV = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilTCGPriceRB);
                vUpdateBT = itemView.findViewById(R.id.adapterRowReceivingCartCard_updateBT);
                foilTCGPriceRB = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilTCGPriceRB);
                foilMyPriceRB = itemView.findViewById(R.id.adapterRowReceivingCartCard_foilMyPriceRB);
                nonfoilTCGPriceRB = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonfoilTCGPriceRB);
                nonfoilMyPriceRB = itemView.findViewById(R.id.adapterRowReceivingCartCard_nonfoilMyPriceRB);
                vRemoveBT = itemView.findViewById(R.id.adapterRowReceivingCartCard_removeBT);
            }

            public void bind(final CartCard cartCard, final int position) {
                mPosition = position;

                //REMOVE LISTENERS
                foilMyPriceET.removeTextChangedListener(textWatcher);
                nonfoilMyPriceET.removeTextChangedListener(textWatcher);
                nonfoilPriceRG.clearCheck();
                foilPriceRG.clearCheck();
                vRemoveBT.setOnClickListener(null);
                foilQuantityET.removeTextChangedListener(textWatcher);
                nonfoilQuantityET.removeTextChangedListener(textWatcher);



                //initial state
                mCardInfo = cartCard;
                vUpdateBT.setEnabled(false);
                setNameTV.setText(cartCard.getSet_name());
                Resources resources = getContext().getResources();
                String imageIndentifier = "s"+cartCard.getSet_code() + "_svg";
                int add = resources.getIdentifier(imageIndentifier, "drawable", getContext().getPackageName());
                setIV.setImageResource(add);


                if(mCartCartType == CartCardType.giving){
                    if(cartCard.getNonfoilPriceType() == 0){
                        nonfoilPriceRG.check(R.id.adapterRowReceivingCartCard_nonfoilTCGPriceRB);
                    }else{
                        nonfoilPriceRG.check(R.id.adapterRowReceivingCartCard_nonfoilMyPriceRB);

                    }

                    if(cartCard.getFoilPriceType() == 0){
                        foilPriceRG.check(R.id.adapterRowReceivingCartCard_foilTCGPriceRB);
                    }else{
                        foilPriceRG.check(R.id.adapterRowReceivingCartCard_foilMyPriceRB);
                    }

                    nonfoilPriceRG.setOnCheckedChangeListener(onCheckedChangeListener);
                    foilPriceRG.setOnCheckedChangeListener(onCheckedChangeListener);
                    foilQuantityET.setKeyListener(null);
                    nonfoilQuantityET.setKeyListener(null);
                    nonfoilMyPriceET.addTextChangedListener(textWatcher);
                    foilMyPriceET.addTextChangedListener(textWatcher);

                }else{
                    nonfoilTCGPriceRB.setEnabled(true);
                    nonfoilMyPriceRB.setEnabled(true);
                    foilTCGPriceRB.setEnabled(true);
                    foilMyPriceRB.setEnabled(true);

                    if(cartCard.getNonfoilPriceType() == 0){
                        nonfoilPriceRG.check(R.id.adapterRowReceivingCartCard_nonfoilTCGPriceRB);
                        nonfoilMyPriceRB.setEnabled(false);
                    }else{
                        nonfoilPriceRG.check(R.id.adapterRowReceivingCartCard_nonfoilMyPriceRB);
                        nonfoilTCGPriceRB.setEnabled(false);

                    }

                    if(cartCard.getFoilPriceType() == 0){
                         foilPriceRG.check(R.id.adapterRowReceivingCartCard_foilTCGPriceRB);
                         foilMyPriceRB.setEnabled(false);
                    }else{
                        foilPriceRG.check(R.id.adapterRowReceivingCartCard_foilMyPriceRB);
                        foilTCGPriceRB.setEnabled(false);
                    }


//                    nonfoilMyPriceET.setEnabled(false);
//                    foilMyPriceET.setEnabled(false);
                    nonfoilMyPriceET.setKeyListener(null);
                    foilMyPriceET.setKeyListener(null);
                    foilQuantityET.addTextChangedListener(textWatcher);
                    nonfoilQuantityET.addTextChangedListener(textWatcher);
                }

                if(cartCard.getFoilPriceType() == 0){
                    foilPriceRG.check(R.id.adapterRowReceivingCartCard_foilTCGPriceRB);
                }
                if(cartCard.getNonfoilPriceType() == 0){
                    nonfoilPriceRG.check(R.id.adapterRowReceivingCartCard_nonfoilTCGPriceRB);
                }

                nameTV.setText(cartCard.getName());


                foilQuantityET.setText(Integer.toString(cartCard.getFoilQuantity()), TextView.BufferType.EDITABLE);
                foilMyPriceET.setText(Double.toString(cartCard.getFoilMyPrice()));


                nonfoilQuantityET.setText(Integer.toString(cartCard.getNonfoilQuantity()), TextView.BufferType.EDITABLE);
                nonfoilMyPriceET.setText(Double.toString(cartCard.getNonfoilMyPrice()));

                if (cartCard.getUsd() != null) {
                    nonfoilTCGPriceTV.setText(Double.toString(cartCard.getUsd()));
                }


                //LISTENERS

                vRemoveBT.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    if(mCartCartType == CartCardType.giving){
                        mTradeActivityInterface.removeCartCard(cartCard, "giving");

                    }else{
                        mTradeActivityInterface.removeCartCard(cartCard, "receiving");

                    }

                    }
                });

                vUpdateBT.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(mCartCartType == CartCardType.giving){
                            Double nonfoilMyPrice = 0d;
                            Double foilMyPrice = 0d;
                            Integer nonfoilPriceType = 0;
                            Integer foilPriceType = 0;

                            if(TextUtils.isEmpty(nonfoilMyPriceET.getText().toString())){
                                nonfoilMyPriceET.setText("0");
                            }
                            nonfoilMyPrice = Double.parseDouble(nonfoilMyPriceET.getText().toString());
                            if(TextUtils.isEmpty(foilMyPriceET.getText().toString())){
                                foilMyPriceET.setText("0");
                            }

                            foilMyPrice = Double.parseDouble(foilMyPriceET.getText().toString());

                            if(nonfoilMyPriceRB.isChecked()){
                                nonfoilPriceType = 1;
                            }

                            if(foilMyPriceRB.isChecked()){
                                foilPriceType = 1;
                            }
                            cartCard.setNonfoilMyPrice(nonfoilMyPrice);
                            cartCard.setFoilMyPrice(foilMyPrice);
                            cartCard.setNonfoilPriceType(nonfoilPriceType);
                            cartCard.setFoilPriceType(foilPriceType);


                            mTradeActivityInterface.getTradesActivityValues().getGiving().set(position, cartCard);
                            mTradeActivityInterface.sendUpdateCartCard(cartCard, "giving");

                        }else{

                            if(TextUtils.isEmpty(nonfoilQuantityET.getText().toString())){
                                nonfoilQuantityET.setText("0");
                            }

                            Integer nonfoilQuantity = Integer.parseInt(nonfoilQuantityET.getText().toString());

                            if(TextUtils.isEmpty(foilQuantityET.getText().toString())){
                                foilQuantityET.setText("0");
                            }
                            Integer foilQuantity = Integer.parseInt(foilQuantityET.getText().toString());

                            cartCard.setNonfoilQuantity(nonfoilQuantity);
                            cartCard.setFoilQuantity(foilQuantity);

                            mTradeActivityInterface.getTradesActivityValues().getReceiving().set(position, cartCard);
                            mTradeActivityInterface.sendUpdateCartCard(cartCard, "receiving");
                        }
                        updateValues();
                    }
                });
            }

            RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    vUpdateBT.setEnabled(true);

                }
            };

            TextWatcher textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    vUpdateBT.setEnabled(true);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
        }
    }

}
