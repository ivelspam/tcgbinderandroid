package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.UserCards.TraderCardsFragment;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.CartCard;
import com.tcgbinder.app.model.Objects.ChatMessage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TradeActivity extends AppCompatActivity implements TradeActivityInterface, LoaderManager.LoaderCallbacks<TradersActivityValues> {
    private final String TAG = "##" + this.getClass().getSimpleName();

    TraderChatFragment mTraderChatFragment = new TraderChatFragment();
    InCommonCardsFragment mInCommonCardsFragment = new InCommonCardsFragment();
    TraderCardsFragment mTraderCardsFragment = new TraderCardsFragment();
    CartFragment mCartFragment = new CartFragment();
    InCommonGeneralCardsFragment mInCommonGeneralCardsFragment = new InCommonGeneralCardsFragment();
    String mTrader_id;
    String mName;
    TraderSocket mTraderSocket;
    TradersActivityValues mTradersActivityValues;
    SwipeRefreshLayout vSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_trade);

        Toolbar toolbar = findViewById(R.id.activityTrade_toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        mTrader_id = intent.getStringExtra(MongoKeys._id);
        mName = intent.getStringExtra(MongoKeys.name);
        vSwipeRefreshLayout = findViewById(R.id.activityTrade_swipe);

        mTraderSocket = new TraderSocket(getBaseContext(), this, mTrader_id, this);
        mTradersActivityValues = new TradersActivityValues(getBaseContext(), mTrader_id, this, this);

        getLoaderManager().restartLoader(0, null, this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_holder, mTraderChatFragment).commit();

        vSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLoaderManager().restartLoader(0, null, TradeActivity.this);
            }
        });
        getSupportActionBar().setTitle(mName + " - Chat");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_trade, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.menuTrade_chat:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_holder, mTraderChatFragment).commit();
                getSupportActionBar().setTitle(mName + " - Chat");
                return true;
            case R.id.menuTrade_setCard:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_holder, mInCommonCardsFragment).commit();
                getSupportActionBar().setTitle(mName + " - Set Cards");
                return true;
            case R.id.menuTrade_card:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_holder, mInCommonGeneralCardsFragment).commit();
                getSupportActionBar().setTitle(mName + " - Any Set Cards");

                return true;
            case R.id.menuTrade_userCards:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_holder, mTraderCardsFragment).commit();
                getSupportActionBar().setTitle(mName + " - All Trader Cards");
                return true;
            case R.id.menuTrade_cart:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_holder, mCartFragment).commit();
                getSupportActionBar().setTitle(mName + " - Cart");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTraderSocket.disconnect();
    }

    //INTERFACE
    @Override
    public String getTrader_id() {
        return mTrader_id;
    }
    public String getTraderName() {
        return mName;
    }

    @Override
    public void sendMessage(String content) {
        mTraderSocket.sendMessage(content, mTrader_id);
    }

    @Override
    public void receivedMessage(ChatMessage chatMessage) {
        mTradersActivityValues.receiveMessage(chatMessage);
        mTraderChatFragment.receiveMessage();
    }

    @Override
    public void addCartToCard(String card_id, Integer nonfoilQuantity, Integer foilQuantity) {
        mTraderSocket.addCardToCard(card_id, mTrader_id, nonfoilQuantity,  foilQuantity);
    }

    @Override
    public void sendUpdateCartCard(CartCard cartCard, String updateType) {
        mTraderSocket.updateGivingCartCard(cartCard, updateType);
    }

    @Override
    public void receiveUpdateCartCard(JSONObject jsonObject) {
        mTradersActivityValues.receiveNewOrUpdated(jsonObject);
    }

    @Override
    public void removeCartCard(CartCard cartCard, String updateType) {
        mTraderSocket.removeCartCard(cartCard, updateType);
    }

    @Override
    public void receivedRemoveCartCard(final JSONObject jsonObject) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTradersActivityValues.receiveRemoveCartCard(jsonObject);
            }
        });

    }

    @Override
    public void updateEverything() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTraderChatFragment.updateValues();
                mInCommonCardsFragment.updateValues();
                mInCommonGeneralCardsFragment.updateValues();
                vSwipeRefreshLayout.setRefreshing(false);
                updateCart();
            }
        });
    }

    @Override
    public void updateCart() {
        mCartFragment.updateValues();
    }

    @Override
    public void addMessageToChat() {

    }

    @Override
    public void socketReceivedClearCart() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTradersActivityValues.clearCart();
            }
        });
    }

    @Override
    public void socketSendClearCart() {
        mTraderSocket.clearCart();
    }

    @Override
    public TradersActivityValues getTradesActivityValues() {
        return mTradersActivityValues;
    }



    //LOADER
    @Override
    public Loader<TradersActivityValues> onCreateLoader(int i, Bundle bundle) {
        return new GetAllInfoATL(getBaseContext(), mTradersActivityValues);
    }

    @Override
    public void onLoadFinished(Loader<TradersActivityValues> loader, TradersActivityValues tradersActivityValues) {
//        mTradersActivityValues = tradersActivityValues;
        updateEverything();
    }

    @Override
    public void onLoaderReset(Loader<TradersActivityValues> loader) {

    }

    public class Adapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
