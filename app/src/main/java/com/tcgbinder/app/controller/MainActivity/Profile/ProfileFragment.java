package com.tcgbinder.app.controller.MainActivity.Profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Logout;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.ServerRequest.QueryForUpdatedSingleton;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_deleteAccount;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_openForTrading;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;
import com.tcgbinder.app.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment{
    private static final String TAG = "##ProfileFragment";

    RecyclerView vListRV;
    AdapterProfileInfaAdapter mListAA;


    public ProfileFragment() {


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildView();
    }

    void buildView(){
        //Connect
        vListRV = getView().findViewById(R.id.fragmentProfile_listRV);

        mListAA = new AdapterProfileInfaAdapter();

        vListRV.setAdapter(mListAA);
        vListRV.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public class AdapterProfileInfaAdapter extends RecyclerView.Adapter{

        private static final String TAG = "##RecyclerViewAdapterAd";

        String[] rows = ProfileValueRow.rows;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new rowHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_profile_info, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((rowHolder) holder).bind(rows[position], position == (rows.length - 1));
        }

        @Override
        public int getItemCount() {
            return rows.length;
        }

        class rowHolder extends RecyclerView.ViewHolder {
            ConstraintLayout hvWholeLayoutCL;
            TextView tvName, tvValue;
            ImageView ivIcon, ivArrow;
            View vBottomLine;
            Switch hvChangeOpenForTraindSWT;
            ChangeOpenForTradingAT mChangeOpenForTrading;

            public rowHolder(View itemView) {
                super(itemView);
                ivIcon = itemView.findViewById(R.id.adapterProfileInfo_ivIcon);
                tvName = itemView.findViewById(R.id.adapterProfileInfo_tvName);
                tvValue = itemView.findViewById(R.id.adapterProfileInfo_tvValue);
                ivArrow = itemView.findViewById(R.id.adapterProfileInfo_ivArrow);
                vBottomLine = itemView.findViewById(R.id.adapterProfileInfo_vBottomLine);
                hvWholeLayoutCL = itemView.findViewById(R.id.adapterProfileInfo_WholeLayoutCL);
                hvChangeOpenForTraindSWT = itemView.findViewById(R.id.adapterProfileInfo_changeOpenForTradingSWT);
            }

            void bind(String rowName, boolean lastRow) {

                final ProfileValueRow profileValueRow = new ProfileValueRow (getContext(), rowName);

                tvName.setText(profileValueRow.getName());
                tvValue.setText(profileValueRow.getValue());
                ivIcon.setImageResource(profileValueRow.getIcon());


                hvWholeLayoutCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        switch (profileValueRow.getName()){
                            case "Delete Account":

                                new AlertDialog.Builder(getContext())
                                        .setTitle("Delete Account")
                                        .setMessage("Do you want to delete your account??")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                new DeleteAccount_http().execute();
                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();

                                break;
                            case "Open For Sharing":

                                return;
                            case "Log Out":
                                new Logout_http().execute();
                                break;
                            case "Trade Info":
                                TradeInfoDialogFragment tradeInfoDialogFragment = new TradeInfoDialogFragment();
                                tradeInfoDialogFragment.show(getFragmentManager(), "custom");
                            break;


                            default:
//                                startActivityForResult(profileValueRow.getIntent(), 987);
                        }
                    }
                });

                if(profileValueRow.getName().equals("Open For Sharing")){
                    hvChangeOpenForTraindSWT.setVisibility(View.VISIBLE);
                    hvChangeOpenForTraindSWT.setChecked(SharedPreferencesUtils.getInstance(getContext()).getBoolean(SharedPreferencesUtils.PrefKeys.openForTrading));
                    hvWholeLayoutCL.setOnClickListener(null);

                    hvChangeOpenForTraindSWT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if(mChangeOpenForTrading != null) return;
                            hvChangeOpenForTraindSWT.setEnabled(false);
                            mChangeOpenForTrading = new ChangeOpenForTradingAT(b);
                            mChangeOpenForTrading.execute();
                        }
                    });
                }
                ivArrow = itemView.findViewById(R.id.adapterProfileInfo_ivArrow);

                if(lastRow){
                    vBottomLine.setVisibility(View.GONE);
                }
            }

            class ChangeOpenForTradingAT extends AsyncTask<String, String, JSONObject> {

                Boolean aOpenForTrading;

                public ChangeOpenForTradingAT(Boolean openForTrading){
                    aOpenForTrading = openForTrading;

                }

                @Override
                protected JSONObject doInBackground(String... strings) {
                    return new ServerAPI(getContext()).serverRequestPOST(new User_openForTrading(aOpenForTrading, getContext()));
                }

                @Override
                protected void onPostExecute(JSONObject jsonObject) {

                    try {
                        if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                            switch (jsonObject.getString(APIRequest.message)){
                                case User_openForTrading.Response.changed:
                                    SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.openForTrading, aOpenForTrading);
                                    break;
                                default:
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hvChangeOpenForTraindSWT.setEnabled(true);
                    mChangeOpenForTrading = null;
                }
            }
        }

        class Logout_http extends AsyncTask<String, String, JSONObject> {
            @Override
            protected JSONObject doInBackground(String... strings) {
                Logout.logout(getContext());
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {


            }
        }

        class DeleteAccount_http extends AsyncTask<String, String, JSONObject> {
            @Override
            protected JSONObject doInBackground(String... strings) {
                return new ServerAPI(getContext()).serverRequestPOST(new User_deleteAccount());
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                        try {
                            switch (jsonObject.getString(APIRequest.message)){
                                case User_deleteAccount.Response.accountDeleted :
                                    Logout.logout(getContext());
                                    break;

                                default:

                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

}
