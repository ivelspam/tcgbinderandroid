package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Objects.TraderWantedCard;

public class InCommonGeneralCardsFragment extends Fragment implements View.OnClickListener {
    private final String TAG = "##" + this.getClass().getSimpleName();

    private TradeActivityInterface mTradeActivityInterface;
    RecyclerView vMyCardsRV, vTradersCardsRV;

    TraderAA mTraderAA = new TraderAA();
    WantedAA mWantedAA;

    public InCommonGeneralCardsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_in_common_cards, container, false);
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWantedAA = new WantedAA(getContext(), mTradeActivityInterface);
        build_view();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTradeActivityInterface = null;
    }

    private void build_view(){
        vMyCardsRV = getView().findViewById(R.id.fragmentInCommonCards_myCardsRV);
        vTradersCardsRV = getView().findViewById(R.id.fragmentInCommonCards_traderCardsRV);

//        ADAPTERS
        vMyCardsRV.setAdapter(mWantedAA);
        vMyCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));


        vTradersCardsRV.setAdapter(mTraderAA);
        vTradersCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));

        mWantedAA.updateCards(mTradeActivityInterface.getTradesActivityValues().getWantedGeneral());
        mWantedAA.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }

    void updateValues(){

        if(mTradeActivityInterface == null || mTradeActivityInterface.getTradesActivityValues() == null){
            return;
        }

        mTraderAA.notifyDataSetChanged();
        mWantedAA.updateCards(mTradeActivityInterface.getTradesActivityValues().getWantedGeneral());
    }

    //ADAPTERS++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    class TraderAA extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_trader_general_card, parent, false);
            return new cardInfoHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((cardInfoHolder) holder).bind(mTradeActivityInterface.getTradesActivityValues().getTraderGeneral().get(position), position);
        }

        @Override
        public int getItemCount() {
            return mTradeActivityInterface.getTradesActivityValues().getTraderGeneral().size();
        }

        private class cardInfoHolder extends RecyclerView.ViewHolder {

            TextView nameTV, nonfoilPriceTV, foilPriceTV;
            ImageView setCodeIV;
            TextView nonfoilQuantityTV, foilQuantityTV;


            public cardInfoHolder(View itemView) {
                super(itemView);

                nameTV = itemView.findViewById(             R.id.adapterRowTraderCards_nameTV);
                foilQuantityTV = itemView.findViewById(     R.id.adapterRowTraderCards_foilQuantityET);
                nonfoilQuantityTV = itemView.findViewById(  R.id.adapterRowTraderCards_nonfoilQuantityET);
                nonfoilPriceTV = itemView.findViewById(     R.id.adapterRowTraderCards_nonFoilPriceTV);
                foilPriceTV = itemView.findViewById(        R.id.adapterRowTraderCards_foilPriceTV);
                setCodeIV = itemView.findViewById(          R.id.adapterRowTraderCards_setCodeIV);

            }

            public void bind(final TraderWantedCard traderWantedCard, final int position) {
                //initial state

                nameTV.setText(traderWantedCard.getName());
                nonfoilQuantityTV.setText(Integer.toString(traderWantedCard.getNonfoilQuantity()), TextView.BufferType.EDITABLE);

                foilQuantityTV.setText(Integer.toString(traderWantedCard.getFoilQuantity()), TextView.BufferType.EDITABLE);

                nameTV.setText(traderWantedCard.getName());
                nonfoilQuantityTV.setText(Integer.toString(traderWantedCard.getNonfoilQuantity()), TextView.BufferType.EDITABLE);

                foilQuantityTV.setText(Integer.toString(traderWantedCard.getFoilQuantity()), TextView.BufferType.EDITABLE);
            }
        }
    }



}
