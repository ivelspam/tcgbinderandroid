package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.UserCards.UserCardsPagination;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.CardsInfo_getPrice;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;

public class WantedAA extends RecyclerView.Adapter{
    private final String TAG = "##" + this.getClass().getSimpleName();

    Context mContext;
    TradeActivityInterface mTradeActivityInterface;
    List<CardInfo> mCardInfos;
    DecimalFormat df = new DecimalFormat("0.00");

    public WantedAA(Context context, TradeActivityInterface tradeActivityInterface){
        mContext = context;
        mTradeActivityInterface = tradeActivityInterface;
    }

    public void updateCards(List<CardInfo> cardInfos){
        mCardInfos = cardInfos;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_wanted_cards, parent, false);
        return new CardInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CardInfoHolder) holder).bind(mCardInfos.get(position), position);
    }

    @Override
    public int getItemCount() {
        if(mCardInfos == null){
            return 0;
        }
        return mCardInfos.size();
    }

    private class CardInfoHolder extends RecyclerView.ViewHolder {

        TextView nameTV, setNameTV, nonfoilPriceTV, foilPriceTV;
        ImageView setIV;
        EditText nonfoilQuantityET, foilQuantityET;
        Button vAddToCardBT;
        CardInfo mCardInfo;


        public CardInfoHolder(View itemView) {
            super(itemView);
            setIV = itemView.findViewById(R.id.adapterRowWantedCards_setCodeIV);
            setNameTV = itemView.findViewById(R.id.adapterRowWantedCards_setNameTV);
            nameTV = itemView.findViewById(R.id.adapterRowWantedCards_nameTV);
            foilQuantityET = itemView.findViewById(R.id.adapterRowWantedCards_foilQuantityET);
            nonfoilQuantityET = itemView.findViewById(R.id.adapterRowWantedCards_nonfoilQuantityET);
            nonfoilPriceTV = itemView.findViewById(R.id.adapterRowWantedCards_nonFoilPriceTV);
            foilPriceTV = itemView.findViewById(R.id.adapterRowWantedCards_foilPriceTV);
            vAddToCardBT = itemView.findViewById(R.id.adapterRowWantedCards_addToCardBT);
        }

        public void bind(final CardInfo cardInfo, int position) {
            //initial state
            mCardInfo = cardInfo;
            nameTV.setText(cardInfo.getName());
            setNameTV.setText(cardInfo.getSet_name());

            nonfoilQuantityET.setText(Integer.toString(cardInfo.getNonfoilQuantity()), TextView.BufferType.EDITABLE);

            foilQuantityET.setText(Integer.toString(cardInfo.getFoilQuantity()), TextView.BufferType.EDITABLE);

            Resources resources = mContext.getResources();
            String imageIndentifier = "s"+cardInfo.getSet_code() + "_svg";
            int add = resources.getIdentifier(imageIndentifier, "drawable", mContext.getPackageName());
            setIV.setImageResource(add);

            //CARD
            if(DateExtension.CalendarOlderThanOneDay(cardInfo.getPriceUpdatedAt())){
                nonfoilPriceTV.setText("");

                new GetCardPrice(cardInfo.get_id(), nonfoilPriceTV).execute();
            }else{
                if(cardInfo.getUsd() != null){
                    nonfoilPriceTV.setText(df.format(cardInfo.getUsd()));
                }else{
                    nonfoilPriceTV.setText("N/A");
                }
            }

            vAddToCardBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int nonfoilQuantity = 0;
                    int foilQuantity = 0;

                    if(!TextUtils.isEmpty(nonfoilQuantityET.getText().toString())){ nonfoilQuantity = Integer.parseInt(nonfoilQuantityET.getText().toString()); }
                    if(!TextUtils.isEmpty(nonfoilQuantityET.getText().toString())){ foilQuantity = Integer.parseInt(foilQuantityET.getText().toString()); }

                    mTradeActivityInterface.addCartToCard(cardInfo.get_id(), nonfoilQuantity, foilQuantity);
                }
            });
        }


        class GetCardPrice extends AsyncTask<Void, Void, JSONObject> {

            String aCard_id;
            WeakReference<TextView> aPriceWR;

            public GetCardPrice(String card_id, TextView textView){
                aCard_id = card_id;
                aPriceWR = new WeakReference<>(textView);
            }

            @Override
            protected JSONObject doInBackground(Void... voids) {
                return new ServerAPI(mContext).serverRequestGET(new CardsInfo_getPrice(aCard_id));
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    switch (jsonObject.getString(APIRequest.message)){
                        case CardsInfo_getPrice.Response.found:
                            if(aPriceWR.get()!= null){
                                JSONObject priceJSONObject = jsonObject.getJSONObject(CardsInfo_getPrice.ResponseFields.price);
                                Motherbase.getInstance(mContext).getCardInfoTable().updatePriceFromJSON(priceJSONObject);
                                if(mCardInfo.get_id().equals(aCard_id)){
                                    if(!priceJSONObject.isNull(MongoKeys.usd)){
                                        aPriceWR.get().setText(df.format(priceJSONObject.getDouble(MongoKeys.usd)));
                                    }else{
                                        aPriceWR.get().setText("N/A");
                                    }
                                }
                            }
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


    }

}
