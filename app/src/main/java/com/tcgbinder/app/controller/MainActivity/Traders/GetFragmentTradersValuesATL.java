package com.tcgbinder.app.controller.MainActivity.Traders;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Traders_getCloserTradersWithInfoAndCart;

import org.json.JSONObject;

public class GetFragmentTradersValuesATL extends AsyncTaskLoader<TradersFragmentValues> {
    private final String TAG = "##" + this.getClass().getSimpleName();

    public GetFragmentTradersValuesATL(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public TradersFragmentValues loadInBackground() {
        TradersFragmentValues tradersFragmentValues = new TradersFragmentValues();
        JSONObject jsonObject = new ServerAPI(getContext()).serverRequestGET(new Traders_getCloserTradersWithInfoAndCart(getContext()));

        Log.i(TAG, jsonObject.toString());

        tradersFragmentValues.insertValues(jsonObject);
        return tradersFragmentValues;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
