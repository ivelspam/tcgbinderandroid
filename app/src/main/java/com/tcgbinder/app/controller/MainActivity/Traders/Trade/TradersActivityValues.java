package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.app.Activity;
import android.content.Context;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.CartCard;
import com.tcgbinder.app.model.Objects.ChatMessage;
import com.tcgbinder.app.model.Objects.TraderWantedCard;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TradersActivityValues {
    private final String TAG = "##" + this.getClass().getSimpleName();
    private List<ChatMessage> chatMessages;
    private List<CardInfo> mWantedCards = new ArrayList<>();
    private List<CardInfo> mTradeCards = new ArrayList<>();
    public List<CardInfo> wantedGeneral = new ArrayList<>();
    public List<TraderWantedCard> traderGeneral = new ArrayList<>();
    public List<CartCard> receiving = new ArrayList<>();
    public List<CartCard> giving = new ArrayList<>();
    private Context mContext;
    private String mTrader_id;
    private TradeActivityInterface mTradeActivityInterface;
    private Activity activity;



    public TradersActivityValues(Context context, String trader_id, TradeActivityInterface tradeActivityInterface, Activity activity){

        mContext = context;
        mTrader_id = trader_id;

        mTradeActivityInterface = tradeActivityInterface;
        chatMessages = Motherbase.getInstance(context).getChatMessageTable().selectAllChatsFromTrader(trader_id);
        this.activity = activity;
        mTradeActivityInterface.updateEverything();
    }

    public void putValues(JSONObject jsonObject){
        try {
            if(jsonObject.has("chats")){
                setChats(jsonObject.getJSONArray("chats"));
            }

            if(jsonObject.has("inCommonGeneral")){
                setInCommonGeneral(jsonObject.getJSONObject("inCommonGeneral"));
            }

            if(jsonObject.has("inCommon")){
                setInCommon(jsonObject.getJSONObject("inCommon"));
            }

            if(jsonObject.has("cartCards")){
                addCardsToCart(jsonObject.getJSONArray("cartCards"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setChats(JSONArray jsonArray){
        Motherbase.getInstance(mContext).getChatMessageTable().insertOrReplaceChatMessages(jsonArray);

        for(int i = 0; i< jsonArray.length(); i++){
            try {
                ChatMessage chatMessage = new ChatMessage(jsonArray.getJSONObject(i));
                boolean found = false;
                for(ChatMessage iChatMessage : chatMessages){
                    if(iChatMessage.getaUUID().equals(chatMessage.getaUUID())){
                        found = true;
                        continue;
                    }
                }
                if(!found){
                    chatMessages.add(chatMessage);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setInCommon(JSONObject jsonObject){

        try {
            JSONArray traderCardsJSONArray = jsonObject.getJSONArray("trade");
            JSONArray wanterCardsJSONArray = jsonObject.getJSONArray("wanted");

            mTradeCards.clear();
            mWantedCards.clear();

            CardInfoTable cardInfoTable = Motherbase.getInstance(mContext).getCardInfoTable();
            for(int i =0 ; i < traderCardsJSONArray.length(); i++){
                CardInfo cardInfo = null;
                try {
                    cardInfo = cardInfoTable.selectCardInfoBy_id(traderCardsJSONArray.getJSONObject(i).getString("card_id"));
                    cardInfo.setNonfoilQuantity(traderCardsJSONArray.getJSONObject(i).getInt(MongoKeys.nonfoilQuantity));
                    cardInfo.setFoilQuantity(traderCardsJSONArray.getJSONObject(i).getInt(MongoKeys.foilQuantity));
                    mTradeCards.add(cardInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            for(int i =0 ; i < wanterCardsJSONArray.length(); i++){
                CardInfo cardInfo;
                try {
                    cardInfo = cardInfoTable.selectCardInfoBy_id(wanterCardsJSONArray.getJSONObject(i).getString("card_id"));
                    cardInfo.setNonfoilQuantity(wanterCardsJSONArray.getJSONObject(i).getInt(MongoKeys.nonfoilQuantity));
                    cardInfo.setFoilQuantity(wanterCardsJSONArray.getJSONObject(i).getInt(MongoKeys.foilQuantity));
                    mWantedCards.add(cardInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void setInCommonGeneral(JSONObject jsonObject){
        try {
            JSONArray myWantedGeneral = jsonObject.getJSONArray("myWantedGeneral");
            CardInfoTable cardInfoTable = Motherbase.getInstance(mContext).getCardInfoTable();
            wantedGeneral.clear();
            traderGeneral.clear();

            for(int i = 0 ; i < myWantedGeneral.length(); i++){
                CardInfo cardInfo = null;
                try {
                    cardInfo = cardInfoTable.selectCardInfoBy_id(myWantedGeneral.getJSONObject(i).getString("card_id"));
                    cardInfo.setNonfoilQuantity(myWantedGeneral.getJSONObject(i).getInt(MongoKeys.nonfoilQuantity));
                    cardInfo.setFoilQuantity(myWantedGeneral.getJSONObject(i).getInt(MongoKeys.foilQuantity));
                    wantedGeneral.add(cardInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONArray tradersWantedGeneral = jsonObject.getJSONArray("tradersWantedGeneral");
            for(int i =0 ; i < tradersWantedGeneral.length(); i++){
                traderGeneral.add(new TraderWantedCard(tradersWantedGeneral.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void addCardsToCart(JSONArray jsonArray){
        CardInfoTable cardInfoTable = Motherbase.getInstance(mContext).getCardInfoTable();
        receiving.clear();
        giving.clear();
        for(int i = 0 ; i < jsonArray.length(); i++){

            try {
                CardInfo cardInfo = cardInfoTable.selectCardInfoBy_id(jsonArray.getJSONObject(i).getString("card_id"));
                CartCard cartCard = new CartCard(jsonArray.getJSONObject(i));

                if(jsonArray.getJSONObject(i).getString("trader_id").equals(mTrader_id)){
                    receiving.add(cartCard);
                }else{
                    giving.add(cartCard);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    void clearCart(){
        if(mTradeActivityInterface == null){
            return;
        }

        receiving.clear();
        giving.clear();
        mTradeActivityInterface.updateCart();

    }


    void receiveRemoveCartCard(JSONObject jsonObject){
        if(mTradeActivityInterface == null){
            return;
        }

        try {
            String trader_id = jsonObject.getString(MongoKeys.trader_id);
            CardInfo cardInfo = Motherbase.getInstance(mContext).getCardInfoTable().selectCardInfoBy_id(jsonObject.getString(MongoKeys.card_id));

            CartCard cartCard = new CartCard(jsonObject, cardInfo);
            if(trader_id.equals(mTradeActivityInterface.getTrader_id())){
                for(int i = 0; i < receiving.size(); i++){
                    if(cartCard.getCard_id().equals(receiving.get(i).getCard_id())){
                        receiving.remove(i);

                        final int index = i;
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                mTradeActivityInterface.updateCart();
                            }
                        });

                        return;
                    }
                }
            }else{
                for(int i = 0; i < giving.size(); i++){
                    if(cartCard.getCard_id().equals(giving.get(i).getCard_id())){
                        giving.remove(i);

                        final int index = i;
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                mTradeActivityInterface.updateCart();
                            }
                        });
                        return;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void receiveNewOrUpdated(JSONObject jsonObject){
        if(mTradeActivityInterface == null){
            return;
        }

        try {
            String trader_id = jsonObject.getString(MongoKeys.trader_id);
            CardInfo cardInfo = Motherbase.getInstance(mContext).getCardInfoTable().selectCardInfoBy_id(jsonObject.getString(MongoKeys.card_id));

            CartCard cartCard = new CartCard(jsonObject);
            boolean found = false;
            if(trader_id.equals(mTradeActivityInterface.getTrader_id())){

                for(int i = 0; i <  mTradeActivityInterface.getTradesActivityValues().getReceiving().size(); i++){
                    if(cartCard.getCard_id().equals( mTradeActivityInterface.getTradesActivityValues().getReceiving().get(i).getCard_id())){

                        found = true;
                        receiving.set(i, cartCard);

                        final int index = i;
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                mTradeActivityInterface.updateCart();
                            }
                        });
                    }
                }

                if(!found){
                    receiving.add(cartCard);
                }

            }else{

                for(int i = 0; i < giving.size(); i++){
                    if(cartCard.getCard_id().equals(giving.get(i).getCard_id())){
                        found = true;
                        giving.set(i, cartCard);
                    }
                }

                if(!found){
                    giving.add(cartCard);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTradeActivityInterface.updateCart();
            }
        });
    }

    public void receiveMessage(ChatMessage chatMessage){
        chatMessages.add(chatMessage);
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setmTradeActivityInterface(TradeActivityInterface mTradeActivityInterface) {
        this.mTradeActivityInterface = mTradeActivityInterface;
    }

    public List<ChatMessage> getChatMessages() {

        return chatMessages;
    }

    public List<CardInfo> getmWantedCards() {
        return mWantedCards;
    }

    public List<CardInfo> getmTradeCards() {
        return mTradeCards;
    }

    public List<CardInfo> getWantedGeneral() {
        return wantedGeneral;
    }

    public List<TraderWantedCard> getTraderGeneral() {
        return traderGeneral;
    }

    public List<CartCard> getReceiving() {
        return receiving;
    }

    public List<CartCard> getGiving() {
        return giving;
    }

    public String getmTrader_id() {
        return mTrader_id;
    }
}
