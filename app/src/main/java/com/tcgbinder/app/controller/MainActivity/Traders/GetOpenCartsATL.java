package com.tcgbinder.app.controller.MainActivity.Traders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.Objects.OpenCart;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Cart_getOpen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetOpenCartsATL extends AsyncTaskLoader<List<OpenCart>> {
    private static final String TAG = "##GetOpenCartsATL";


    public GetOpenCartsATL(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public List<OpenCart> loadInBackground() {

        JSONObject jsonObject = new ServerAPI(getContext()).serverRequestPOST(new Cart_getOpen());

        List<OpenCart> openCarts = new ArrayList<>();

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(Cart_getOpen.ResponseFields.openCarts);

            for(int i = 0; i < jsonArray.length(); i++){
                openCarts.add(new OpenCart(jsonArray.getJSONObject(i)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return openCarts;

    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
