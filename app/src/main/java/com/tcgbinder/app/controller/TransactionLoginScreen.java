package com.tcgbinder.app.controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.auth0.android.jwt.JWT;
import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MainActivity;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.TokenValues;
import com.tcgbinder.app.model.Location.LocationService;
import com.tcgbinder.app.model.Logout;
import com.tcgbinder.app.model.MakeDatabaseIS;
import com.tcgbinder.app.model.PermissionsUtils;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.Location.LocationUtils;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.AllInfo_getAllInfo;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;
import com.tcgbinder.app.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

public class TransactionLoginScreen extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "##TransactionLoginScree";
    LocationManager mLocationManager;
    TextView vFindingLocationTV, vDatabaseUpdateTV, vRetryConnectionTV;
    BuildCardDatabase mBuildCardDatabase = new BuildCardDatabase();
    Button vReconnectBT;
    boolean mServer, mCardDatabase = false;

    LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            mLocationManager.removeUpdates(this);
            makeAllRequests();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onProviderEnabled(String provider) {

        }

        public void onProviderDisabled(String provider) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_login_screen);

        SharedPreferencesUtils sharedPreferencesUtils = SharedPreferencesUtils.getInstance(this);
        String token = sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.token);
        buildView();

        if(token != null){
            JWT jwt = new JWT(token);
            int accountStatus = jwt.getClaim(TokenValues.accountStatus).asInt();

            switch (accountStatus){
                case 0:
//                    Intent intent = new Intent(this, EmailConfirmationActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
//                    startActivity(intent);
//                    finish();
                break;
                case 1:
                    if (PermissionsUtils.checkAndRequestPermissions(this)) {
                        FindLocation();
                    }else{

                    }
                break;
                default:
                    Log.e(TAG, "Not Suppose to be here");
            }
        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

    }


    void buildView(){
        vFindingLocationTV = findViewById(R.id.activityTransactionLoginScreen_findingLocationTV);
        vDatabaseUpdateTV = findViewById(R.id.activityTransactionLoginScreen_databaseUpdateTV);
        vReconnectBT = findViewById(R.id.activityTransactionLoginScreen_retryConnectionBT);
        vRetryConnectionTV = findViewById(R.id.activityTransactionLoginScreen_connectionProblemTV);


        //LISTENER
        vReconnectBT.setOnClickListener(this);
    }

    public void FindLocation() {

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        vFindingLocationTV.setVisibility(View.VISIBLE);



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location location = LocationUtils.getLastKnownLocation(getBaseContext());


        if(location != null){
            makeAllRequests();
        }else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 1000, mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 1000, mLocationListener);
        }
}

    private void makeAllRequests(){
        mBuildCardDatabase.execute();
        new firstLogin().execute();
    }

    private void loginIn(){

        if(mServer && mCardDatabase){
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onClick(View view) {
        if(view == vReconnectBT){
            vReconnectBT.setVisibility(View.GONE);
            new firstLogin().execute();

        }
    }


    class firstLogin extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            Motherbase.getInstance(getBaseContext()).createTableIfDontExists();

            Intent intent = new Intent(getBaseContext(), MakeDatabaseIS.class);

            startService(intent);

            JSONObject jsonObject = new ServerAPI(getBaseContext()).serverRequestPOST(new AllInfo_getAllInfo(getBaseContext()));

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case AllInfo_getAllInfo.Response.userFound :

                        Motherbase.getInstance(getBaseContext()).rebuildDatabase();

                        try {
                            JSONObject userData = jsonObject.getJSONObject("userData");
                            Motherbase.getInstance(getBaseContext()).getCardInfoTable().updatePricesFromJSON(userData.getJSONArray(MongoKeys.wanted));
                            Motherbase.getInstance(getBaseContext()).getCardInfoTable().updatePricesFromJSON(userData.getJSONArray(MongoKeys.cards));

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    case AllInfo_getAllInfo.Response.userNotFound :

                    default :

                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case AllInfo_getAllInfo.Response.userFound:

                            JSONObject userData = jsonObject.getJSONObject("userData");
                            SharedPreferencesUtils.getInstance(getBaseContext()).updateUser(userData, null);
                            Motherbase.getInstance(getBaseContext()).getMyWantedGeneralTable().insertOrReplaceAllCards(userData.getJSONArray(MongoKeys.wantedCards));
                            Motherbase.getInstance(getBaseContext()).getMyCardsTable().insertOrReplaceAllCards(userData.getJSONArray(MongoKeys.cards));
                            Motherbase.getInstance(getBaseContext()).getMyWantedCardsTable().insertOrReplaceAllCards(userData.getJSONArray(MongoKeys.wanted));
                            mServer = true;
                            loginIn();
                            break;
                        case "401" :
                        case AllInfo_getAllInfo.Response.userNotFound:
                            Logout.logout(getCurrentActivity());
                            mBuildCardDatabase.cancel(true);
                            break;
                    }
                }else{
                    vReconnectBT.setVisibility(View.VISIBLE);
                    vRetryConnectionTV.setVisibility(View.VISIBLE);

                    //                vServerStatusTV.setText("Server is Offline");
                    //                vServerStatusTV.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean permissionGranted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == -1) {
                permissionGranted = false;
            }
        }

        if (permissionGranted) {
            FindLocation();
        } else {


//            llGivePermissions.setVisibility(View.VISIBLE);
//            btGivePermissions.setVisibility(View.VISIBLE);
        }
    }


    class BuildCardDatabase extends AsyncTask<String, String, JSONObject> implements BuildCardDatabaseInterface{

        JSONObject jsonObject;

        @Override
        protected JSONObject doInBackground(String... params) {

            jsonObject = getAppInfoJSON();

            int tableQuantity = Motherbase.getInstance(getBaseContext()).getCardInfoTable().getQuantity();
            try {
                if(tableQuantity < jsonObject.getInt("quantityOfCards")){
                    Motherbase.getInstance(getBaseContext()).getCardInfoTable().readFilesTable(getBaseContext(), this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            try {
                vDatabaseUpdateTV.setText("Building Cards Database. Cards " + values[0] + " of " + jsonObject.getInt("quantityOfCards"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            mCardDatabase = true;
            loginIn();
        }

        @Override
        public void updateCardDatabaseValue(int value) {
            publishProgress(Integer.toString(value));
        }
    }

    private JSONObject getAppInfoJSON() {

        String json = null;
        try {
            InputStream is = getBaseContext().getAssets().open("appInfo.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

            return new JSONObject(json);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            try {
                mLocationManager.removeUpdates(mLocationListener);
            } catch (Exception ex) {
                Log.d(TAG, "fail to remove location listners, ignore", ex);
            }

        }
    }


    private Activity getCurrentActivity(){
        return this;
    }
}
