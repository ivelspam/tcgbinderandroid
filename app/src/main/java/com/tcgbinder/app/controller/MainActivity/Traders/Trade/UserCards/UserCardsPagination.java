package com.tcgbinder.app.controller.MainActivity.Traders.Trade.UserCards;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MyCollection.CardsAA;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivityInterface;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.WantedAA;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.CardsInfo_getPrice;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Traders_getCardsByPage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class UserCardsPagination extends Fragment {
    private final String TAG = "##" + this.getClass().getSimpleName();


    List<CardInfo> mTraderCards = new ArrayList<>();
    RecyclerView vTraderCardsRV;
    ProgressBar vloadingPB;
    WantedAA mWantedAA;

    private int mPageNumber = 0;
    private int mItemCount = 10;

    //pagination
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemsCount, totalItemCount, previousTotal = 0;
    private int viewThreashold = 0;
    private TradeActivityInterface mTradeActivityInterface;
    private LinearLayoutManager linearLayoutManager;
    public UserCardsPagination() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWantedAA = new WantedAA(getContext(), mTradeActivityInterface);

    }

    public void setListenerer(TradeActivityInterface tradeActivityInterface){
        mTradeActivityInterface = tradeActivityInterface;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
            build_view();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_cards_pagination, container, false);
    }

    private void build_view(){
        vTraderCardsRV = getView().findViewById(R.id.fragmentUserCardsPagination_traderCardsRV);
        vloadingPB = getView().findViewById(R.id.fragmentUserCardsPagination_loadingPG);

//        ADAPTERS
        vTraderCardsRV.setAdapter(mWantedAA);
        linearLayoutManager = new LinearLayoutManager(getContext());

        vTraderCardsRV.setLayoutManager(linearLayoutManager);


        vTraderCardsRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemsCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0){

                    if(isLoading){
                        if(totalItemCount > previousTotal){
                            isLoading = false;
                            previousTotal = totalItemCount;
                        }
                    }

                    if(!isLoading && (totalItemCount - visibleItemsCount) <= (pastVisibleItems + viewThreashold)){
                        mPageNumber++;
                        vloadingPB.setVisibility(View.VISIBLE);
                        new userCardsPagination().execute();
                        isLoading = true;
                    }
                }
            }
        });

        if(mTraderCards.size() == 0){
            vloadingPB.setVisibility(View.VISIBLE);
            new userCardsPagination().execute();

        }
        mWantedAA.updateCards(mTraderCards);

    }

    class userCardsPagination extends AsyncTask<Void, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getContext()).serverRequestGET(new Traders_getCardsByPage(mTradeActivityInterface.getTrader_id(), mPageNumber, mItemCount));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case Traders_getCardsByPage.Response.endOfList:
                    case Traders_getCardsByPage.Response.found:
                        CardInfoTable cardInfoTable = Motherbase.getInstance(getContext()).getCardInfoTable();
                        JSONArray jsonArray = jsonObject.getJSONArray(Traders_getCardsByPage.ResponseFields.cards);
                            for(int i =0 ; i < jsonArray.length(); i++){
                                CardInfo cardInfo = null;
                                try {

                                    cardInfo = cardInfoTable.selectCardInfoBy_id(jsonArray.getJSONObject(i).getString("card_id"));
                                    cardInfo.setNonfoilQuantity(jsonArray.getJSONObject(i).getInt(MongoKeys.nonfoilQuantity));
                                    cardInfo.setFoilQuantity(jsonArray.getJSONObject(i).getInt(MongoKeys.foilQuantity));
                                    mTraderCards.add(cardInfo);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            vloadingPB.setVisibility(View.GONE);

            mWantedAA.updateCards(mTraderCards);
       }
    }


}
