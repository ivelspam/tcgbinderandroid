package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.Objects.ChatMessage;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;


public class SQLiteChatMessagesATL extends AsyncTaskLoader<List<ChatMessage>> {
    static private String TAG = "##SQLiteChatMessagesATL";

    String mTrader_id;

    public SQLiteChatMessagesATL(Context context, String trader_id) {
        super(context);
        mTrader_id = trader_id;
    }

    @Override
    public List<ChatMessage> loadInBackground() {
        return Motherbase.getInstance(getContext()).getChatMessageTable().selectAllChatsFromTrader(mTrader_id);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
