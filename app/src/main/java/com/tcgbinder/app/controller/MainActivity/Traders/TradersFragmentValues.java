package com.tcgbinder.app.controller.MainActivity.Traders;

import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.OpenCart;
import com.tcgbinder.app.model.Objects.TraderSimple;
import com.tcgbinder.app.model.Objects.TraderWithQuantity;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Traders_getCloserTradersAndOpenCarts;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Traders_getCloserTradersWithInfoAndCart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TradersFragmentValues {
    private static final String TAG = "##TradersFragmentValues";


    public List<OpenCart> openCarts = new ArrayList<>();
    public List<TraderWithQuantity> tradersWithQuantities = new ArrayList<>();
    public List<TraderSimple> tradersSimples = new ArrayList<>();


    public void insertValues(JSONObject jsonObject){
        openCarts.clear();
        tradersSimples.clear();

        if(jsonObject.has(Traders_getCloserTradersAndOpenCarts.ResponseFields.openCarts)){

            try {

                JSONArray openCardsJSONArray = jsonObject.getJSONArray(Traders_getCloserTradersWithInfoAndCart.ResponseFields.openCarts);

                for(int i = 0; i < openCardsJSONArray.length(); i++){
                    openCarts.add(new OpenCart(openCardsJSONArray.getJSONObject(i)));
                }

                if(jsonObject.has(Traders_getCloserTradersAndOpenCarts.ResponseFields.closerTraders)){

                    JSONArray closerTraders = jsonObject.getJSONArray(Traders_getCloserTradersWithInfoAndCart.ResponseFields.closerTraders);
                    JSONArray setCardsQuantities = jsonObject.getJSONArray(Traders_getCloserTradersWithInfoAndCart.ResponseFields.setCardsQuantity);
                    JSONArray anyCardsQuantities = jsonObject.getJSONArray(Traders_getCloserTradersWithInfoAndCart.ResponseFields.anyCardsQuantity);

                    for(int i = 0 ; i < closerTraders.length(); i++){
                        tradersSimples.add(new TraderSimple(closerTraders.getJSONObject(i)));
                        tradersWithQuantities.add(new TraderWithQuantity(closerTraders.getJSONObject(i)));
                    }

                    for(int i = 0 ; i < setCardsQuantities.length(); i++){
                        for(int j = 0; j < tradersWithQuantities.size(); j++){
                            if(setCardsQuantities.getJSONObject(i).getString(MongoKeys._id).equals(tradersWithQuantities.get(j).get_id())){
                                    tradersWithQuantities.get(j).setSetQuantity(setCardsQuantities.getJSONObject(i));
                            }
                        }
                    }

                    for(int i = 0 ; i < anyCardsQuantities.length(); i++){
                        for(int j = 0; j < tradersWithQuantities.size(); j++){
                            if(anyCardsQuantities.getJSONObject(i).getString(MongoKeys._id).equals(tradersWithQuantities.get(j).get_id())){
                                tradersWithQuantities.get(j).setAnySetQuantity(anyCardsQuantities.getJSONObject(i));
                            }
                        }
                    }


                    int index = 0;
                    while (index < tradersWithQuantities.size() ){
                        if(!tradersWithQuantities.get(index).isHasCards()){
                            tradersWithQuantities.remove(index);
                            continue;
                        }
                        index++;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
