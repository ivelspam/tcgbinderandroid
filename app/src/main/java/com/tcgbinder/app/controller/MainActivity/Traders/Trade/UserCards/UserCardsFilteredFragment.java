package com.tcgbinder.app.controller.MainActivity.Traders.Trade.UserCards;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivityInterface;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.CardInfoTable;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_getTradersCardsByToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserCardsFilteredFragment extends Fragment {


    private final String TAG = "##" + this.getClass().getSimpleName();

    private RecyclerView vCardsRV;
    private TextView vTokenTV;

    FoundCardsAA mFoundCardsAA = new FoundCardsAA();

    List<CardInfo> mFoundCards = new ArrayList<>();

    private TradeActivityInterface mTradeActivityInterface;

    public UserCardsFilteredFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_cards_filtered, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        build_view();
    }

    private void build_view(){

        vCardsRV = getView().findViewById(R.id.userCardsFilteredFragment_cardsRV);
        vTokenTV = getView().findViewById(R.id.userCardsFilteredFragment_tokenTV);


        vTokenTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mHandler.removeCallbacks(input_finish_checker);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 2) {
                    last_text_edit = System.currentTimeMillis();
                    mHandler.postDelayed(input_finish_checker, delay);
                } else {

                }
            }
        });

        vCardsRV.setAdapter(mFoundCardsAA);
        vCardsRV.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTradeActivityInterface = null;
    }

    class FoundCardsAA extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_wanted_cards, parent, false);
            return new FoundCardsAA.cardInfoHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((FoundCardsAA.cardInfoHolder) holder).bind(mFoundCards.get(position), position);
        }

        @Override
        public int getItemCount() {
            return mFoundCards.size();
        }

        private class cardInfoHolder extends RecyclerView.ViewHolder {

            TextView nameTV, nonfoilPriceTV, foiPriceTV;
            ImageView setCodeIV;
            EditText nonfoilQuantityET, foilQuantityET;
            CardInfo mCardInfo;
            Button vAddToCardBT;
            int mPosition;


            public cardInfoHolder(View itemView) {
                super(itemView);
                setCodeIV = itemView.findViewById(R.id.adapterRowWantedCards_setCodeIV);
                nameTV = itemView.findViewById(R.id.adapterRowWantedCards_nameTV);
                foilQuantityET = itemView.findViewById(R.id.adapterRowWantedCards_foilQuantityET);
                nonfoilQuantityET = itemView.findViewById(R.id.adapterRowWantedCards_nonfoilQuantityET);
                nonfoilPriceTV = itemView.findViewById(R.id.adapterRowWantedCards_nonFoilPriceTV);
                foiPriceTV = itemView.findViewById(R.id.adapterRowWantedCards_foilPriceTV);
                vAddToCardBT = itemView.findViewById(R.id.adapterRowWantedCards_addToCardBT);
            }

            public void bind(final CardInfo cardInfo, final int position) {
                mPosition = position;
                //initial state
                mCardInfo = cardInfo;

                nameTV.setText(cardInfo.getName());

                Resources resources = getContext().getResources();
                String imageIndentifier = "s"+cardInfo.getSet_code() + "_svg";
                int add = resources.getIdentifier(imageIndentifier, "drawable", getContext().getPackageName());
                setCodeIV.setImageResource(add);

                nonfoilQuantityET.setText(Integer.toString(cardInfo.getNonfoilQuantity()), TextView.BufferType.EDITABLE);

                foilQuantityET.setText(Integer.toString(cardInfo.getFoilQuantity()), TextView.BufferType.EDITABLE);

                if (cardInfo.getUsd() != null) {
                    nonfoilPriceTV.setText(Double.toString(cardInfo.getUsd()));
                }else{
                    nonfoilPriceTV.setText("N/A");
                }

                vAddToCardBT.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTradeActivityInterface.addCartToCard(cardInfo.get_id(), Integer.parseInt(nonfoilQuantityET.getText().toString()), Integer.parseInt(foilQuantityET.getText().toString()));
                    }
                });
            }
        }
    }

    long delay = 600;
    long last_text_edit = 0;
    Handler mHandler = new Handler();

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                new GetCardsByToken().execute();
            }
        }
    };

    class GetCardsByToken extends AsyncTask<Void, Void, JSONObject> {


        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = new ServerAPI(getContext()).serverRequestGET(new UserCards_getTradersCardsByToken(vTokenTV.getText().toString(), mTradeActivityInterface.getTrader_id()));

            try {
                switch (jsonObject.getString(APIRequest.message)) {
                    case UserCards_getTradersCardsByToken.Response.done:

                        CardInfoTable cardInfoTable = Motherbase.getInstance(getContext()).getCardInfoTable();
                        JSONArray jsonArray = jsonObject.getJSONArray(UserCards_getTradersCardsByToken.ResponseFields.cards);

                        cardInfoTable.updatePricesFromJSON(jsonArray);

                        mFoundCards.clear();

                        for(int i = 0 ; i < jsonArray.length(); i++){

                            JSONObject cardJSON = jsonArray.getJSONObject(i);
                            CardInfo cardInfo = cardInfoTable.selectCardInfoBy_id(cardJSON.getString(MongoKeys._id));
                            cardInfo.setNonfoilQuantity(cardJSON.getInt(MongoKeys.nonfoilQuantity));
                            cardInfo.setFoilQuantity(cardJSON.getInt(MongoKeys.foilQuantity));
                            mFoundCards.add(cardInfo);
                        }

                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            mFoundCardsAA.notifyDataSetChanged();

        }
    }



}
