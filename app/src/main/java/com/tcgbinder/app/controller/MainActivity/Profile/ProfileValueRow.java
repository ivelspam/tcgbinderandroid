package com.tcgbinder.app.controller.MainActivity.Profile;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

public class ProfileValueRow {

    private static final String TAG = "SharedPreferencesUtils";
    public static final String[] rows = new String[]{"Name", "Open For Sharing", "Trade Info", "Delete Account", "Log Out"};

    String name, value;
    int icon;
    Intent intent;
    Context mContext;

    public ProfileValueRow(Context context, String name){
        this.name = name;
        mContext = context;
        setIcon();
        setValue();
        setIntent();
    }

    public String getName() {
        return name;
    }

    public int getIcon(){return icon;}

    private void setIcon(){
        switch (name){
            case "Name":

            return;
            case "Open For Sharing":

                return;
            case "Delete Account":

                return;
            case "Log Out":

            return;
        }
    }

    public String getValue(){return value;}

    private void setValue(){
        switch (name){
            case "Name":
                value = SharedPreferencesUtils.getInstance(mContext).getString(SharedPreferencesUtils.PrefKeys.name);
                return;
            case "Open For Sharing":

                return;
            case "Delete Account":
                value = "";
                return;
            case "Log Out":
                value = "";
                return;
        }
    }

    public Intent getIntent(){return intent;}

    private void setIntent(){
        switch (name){
            case "Name":

                break;
            case "Open For Sharing":

                return;
            case "Delete Account":

                break;
            case "Log Out":
//                intent = new Intent(mContext, EditDistanceActivity.class);
                break;
        }
    }
}
