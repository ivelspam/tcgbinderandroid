package com.tcgbinder.app.controller.MainActivity.MyCollection;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;

public class GetWantedGeneralCardsATL extends AsyncTaskLoader<List<WantedCard>> {
    private static final String TAG = "##AddCardDialogFragment";

    int mType;
    List<WantedCard> mData;

    public GetWantedGeneralCardsATL(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public List<WantedCard> loadInBackground() {
        return Motherbase.getInstance(getContext()).getMyWantedGeneralTable().selectAllCards();
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        }

        if (takeContentChanged() || mData == null) {
            forceLoad();
            Intent  intent = new Intent("my-event");
            intent.putExtra("message", "message");
            getContext().sendBroadcast(intent);
        }

        forceLoad();

    }

    @Override
    public void deliverResult(@Nullable List<WantedCard> data) {
        if(isReset()){
            releaseResources(data);
        }

        List<WantedCard> oldData = mData;
        mData = data;

        if(isStarted()){
            super.deliverResult(data);
        }

        if(oldData != null && oldData != data){
            releaseResources(oldData);
        }

        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        // Ensure the loader has been stopped.
        onStopLoading();

        // At this point we can release the resources associated with 'mData'.
        if (mData != null) {
            releaseResources(mData);
            mData = null;
        }

        // The Loader is being reset, so we should stop monitoring for changes.
        ////if (mObserver != null) {
        // TODO: unregister the observer
        //// mObserver = null;
        ////}

        //if (mObserver != null) {
        //    try {
        //        getContext().unregisterReceiver(mObserver);
        //    } catch (Exception e) {
        //        e.printStackTrace();
        //    }
        //    mObserver = null;
        //}

    }

    private void releaseResources(List<WantedCard> data) {
        // For a simple List, there is nothing to do. For something like a Cursor, we
        // would close it in this method. All resources associated with the Loader
        // should be released here.
    }

    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        cancelLoad();

        // Note that we leave the observer as is. Loaders in a stopped state
        // should still monitor the data source for changes so that the Loader
        // will know to force a new load if it is ever started again.
    }

    @Override
    public void onCanceled(List<WantedCard> data) {
        // Attempt to cancel the current asynchronous load.
        super.onCanceled(data);

        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        releaseResources(data);
    }

//    private BroadcastReceiver mObserver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // Extract data included in the Intent
//            //String message = intent.getStringExtra("message");
//            Log.d("receiver", "Got message: ");
//        }
//    };


}
