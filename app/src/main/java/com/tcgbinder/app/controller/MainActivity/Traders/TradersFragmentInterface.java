package com.tcgbinder.app.controller.MainActivity.Traders;

import com.tcgbinder.app.model.Objects.OpenCart;
import com.tcgbinder.app.model.Objects.TraderSimple;
import com.tcgbinder.app.model.Objects.TraderWithQuantity;

import java.util.List;

public interface TradersFragmentInterface {

    List<TraderWithQuantity> getTradersWithQuantities();
    List<TraderSimple> getTradersSimple();

    List<OpenCart> getOpenCarts();

}
