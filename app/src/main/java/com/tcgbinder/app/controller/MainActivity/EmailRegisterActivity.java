package com.tcgbinder.app.controller.MainActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.LoginActivity;
import com.tcgbinder.app.controller.TransactionLoginScreen;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Email_addUser;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailRegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "##LoginActivity";


    EditText vEmailET, vEmailConfirmedET, vNameET, vPasswordET, vPasswordConfirmedET;
    Button vRegisterBT;
    RegisterUserAT mRegisterUserAT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_register);
        build_view();
    }



    void build_view(){
        vEmailET =  findViewById(R.id.activityEmailRegister_emailET);
        vEmailConfirmedET = findViewById(R.id.activityEmailRegister_confirmEmailET);
        vPasswordET = findViewById(R.id.activityEmailRegister_passwordET);
        vPasswordConfirmedET = findViewById(R.id.activityEmailRegister_confirmPasswordET);
        vRegisterBT = findViewById(R.id.activityEmailRegister_registerBT);
        vNameET = findViewById(R.id.activityEmailRegister_nameET);
        vRegisterBT.setOnClickListener(this);

    }


    private void attemptToRegister() {
        if (mRegisterUserAT != null) {
            return;
        }

        vEmailET.setError(null);
        vEmailConfirmedET.setError(null);
        vPasswordET.setError(null);
        vPasswordConfirmedET.setError(null);


        String name = vNameET.getText().toString();
        String email = vEmailET.getText().toString();
        String emailConfirmed = vEmailConfirmedET.getText().toString();
        String password = vPasswordET.getText().toString();
        String passwordConfirmed = vPasswordConfirmedET.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(name)) {
            vNameET.setError(getString(R.string.error_field_required));
            focusView = vNameET;
            cancel = true;
        }

        //EMAIL
        else if (TextUtils.isEmpty(email)) {

            vEmailET.setError(getString(R.string.error_field_required));
            focusView = vEmailET;
            cancel = true;
        } else if (!isEmailValid(email)) {
            vEmailET.setError(getString(R.string.error_invalid_email));
            focusView = vEmailET;
            cancel = true;
        }
        //EMAIL CONFIRMED

        else if (TextUtils.isEmpty(emailConfirmed)) {

            vEmailConfirmedET.setError(getString(R.string.error_field_required));
            focusView = vEmailConfirmedET;
            cancel = true;
        } else if (!isEmailValid(emailConfirmed)) {

            vEmailConfirmedET.setError(getString(R.string.error_invalid_email));
            focusView = vEmailConfirmedET;
            cancel = true;
        }
        //PASSWORD
        else if (TextUtils.isEmpty(password)) {
            vPasswordET.setError(getString(R.string.error_field_required));
            focusView = vPasswordET;
            cancel = true;
        }else if (password.length() < 8) {
            vPasswordET.setError(getString(R.string.error_password_too_short));
            focusView = vPasswordET;
            cancel = true;
        }else if (password.length() > 32) {
            vPasswordET.setError(getString(R.string.error_password_too_big));
            focusView = vPasswordET;
            cancel = true;
        }else if (stringConstainsSpace(password)) {
            vPasswordET.setError(getString(R.string.error_password_cant_contains_space));
            focusView = vPasswordET;
            cancel = true;
        }


        //PASSWORD CONFIRMED
        else if (TextUtils.isEmpty(passwordConfirmed)) {
            vPasswordConfirmedET.setError(getString(R.string.error_field_required));
            focusView = vPasswordConfirmedET;
            cancel = true;
        }else if (passwordConfirmed.length() < 8) {
            vPasswordConfirmedET.setError(getString(R.string.error_password_too_short));
            focusView = vPasswordConfirmedET;
            cancel = true;
        }else if (passwordConfirmed.length() > 32) {
            vPasswordConfirmedET.setError(getString(R.string.error_password_too_big));
            focusView = vPasswordConfirmedET;
            cancel = true;
        }else if (stringConstainsSpace(passwordConfirmed)) {
            vPasswordConfirmedET.setError(getString(R.string.error_password_cant_contains_space));
            focusView = vPasswordConfirmedET;
            cancel = true;
        }

        //EMAIL DIFERENT
        else if (!email.equals(emailConfirmed)) {
            vEmailET.setError(getString(R.string.error_email_are_different));
            vEmailConfirmedET.setError(getString(R.string.error_email_are_different));
            focusView = vEmailET;
            cancel = true;
        }else if (!password.equals(passwordConfirmed)) {
            vPasswordET.setError(getString(R.string.error_password_are_different));
            vPasswordConfirmedET.setError(getString(R.string.error_password_are_different));
            focusView = vPasswordET;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
//            showProgress(true);
            mRegisterUserAT = new RegisterUserAT(email, name, password);
            mRegisterUserAT.execute((Void) null);
        }
    }

    public boolean stringConstainsSpace(String password) {
        return password.matches("/\\s/");
    }

    public boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onClick(View view) {
        attemptToRegister();
    }

    class RegisterUserAT extends AsyncTask<Void, Void, JSONObject>{


        String aPassword, aEmail, aName;
        public RegisterUserAT(String email, String name, String password){
            aEmail = email;
            aPassword = password;
            aName = name;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = new ServerAPI(getBaseContext()).serverRequestPOST(new Email_addUser( aName, aEmail, aPassword));
            return jsonObject ;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            mRegisterUserAT = null;
            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case Email_addUser.Response.registered:
                        onLoginSuccess(jsonObject);
                        break;
                    case Email_addUser.Response.accountExists:
                        Toast.makeText(EmailRegisterActivity.this, "This Email is Already Registered.",
                                Toast.LENGTH_LONG).show();
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void onLoginSuccess(JSONObject loginInformation) throws JSONException {
        SharedPreferencesUtils.getInstance(this).put(SharedPreferencesUtils.PrefKeys.token, loginInformation.getString("token"));
        Intent intent = new Intent(this, TransactionLoginScreen.class);
        startActivity(intent);
        finish();
    }

}
