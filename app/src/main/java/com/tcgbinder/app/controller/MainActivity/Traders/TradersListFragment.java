package com.tcgbinder.app.controller.MainActivity.Traders;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivity;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.TraderSimple;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_UpdateDistanceAndMeasurementSystem;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class TradersListFragment extends Fragment{
    private static final String TAG = "##TradersListFragment";

    TraderSimpleAA traderSimpleAA = new TraderSimpleAA();
    TradersFragmentInterface mTradersFragmentInterface;

    public TradersListFragment() {

    }

    RecyclerView vListRV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_traders_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buildView();

    }

    public void insertInterface(TradersFragmentInterface tradersFragmentInterface){
        mTradersFragmentInterface = tradersFragmentInterface;
    }

    private void buildView() {
        vListRV = getView().findViewById(R.id.fragmentTradersList_traderSimpleRV);

        //INITIAL STATE
//        vMeasurementRG.check(R.id.fragmentTradersList_kmRB);
        String measurement = SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.measurementSystem);

        //ADAPTERS
        vListRV.setAdapter(traderSimpleAA);
        vListRV.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    void updateView(){
        traderSimpleAA.notifyDataSetChanged();
    }

    class TraderSimpleAA extends RecyclerView.Adapter {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_trader_simple, parent, false);
            return new traderSimpleHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((traderSimpleHolder) holder).bind(mTradersFragmentInterface.getTradersSimple().get(position));

        }

        @Override
        public int getItemCount() {
            return mTradersFragmentInterface.getTradersSimple().size();
        }

        private class traderSimpleHolder extends RecyclerView.ViewHolder {

            TextView nameTV, distanceTV, tradeInfoTV;
            ConstraintLayout vParentCL;


            public traderSimpleHolder(View itemView) {
                super(itemView);
                nameTV = itemView.findViewById(R.id.adapterRowTraderSimple_nameTV);
                distanceTV = itemView.findViewById(R.id.adapterRowTraderSimple_distanceTV);
                vParentCL = itemView.findViewById(R.id.fragmentTradersList_parentCL);
                tradeInfoTV = itemView.findViewById(R.id.adapterRowTraderSimple_tradeInfoTV);
            }

            public void bind(final TraderSimple traderSimple) {
                nameTV.setText(traderSimple.getName());


                String measurementSystem = SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.measurementSystem);

                if (measurementSystem.equals("km")) {

                    if(traderSimple.getDistance() <= 1){
                        distanceTV.setText("Less than " + 2 + " KM.");
                    }else{
                        distanceTV.setText("About " + (int)(traderSimple.getDistance().intValue() * 1.6) + " KM.");
                    }

                } else if (measurementSystem.equals("mi")) {

                    if(traderSimple.getDistance() <= 1){
                        distanceTV.setText("Less than " + 1 + " Miles.");
                    }else{
                        distanceTV.setText("About " + traderSimple.getDistance().intValue() + " Mile.");
                    }

                }

                tradeInfoTV.setText(traderSimple.getTradeInfo());
                vParentCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), TradeActivity.class);
                        intent.putExtra(MongoKeys._id, traderSimple.get_id());
                        intent.putExtra(MongoKeys.name, traderSimple.getName());
                        startActivityForResult(intent, 0);
                    }
                });
            }
        }


    }

}