package com.tcgbinder.app.controller.MainActivity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MyCollection.MyCollectionFragment;
import com.tcgbinder.app.controller.MainActivity.Profile.ProfileFragment;
import com.tcgbinder.app.controller.MainActivity.Traders.NonSwipeableViewPager;
import com.tcgbinder.app.controller.MainActivity.Traders.TradersFragment;
import com.tcgbinder.app.model.ExampleJobService;
import com.tcgbinder.app.model.Location.LocationService;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "##MainActivity";

    private NonSwipeableViewPager vPagerVP;
    private BottomNavigationView vNavigationBNV;

    private ProfileFragment mProfileFragment = new ProfileFragment();
    private TradersFragment  mTradersFragment = new TradersFragment();
    private MyCollectionFragment mMyCollectionFragment = new MyCollectionFragment();

    private MainActivityPA mMainActivityPA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildView();
        scheduleJob();
    }


    private void scheduleJob(){

        ComponentName componentName = new ComponentName(this, ExampleJobService.class);
        JobInfo info = new JobInfo.Builder(ExampleJobService.ID, componentName)
                .setRequiresCharging(true)

//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)

//                .setPersisted(true)
                .setPeriodic(2 * 60 * 60 * 1000)
                .build();

        if(isJobServiceOn()){
            Log.i(TAG, "WORKING");
        }else{
            JobScheduler scheduler = (JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
            int resultCode = scheduler.schedule(info);
        }
    }

    public boolean isJobServiceOn() {
        JobScheduler scheduler = (JobScheduler) getBaseContext().getSystemService( getBaseContext().JOB_SCHEDULER_SERVICE ) ;

        boolean hasBeenScheduled = false ;

        for ( JobInfo jobInfo : scheduler.getAllPendingJobs() ) {
            if ( jobInfo.getId() == ExampleJobService.ID ) {
                hasBeenScheduled = true ;
                break ;
            }
        }
        return hasBeenScheduled ;
    }

    public void cancelJob(){

        JobScheduler scheduler = (JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(123);


    }
    private void buildView(){
        vNavigationBNV = findViewById(R.id.activityMain_navigationBNV);

        vPagerVP = findViewById(R.id.activityMain_pager);

        vNavigationBNV.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mMainActivityPA = new MainActivityPA(getSupportFragmentManager());
        vPagerVP.setAdapter(mMainActivityPA);
        vNavigationBNV.setSelectedItemId(R.id.navigation_traders);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    vPagerVP.setCurrentItem(0, true);
                    return true;
                case R.id.navigation_traders:
                    vPagerVP.setCurrentItem(1, true);
                    return true;
                case R.id.navigation_collection:
                    vPagerVP.setCurrentItem(2, true);
                    return true;
            }
            return false;
        }
    };


    public class MainActivityPA extends FragmentPagerAdapter {

        public MainActivityPA(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return mProfileFragment;
                case 1 :
                    return mTradersFragment;
                case 2 :
                    return mMyCollectionFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
