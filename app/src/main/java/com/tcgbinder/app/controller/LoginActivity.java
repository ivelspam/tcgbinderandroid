package com.tcgbinder.app.controller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.tcgbinder.app.BuildConfig;
import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.EmailRegisterActivity;
import com.tcgbinder.app.model.MakeDatabaseIS;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Email_login;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Facebook_login;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Google_login;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements OnClickListener {
    private static final String TAG = "##LoginActivity";
    private static final int RC_SIGN_IN = 999;

    private UserLoginTask mAuthTask = null;
    private static final String EMAIL = "email";

    // UI references.
    private EditText vEmailET, vPasswordET;
    private TextView vCreateAccountTV, vBuildVersionTV, vBuildDateTV;
    private View vProgressView;
    private Button vEmailSignInBT;
    LoginButton vFacebookLoginBT;
    GoogleSignInClient mGoogleSignInClient;
    FacebookLoginAT mFacebookLoginAT;

    GoogleLoginAT mGoogleLoginAT;

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        build_view();
        printToken();
    }

    void printToken(){
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        try {
            int v = getPackageManager().getPackageInfo(GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE, 0 ).versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    void build_view(){
        // Set up the login form.
        vEmailET = findViewById(R.id.activityLogin_email);
        vPasswordET = findViewById(R.id.activityLogin_password);
        vEmailSignInBT = findViewById(R.id.activityLogin_signInButton);
        vCreateAccountTV = findViewById(R.id.activityLogin_createAccountTV);
        vProgressView = findViewById(R.id.activityLogin_loginProgress);
        vFacebookLoginBT = findViewById(R.id.activityLogin_facebookLoginBR);
        SignInButton signInButton = findViewById(R.id.activityLogin_googleSignInBT);
        vBuildVersionTV = findViewById(R.id.activityTransactionLoginScreen_versionTV);
        vBuildDateTV = findViewById(R.id.activityTransactionLoginScreen_buildDateTV);

        //AASDd
        signInButton.setSize(SignInButton.SIZE_WIDE);

        //initial states
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yy HH:mm");
        vBuildDateTV.setText("BUILD DATE: " + simpleDateFormat.format(buildDate));
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            vBuildVersionTV.setText("VERSION: " +versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //listeners
        vEmailSignInBT.setOnClickListener(this);
        vCreateAccountTV.setOnClickListener(this);
        signInButton.setOnClickListener(this);

//        Intent intent = new Intent(getBaseContext(), MakeDatabaseIS.class);
//        startService(intent);

        temp();
    }

    //FACEBOOK

    void temp(){

        //INITIAL STATE
        callbackManager = CallbackManager.Factory.create();

        vFacebookLoginBT.setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        vFacebookLoginBT.setReadPermissions(Arrays.asList(EMAIL));

        // Callback registration
        vFacebookLoginBT.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                if(mFacebookLoginAT != null){
                    return;
                }

                String userId = loginResult.getAccessToken().getUserId();
                String token =  loginResult.getAccessToken().getToken();

                mFacebookLoginAT = new FacebookLoginAT(userId, token);
                mFacebookLoginAT.execute();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

            }
        });
    }


    private void attemptLogin() {

        if (mAuthTask != null) {
            return;
        }

        vEmailET.setError(null);
        vPasswordET.setError(null);

        String email = vEmailET.getText().toString();
        String password = vPasswordET.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {

            vEmailET.setError(getString(R.string.error_field_required));
            focusView = vEmailET;
            cancel = true;
        } else if (!isEmailValid(email)) {

            vEmailET.setError(getString(R.string.error_invalid_email));
            focusView = vEmailET;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            vPasswordET.setError(getString(R.string.error_field_required));
            focusView = vPasswordET;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(FacebookSdk.getCallbackRequestCodeOffset() == requestCode){
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            int statusCode = result.getStatus().getStatusCode();

            handleSignInResult(task);
        }
    }

    private void signIn() {

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {

            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            String idToken = account.getIdToken();

            mGoogleLoginAT = new GoogleLoginAT(account.getIdToken());
            mGoogleLoginAT.execute();
            //updateUI(account);

        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            vProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            vProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    vProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            vProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

        Log.i(TAG, "AAAAAAAAAAAAA");
        if(view == vCreateAccountTV){
            Intent intent = new Intent(this, EmailRegisterActivity.class);
            startActivity(intent);

        }else if(view == vEmailSignInBT){
            attemptLogin();
        }else if(view.getId() == R.id.activityLogin_googleSignInBT){

            if(mGoogleLoginAT != null){
                return;
            }
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    public void onLoginSuccess(JSONObject loginInformation) throws JSONException {
        SharedPreferencesUtils .getInstance(this).put(SharedPreferencesUtils.PrefKeys.token, loginInformation.getString("token"));
        Intent intent = new Intent(this, TransactionLoginScreen.class);
        startActivity(intent);
        finish();
    }

    public void errorToast(String Message) {
        Toast.makeText(getBaseContext(), "Login failed : " + Message, Toast.LENGTH_LONG).show();
    }

    public class UserLoginTask extends AsyncTask<Void, Void, JSONObject> {

        private final String aEmail;
        private final String aPassword;

        UserLoginTask(String email, String password) {
            aEmail = email;
            aPassword = password;
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            return new ServerAPI(getBaseContext()).serverRequestPOST(new Email_login(aEmail, aPassword));
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case Email_login.Response.match:
                            onLoginSuccess(jsonObject);
                        break;
                    case Email_login.Response.noMatch:
                        errorToast("Password and Email Doesn't match");
                        break;
                    default:
                        Log.e(TAG, "Error Default : 0");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mAuthTask = null;
            showProgress(false);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }


    class FacebookLoginAT extends AsyncTask<Void, Void, JSONObject> {

        String aUserId;
        String aToken;

        FacebookLoginAT(String userId, String token){
            aUserId = userId;
            aToken = token;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = new ServerAPI(getBaseContext()).serverRequestPOST(new Facebook_login(aUserId, aToken));
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case Facebook_login.Response.foundUser:
                    case Facebook_login.Response.synced:
                    case Facebook_login.Response.created:
                        onLoginSuccess(jsonObject);
                        break;
                    default:
                        Log.e(TAG, "Error Default : FACEBOOK LOGIN ERROR");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mFacebookLoginAT = null;
        }
    }


    class GoogleLoginAT extends AsyncTask<Void, Void, JSONObject>{

        String aId_token;

        public GoogleLoginAT(String id_token){

            aId_token = id_token;

        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = new ServerAPI(getBaseContext()).serverRequestPOST(new Google_login(aId_token));

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case Facebook_login.Response.foundUser:
                    case Facebook_login.Response.synced:
                    case Facebook_login.Response.created:
                        onLoginSuccess(jsonObject);
                        break;
                    default:
                        Log.e(TAG, "Error Default : FACEBOOK LOGIN ERROR");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mGoogleLoginAT = null;
        }
    }
}

