package com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;

public class FilterWantedATL extends AsyncTaskLoader<List<CardInfo>> {
    private static final String TAG = "##FilterCardATL";

    String mToken;

    public FilterWantedATL(Context context, String token) {
        super(context);
        mToken = token;
    }

    @Override
    public List<CardInfo> loadInBackground() {

        List<CardInfo> temp = Motherbase.getInstance(getContext()).getCardInfoTable().selectByNameTokenWanted(mToken);
        return temp;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
