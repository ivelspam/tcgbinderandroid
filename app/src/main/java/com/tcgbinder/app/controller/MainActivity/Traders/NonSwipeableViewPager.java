package com.tcgbinder.app.controller.MainActivity.Traders;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;


public class NonSwipeableViewPager extends ViewPager {
    private static final String TAG = "##NonSwipeableViewPager";

    public NonSwipeableViewPager(Context context) {
        super(context);
//        setMyScroller();
    }

    public NonSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
//        setMyScroller();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }

}