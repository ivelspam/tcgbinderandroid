package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.Constants.CalendarFormats;
import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Objects.ChatMessage;
import com.tcgbinder.app.model.TokenUtilities;

import java.util.ArrayList;
import java.util.List;

public class TraderChatFragment extends Fragment implements View.OnClickListener{
    private final String TAG = "##" + this.getClass().getSimpleName();

    private final int LOADER_SQLITE_CHAT_MESSAGES = 0;

    RecyclerView vChatMessagesRV;
    EditText vMessageET;
    Button vSendBT;

    ChatMessagesAA mChatMessagesAA = new ChatMessagesAA();

    private TradeActivityInterface mTradeActivityInterface;

    public TraderChatFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trader_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    private void build_view(){
        vSendBT = getView().findViewById(R.id.fragmentTraderChat_sendBT);
        vMessageET = getView().findViewById(R.id.fragmentTraderChat_messageET);
        vChatMessagesRV = getView().findViewById(R.id.fragmentTraderChat_messageRV);

        //ADD LISTENRES
        vSendBT.setOnClickListener(this);

        //ADD ADAPTERS
        vChatMessagesRV.setAdapter(mChatMessagesAA);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        vChatMessagesRV.setLayoutManager(linearLayoutManager);
    }

    public void updateValues(){
        if(mTradeActivityInterface == null || mTradeActivityInterface.getTradesActivityValues() == null){
            return;
        }
        mChatMessagesAA.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TradeActivityInterface) {
            mTradeActivityInterface = (TradeActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTradeActivityInterface = null;
    }


    //LISTENERS
    @Override
    public void onClick(View view) {
        if(view == vSendBT){

            if(TextUtils.isEmpty(vMessageET.getText().toString())){

            }else{
                mTradeActivityInterface.sendMessage(vMessageET.getText().toString());
                vMessageET.setText("");
            }
        }
    }

    //Add Message

    public void receiveMessage(){
        mChatMessagesAA.notifyItemInserted(mTradeActivityInterface.getTradesActivityValues().getChatMessages().size() - 1);
        vChatMessagesRV.scrollToPosition(mTradeActivityInterface.getTradesActivityValues().getChatMessages().size() - 1);
    }

    //ADAPTERS
    class ChatMessagesAA extends RecyclerView.Adapter {

        private static final int VIEW_TYPE_MESSAGE_SENT = 1;
        private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;


        @Override
        public int getItemViewType(int position) {
            ChatMessage chatMessage = mTradeActivityInterface.getTradesActivityValues().getChatMessages().get(position);

            String user_id = TokenUtilities.get_id(getContext());
            if(user_id.equals(chatMessage.getSender_id())){
                return VIEW_TYPE_MESSAGE_SENT;
            }else{
                return VIEW_TYPE_MESSAGE_RECEIVED;
            }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            if(viewType == VIEW_TYPE_MESSAGE_SENT){
                view = LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.adapter_row_message_sent,
                        parent,
                        false
                );
                return new SentMessageHolder(view);
            }else if(viewType == VIEW_TYPE_MESSAGE_RECEIVED){
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.adapter_row_message_received,
                        parent,
                        false
                );
                return new ReceivedMessageHolder(view);
            }
            return  null;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ChatMessage chatMessage = mTradeActivityInterface.getTradesActivityValues().getChatMessages().get(position);
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((SentMessageHolder) holder).bind(chatMessage);
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageHolder) holder).bind(chatMessage);
            }

        }

        @Override
        public int getItemCount() {
            if(mTradeActivityInterface.getTradesActivityValues() == null){
                return 0;
            }
            return mTradeActivityInterface.getTradesActivityValues().getChatMessages().size();
        }

        class ReceivedMessageHolder extends RecyclerView.ViewHolder {
            TextView vNameTV, vContentTV, vTimeTV;

            public ReceivedMessageHolder(View itemView) {
                super(itemView);
                vNameTV = itemView.findViewById(R.id.adapterRowMessageReceived_nameTV);
                vContentTV = itemView.findViewById(R.id.adapterRowMessageReceived_contentTV);
                vTimeTV = itemView.findViewById(R.id.adapterRowMessageReceived_timeTV);
            }

            void bind(ChatMessage chatMessage){
                vNameTV.setText(mTradeActivityInterface.getTraderName());
                vContentTV.setText(chatMessage.getContent());
                vTimeTV.setText(DateExtension.CalendarGetFormated(chatMessage.getCreatedAt(), CalendarFormats.dayhour));
            }
        }

        class SentMessageHolder extends RecyclerView.ViewHolder {
            TextView vContentTV;
            public SentMessageHolder(View itemView) {
                super(itemView);
                vContentTV = itemView.findViewById(R.id.adapterRowMessageSent_contentTV);
            }

            void bind(ChatMessage chatMessage){
                vContentTV.setText(chatMessage.getContent());
            }
        }
    }


}
