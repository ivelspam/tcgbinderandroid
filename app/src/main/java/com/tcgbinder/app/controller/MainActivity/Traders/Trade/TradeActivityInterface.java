package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import com.tcgbinder.app.model.Objects.CartCard;
import com.tcgbinder.app.model.Objects.ChatMessage;

import org.json.JSONObject;

public interface TradeActivityInterface {

    String getTrader_id();
    String getTraderName();
    void sendMessage(String content);
    void receivedMessage(ChatMessage chatMessage);
    void addCartToCard(String card_id, Integer nonfoilQuantity, Integer foilQuantity);
    void sendUpdateCartCard(CartCard cartCard, String updateType);
    void receiveUpdateCartCard(JSONObject jsonObject);
    void removeCartCard(CartCard cartCard, String updateType);
    void receivedRemoveCartCard(JSONObject jsonObject);
    void updateEverything();
    TradersActivityValues getTradesActivityValues();
    void updateCart();
    void addMessageToChat();
    void socketReceivedClearCart();
    void socketSendClearCart();


}
