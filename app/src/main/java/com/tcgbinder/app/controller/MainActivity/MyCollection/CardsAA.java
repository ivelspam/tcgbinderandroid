package com.tcgbinder.app.controller.MainActivity.MyCollection;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.MyCollection.AddDialog.AddCardDialogInterface;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Extensions.DateExtension;
import com.tcgbinder.app.model.Objects.CardInfo;
import com.tcgbinder.app.model.Objects.WantedCard;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.CardsInfo_getPrice;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_addUpdateCards;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_addUpdateCardsToWanted;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_deleteWantedCard;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_deleteWantedGeneralCard;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_deleteCard;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.UserCards_addUpdateCardsToWantedGeneral;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CardsAA extends RecyclerView.Adapter {

    private static final String TAG = "##CardsAA";
    List<CardInfo> mCardsInfo = new ArrayList<>();
    List<WantedCard> mWantedCards = new ArrayList<>();
    Context mContext;
    DecimalFormat df = new DecimalFormat("0.00");

    OptionEnum mOptionEnum;
    AddCardDialogInterface mAddCardDialogInterface;

    public enum OptionEnum {
        all("all"),
        searchAll("searchAll"),
        wanted("wanted"),
        searchWanted("searchWanted"),
        wantedGeneral("wantedGeneral"),
        searchWantedGeneral("searchWantedGeneral");

        private final String text;
        private static Map map = new HashMap<>();

        public static OptionEnum valueOf(int pageType) {
            return (OptionEnum) map.get(pageType);
        }

        OptionEnum(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public CardsAA(Context context, OptionEnum optionEnum, AddCardDialogInterface addCardDialogInterface){
        mContext = context;
        mOptionEnum = optionEnum;
        mAddCardDialogInterface = addCardDialogInterface;
    }

    public CardsAA(Context context, OptionEnum optionEnum){
        mContext = context;
        mOptionEnum = optionEnum;

    }

    public void setmOptionEnum(OptionEnum mOptionEnum) {
        this.mOptionEnum = mOptionEnum;
    }

    public List<CardInfo> getmCardsInfo() {
        return mCardsInfo;
    }

    public List<WantedCard> getmWantedCards() {
        return mWantedCards;
    }

    public CardsAA(Context context, List<CardInfo> cardInfos, OptionEnum optionEnum){
        mContext = context;
        mOptionEnum = optionEnum;
        mCardsInfo = cardInfos;
    }

    public void updateListAA(List<CardInfo> cardInfos){
        mCardsInfo = cardInfos;
        notifyDataSetChanged();
    }

    public void updateWantedListAA(List<WantedCard> wantedCards){
        mWantedCards = wantedCards;
        notifyDataSetChanged();
    }

    public void clearCardsAA(){
        mCardsInfo.clear();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filtered_cards, parent, false);
        switch (mOptionEnum){

            case wantedGeneral:
            case searchWantedGeneral:
                return new WantedCardHolder(view);
            default:
                return new CardInfoHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (mOptionEnum){

            case wantedGeneral:
            case searchWantedGeneral:
                return 0;
            default:
                return 1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (mOptionEnum){

            case wantedGeneral:
            case searchWantedGeneral:
                ((WantedCardHolder) holder).bind(mWantedCards.get(position));
                break;
            default:
                ((CardInfoHolder) holder).bind(mCardsInfo.get(position));
        }
    }

    @Override
    public int getItemCount() {
        switch (mOptionEnum){

            case wantedGeneral:
            case searchWantedGeneral:
                return mWantedCards.size();
            default:
                return mCardsInfo.size();
        }
    }

    private class CardInfoHolder extends RecyclerView.ViewHolder {


        TextView nameTV, priceTV, setNameTV;
        EditText nonfoilQuantityET, foilQuantityET;
        ImageView setIV;
        Button removeBT, setBT;
        CardInfo mCardInfo;


        public CardInfoHolder(View itemView) {
            super(itemView);
            setIV = itemView.findViewById(R.id.adapterFilteredCards_setCodeIV);
            nameTV = itemView.findViewById(R.id.adapterFilteredCards_nameTV);
            nonfoilQuantityET = itemView.findViewById(R.id.adapterFilteredCards_nonfoilQuantityET);
            foilQuantityET = itemView.findViewById(R.id.adapterFilteredCards_foilQuantityET);
            priceTV = itemView.findViewById(R.id.adapterFilteredCards_nonFoilPriceTV);
            removeBT = itemView.findViewById(R.id.adapterFilteredCards_removeBT);
            setBT = itemView.findViewById(R.id.adapterFilteredCards_setBT);
            setNameTV = itemView.findViewById(R.id.adapterFilteredCards_setNameTV);
        }

        public void bind(final CardInfo cardInfo) {

            //initial state
            mCardInfo = cardInfo;
            nameTV.setText(cardInfo.getName());
            setNameTV.setText(cardInfo.getSet_name());
            setBT.setVisibility(View.GONE);

            Resources resources = mContext.getResources();
            String imageIndentifier = "s"+cardInfo.getSet_code() + "_svg";
            int add = resources.getIdentifier(imageIndentifier, "drawable", mContext.getPackageName());
            setIV.setImageResource(add);

            nonfoilQuantityET.removeTextChangedListener(textWatcher);
            foilQuantityET.removeTextChangedListener(textWatcher);

            nonfoilQuantityET.setText(Integer.toString(cardInfo.getNonfoilQuantity()), TextView.BufferType.EDITABLE);
            foilQuantityET.setText(Integer.toString(cardInfo.getFoilQuantity()), TextView.BufferType.EDITABLE);

            switch (mOptionEnum){
                case wanted:

                    break;
                case all:
                    break;
                case searchAll:
                case searchWanted:
                case searchWantedGeneral:
                    removeBT.setVisibility(View.GONE);
                    break;
            }


            nonfoilQuantityET.addTextChangedListener(textWatcher);
            foilQuantityET.addTextChangedListener(textWatcher);


            setBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBT.setVisibility(View.GONE);
                    int tempNonfoil = 0;
                    int tempFoil = 0;
                    if(nonfoilQuantityET.length() > 0){
                        tempNonfoil = Integer.parseInt(nonfoilQuantityET.getText().toString());
                    }
                    if(foilQuantityET.length() > 0){
                        tempFoil = Integer.parseInt(foilQuantityET.getText().toString());
                    }
                    UpdateCardsValues updateCardsValues = new UpdateCardsValues(mCardInfo.get_id(), tempFoil, tempNonfoil);
                    updateCardsValues.execute();
                }
            });

            removeBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new RemoveCardAT(cardInfo.get_id()).execute();
                }
            });

            priceTV.setText("N/A");

            if(DateExtension.CalendarOlderThanOneDay(cardInfo.getPriceUpdatedAt())){
                new GetCardPrice(cardInfo.get_id(), priceTV).execute();
            }else{
                if(cardInfo.getUsd() != null){
                    priceTV.setText(df.format(cardInfo.getUsd()));
                }
            }
        }

        class GetCardPrice extends AsyncTask<Void, Void, JSONObject> {

            String aCard_id;
            WeakReference<TextView> aPriceWR;

            public GetCardPrice(String card_id, TextView textView){
                aCard_id = card_id;
                aPriceWR = new WeakReference<>(textView);
            }

            @Override
            protected JSONObject doInBackground(Void... voids) {
                return new ServerAPI(mContext).serverRequestGET(new CardsInfo_getPrice(aCard_id));
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    switch (jsonObject.getString(APIRequest.message)){
                        case CardsInfo_getPrice.Response.found:

                            if(aPriceWR.get()!= null){
                                JSONObject priceJSONObject = jsonObject.getJSONObject(CardsInfo_getPrice.ResponseFields.price);
                                Motherbase.getInstance(mContext).getCardInfoTable().updatePriceFromJSON(priceJSONObject);

                                if(mCardInfo.get_id().equals(aCard_id)){
                                    if(!priceJSONObject.isNull(MongoKeys.usd)){
                                        aPriceWR.get().setText(df.format(priceJSONObject.getDouble(MongoKeys.usd)));
                                    }else{
                                        aPriceWR.get().setText("N/A");
                                    }
                                }
                            }
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setBT.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private class WantedCardHolder extends RecyclerView.ViewHolder {

        TextView nameTV, priceTV, setNameTV;
        EditText nonfoilQuantityET, foilQuantityET;
        ImageView setIV;
        Button removeBT, setBT;
        String card_id;

        public WantedCardHolder(View itemView) {
            super(itemView);
            setIV = itemView.findViewById(R.id.adapterFilteredCards_setCodeIV);
            nameTV = itemView.findViewById(R.id.adapterFilteredCards_nameTV);
            nonfoilQuantityET = itemView.findViewById(R.id.adapterFilteredCards_nonfoilQuantityET);
            foilQuantityET = itemView.findViewById(R.id.adapterFilteredCards_foilQuantityET);
            priceTV = itemView.findViewById(R.id.adapterFilteredCards_nonFoilPriceTV);
            removeBT = itemView.findViewById(R.id.adapterFilteredCards_removeBT);
            setBT = itemView.findViewById(R.id.adapterFilteredCards_setBT);
            setNameTV = itemView.findViewById(R.id.adapterFilteredCards_setNameTV);
        }

        public void bind(final WantedCard wantedCard) {

            setIV.setVisibility(View.GONE);
            setNameTV.setVisibility(View.GONE);
            priceTV.setVisibility(View.GONE);
            setBT.setVisibility(View.GONE);


            switch (mOptionEnum){
                case searchWantedGeneral:
                    removeBT.setVisibility(View.GONE);
                    break;
            }


            //remove listeners
            nonfoilQuantityET.removeTextChangedListener(textWatcher);
            foilQuantityET.removeTextChangedListener(textWatcher);

            //initial state
            nameTV.setText(wantedCard.getName());
            nonfoilQuantityET.setText(Integer.toString(wantedCard.getNonfoilQuantity()), TextView.BufferType.EDITABLE);
            foilQuantityET.setText(Integer.toString(wantedCard.getFoilQuantity()), TextView.BufferType.EDITABLE);

            removeBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new RemoveCardAT(wantedCard.getName()).execute();
                }
            });

            setBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBT.setVisibility(View.GONE);
                    int tempNonfoil = 0;
                    int tempFoil = 0;
                    if(nonfoilQuantityET.length() > 0){
                        tempNonfoil = Integer.parseInt(nonfoilQuantityET.getText().toString());
                    }
                    if(foilQuantityET.length() > 0){
                        tempFoil = Integer.parseInt(foilQuantityET.getText().toString());
                    }
                    UpdateCardsValues updateCardsValues = new UpdateCardsValues(wantedCard.getName(), tempFoil, tempNonfoil);
                    updateCardsValues.execute();
                }
            });

            nonfoilQuantityET.addTextChangedListener(textWatcher);
            foilQuantityET.addTextChangedListener(textWatcher);

        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setBT.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        };

    }

    class UpdateCardsValues extends AsyncTask<Void, Void, JSONObject> {

        String aCard_id;
        int aFoilQuantity, aNonfoilQuantity;

        public UpdateCardsValues(String card_id, int foilQuantity, int nonFoilQuantity){
            aCard_id = card_id;
            aFoilQuantity = foilQuantity;
            aNonfoilQuantity = nonFoilQuantity;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {

            JSONObject jsonObject = null;
            switch (mOptionEnum){
                case all:
                case searchAll:
                    jsonObject = new ServerAPI(mContext).serverRequestPOST(new UserCards_addUpdateCards(aCard_id, aFoilQuantity, aNonfoilQuantity));
                    break;
                case wanted:
                case searchWanted:
                    jsonObject = new ServerAPI(mContext).serverRequestPOST(new UserCards_addUpdateCardsToWanted(aCard_id, aFoilQuantity, aNonfoilQuantity));
                    break;
                case wantedGeneral:
                case searchWantedGeneral:
                    jsonObject =  new ServerAPI(mContext).serverRequestPOST(new UserCards_addUpdateCardsToWantedGeneral(aCard_id, aFoilQuantity, aNonfoilQuantity));
            }

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case UserCards_addUpdateCards.Response.added:
                    case UserCards_addUpdateCards.Response.updated:
                        Motherbase.getInstance(mContext).getMyCardsTable().insertOrReplaceCards("", aCard_id, aFoilQuantity, aNonfoilQuantity);
                        break;
                    case UserCards_addUpdateCardsToWanted.Response.addedWanted:
                    case UserCards_addUpdateCardsToWanted.Response.updatedWanted:
                        Motherbase.getInstance(mContext).getMyWantedCardsTable().insertOrReplaceCards(aCard_id, aFoilQuantity, aNonfoilQuantity);

                        break;
                    case UserCards_addUpdateCardsToWantedGeneral.Response.updatedWantedGeneral:
                        Motherbase.getInstance(mContext).getMyWantedGeneralTable().insertOrReplaceCards(aCard_id, aFoilQuantity, aNonfoilQuantity);
                        break;
                    default:
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(mAddCardDialogInterface != null){
                mAddCardDialogInterface.changeList();
            }
        }
    }


    class RemoveCardAT extends AsyncTask<Void, Void, JSONObject> {

        String aCard_id;

        public RemoveCardAT(String card_id){
            aCard_id = card_id;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = null;
            switch (mOptionEnum){
                case all:
                case searchAll:
                    jsonObject = new ServerAPI(mContext).serverRequestPOST(new UserCards_deleteCard(aCard_id));
                    break;
                case wanted:
                case searchWanted:
                    jsonObject = new ServerAPI(mContext).serverRequestPOST(new UserCards_deleteWantedCard(aCard_id));
                    break;
                case wantedGeneral:
                case searchWantedGeneral:
                    jsonObject = new ServerAPI(mContext).serverRequestPOST(new UserCards_deleteWantedGeneralCard(aCard_id));
            }

            try {
                switch (jsonObject.getString(APIRequest.message)){
                    case UserCards_deleteCard.Response.deleted:
                        Motherbase.getInstance(mContext).getMyCardsTable().removeCard(aCard_id);
                        break;
                    case UserCards_deleteWantedCard.Response.wantedDeleted:
                        Motherbase.getInstance(mContext).getMyWantedCardsTable().removeCard(aCard_id);
                        break;
                    case UserCards_deleteWantedGeneralCard.Response.wantedGeneralDeleted:
                        Motherbase.getInstance(mContext).getMyWantedGeneralTable().removeCard(aCard_id);
                        break;
                    default:
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            if(mAddCardDialogInterface != null){
                mAddCardDialogInterface.changeList();
            }

        }
    }

}
