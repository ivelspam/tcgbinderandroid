package com.tcgbinder.app.controller.MainActivity.MyCollection;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.tcgbinder.app.model.Objects.MyBinder;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;

import java.util.List;

public class GetBindersATL extends AsyncTaskLoader<List<MyBinder>> {

    public GetBindersATL(@NonNull Context context) {
        super(context);
    }

    @Nullable
    @Override
    public List<MyBinder> loadInBackground() {
        return Motherbase.getInstance(getContext()).getMyBindersTable().selectAllBinders();
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
