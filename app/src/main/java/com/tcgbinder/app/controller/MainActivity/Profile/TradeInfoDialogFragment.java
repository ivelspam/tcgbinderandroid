package com.tcgbinder.app.controller.MainActivity.Profile;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.tcgbinder.app.R;
import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.APIRequest;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.User_updateTradeInfo;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class TradeInfoDialogFragment extends DialogFragment implements View.OnClickListener {
    private static final String TAG = "##TradeInfoDialogFragme";


    EditText vTradeInfoET;
    TextInputLayout vHintTIL;
    Button vUpdateBT;
    UpdateTradeInfoAT mUpdateTradeInfoAT;
    private OnFragmentInteractionListener mListener;


    public TradeInfoDialogFragment() {}

    public static TradeInfoDialogFragment newInstance(String param1, String param2) {
        TradeInfoDialogFragment fragment = new TradeInfoDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createView();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_info_dialog, container, false);
    }

    void createView(){
        vTradeInfoET = getView().findViewById(R.id.fragmentTradeInfoDialog_tradeInfoET);
        vUpdateBT = getView().findViewById(R.id.fragmentTradeInfoDialog_updateBT);
        vHintTIL = getView().findViewById(R.id.fragmentTradeInfoDialog_tradeInfoTIL);

        vTradeInfoET.setText(SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.tradeInfo));

        vUpdateBT.setOnClickListener(this);
        setHintSize();

        vTradeInfoET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                setHintSize();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    void setHintSize(){
        vHintTIL.setHint("Trade Info: Max 100 Characters (" + vTradeInfoET.length() + ")");
    }

    private void checkIfTradeInfoIsValid() {

        if (mUpdateTradeInfoAT != null) {
            return;
        }

        vTradeInfoET.setError(null);

        String tradeInfo = vTradeInfoET.getText().toString();

        boolean cancel = false;



        if (tradeInfo.length() > 140) {

            vTradeInfoET.setError(getString(R.string.error_trade_info_too_long));
            cancel = true;
        }

        if (cancel) {

        } else {
            mUpdateTradeInfoAT = new UpdateTradeInfoAT(tradeInfo);
            mUpdateTradeInfoAT.execute();
        }
    }


    public void onResume() {
        super.onResume();

        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;
        int height = size.y;

        window.setLayout((int) (width * 1.0), (int) (height * 0.50));
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

        checkIfTradeInfoIsValid();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class UpdateTradeInfoAT extends AsyncTask<Void, Void, JSONObject>{

        String aTradeInfo;

        public UpdateTradeInfoAT(String tradeInfo){
            aTradeInfo = tradeInfo;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {

            JSONObject jsonObject = new ServerAPI(getContext()).serverRequestPOST(new User_updateTradeInfo(aTradeInfo));

            try {

                switch (jsonObject.getString(APIRequest.message)){
                    case User_updateTradeInfo.Response.updated :
                        SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.tradeInfo, aTradeInfo);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {


            mUpdateTradeInfoAT = null;
        }
    }


}
