package com.tcgbinder.app.controller.MainActivity.Traders.Trade;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Constants.SocketIOConnections;
import com.tcgbinder.app.model.Objects.CartCard;
import com.tcgbinder.app.model.Objects.ChatMessage;
import com.tcgbinder.app.model.SQLiteTable.Motherbase;
import com.tcgbinder.app.model.SQLiteTable.SQLiteTables.ChatMessageTable;
import com.tcgbinder.app.model.ServerRequest.SharedPreferencesUtils;
import com.tcgbinder.app.model.TokenUtilities;
//import com.github.nkzawa.emitter.Emitter;
//import com.github.nkzawa.socketio.client.Ack;
//import com.github.nkzawa.socketio.client.Socket;
//import com.github.nkzawa.socketio.client.IO;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class TraderSocket{
    private static final String TAG = "##TraderSocket";

    Socket mSocket;
    String mChatRoomId, mCartRoomId, mTraderId;
    Context mContext;
    TradeActivityInterface mTradeActivityInterface;
    ChatMessageTable mChatMessageTable;
    Activity mActivity;


    public TraderSocket(Context context, Activity activity,  String trader_id, TradeActivityInterface tradeActivityInterface){
        mTraderId = trader_id;
        mContext = context;
        mActivity = activity;
        mChatMessageTable = Motherbase.getInstance(context).getChatMessageTable();
        mTradeActivityInterface = tradeActivityInterface;

        IO.Options options = new IO.Options();
        options.forceNew = true;
        options.query = "token=" + SharedPreferencesUtils.getInstance(mContext).getString(SharedPreferencesUtils.PrefKeys.token);

        try {
            mSocket = IO.socket("https://tcgbinder.com:3000", options);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        mChatRoomId = SocketIOConnections.makeChatRoomId(mContext, trader_id);
        mCartRoomId = SocketIOConnections.makeCartRoom(mContext, trader_id);

        mSocket.on("connect", handleConnection);

        mSocket.on(mChatRoomId, handleIncomeMessages);
        mSocket.on(mCartRoomId, handleIncomeTrades);

        mSocket.connect();
    }

    public void sendMessage(String content, String receiver_id){

        TokenUtilities.get_id(mContext);
        ChatMessage chatMessage = new ChatMessage( content, receiver_id, TokenUtilities.get_id(mContext), 0);
        mTradeActivityInterface.receivedMessage(chatMessage);

        Motherbase.getInstance(mContext).getChatMessageTable().insertOrReplaceChatMessage(chatMessage);
        mSocket.emit(SocketIOConnections.sendChatMessage, chatMessage.getJSONString(), new Ack() {
            @Override
            public void call(Object... args) {
                switch (args[0].toString()){
                    case "serverReceived" :
                        Motherbase.getInstance(mContext).getChatMessageTable().insertOrReplaceChatMessagesFromMySQL((JSONObject)args[1]);
                        break;
                }
            }


        });
    }

    public void addCardToCard(String card_id, String trader_id, Integer nonfoilQuantity, Integer foilQuantity ){

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MongoKeys.card_id, card_id);
            jsonObject.put(MongoKeys.trader_id, trader_id);
            jsonObject.put(MongoKeys.nonfoilQuantity, nonfoilQuantity);
            jsonObject.put(MongoKeys.foilQuantity, foilQuantity);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketIOConnections.addWantCartCard, jsonObject.toString(), new Ack() {
            @Override
            public void call(Object... args) {
                switch (args[0].toString()){
                    case "added" :
                        mTradeActivityInterface.receiveUpdateCartCard((JSONObject) args[1]);
                        break;
                }
            }
        });
    }

    public void updateGivingCartCard(CartCard cartCard, String updateType){
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cartCard", cartCard.getCardCardInJSONObject());
            jsonObject.put("updateType", updateType);
            jsonObject.put(MongoKeys.trader_id, mTraderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketIOConnections.updateCartCard, jsonObject, new Ack() {
            @Override
            public void call(Object... args) {

                JSONObject jsonObject1 = (JSONObject) args[0];

//                switch (args[0].toString()){
//
//                    case "serverReceived" :
//                        Motherbase.getInstance(mContext).getChatMessageTable().insertOrReplaceChatMessagesFromMySQL((JSONObject)args[0]);
//                        break;
//
//                }
            }


        });

    }

    public void removeCartCard(CartCard cartCard, String updateType){

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cartCard", cartCard.getCardCardInJSONObject());
            jsonObject.put("updateType", updateType);
            jsonObject.put(MongoKeys.trader_id, mTraderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketIOConnections.removeCartCard, jsonObject, new Ack() {
            @Override
            public void call(Object... args) {
                switch (args[0].toString()){
                    case "deleted" :
                        mTradeActivityInterface.receivedRemoveCartCard((JSONObject) args[1]);
                        break;
                }
            }


        });

    }

    public void clearCart(){
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MongoKeys.trader_id, mTraderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketIOConnections.clearCart, jsonObject, new Ack() {
            @Override
            public void call(Object... args) {
//                JSONObject jsonObject1 = (JSONObject) args[0];

                switch (args[0].toString()){
                    case "cleared" :
                        mTradeActivityInterface.socketReceivedClearCart();
                        break;
                }
            }


        });

    }

    public void disconnect(){
        mSocket.disconnect();
    }

    private Emitter.Listener handleIncomeMessages = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        if (mChatMessageTable.selectAllChatsByUUID(jsonObject.getString(MongoKeys.aUUID))){
                            return;
                        }
                        mChatMessageTable.insertOrReplaceChatMessagesFromMySQL(jsonObject);
                        ChatMessage chatMessage = new ChatMessage(jsonObject);
                        mTradeActivityInterface.receivedMessage(chatMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener handleIncomeTrades = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {



            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    switch ((String) args[0]) {
                        case "deleted":
                            mTradeActivityInterface.receivedRemoveCartCard((JSONObject) args[1]);
                            break;
                        case "cleared":
                            mTradeActivityInterface.socketReceivedClearCart();
                            break;
                        case "updated":
                        case "added":
                            mTradeActivityInterface.receiveUpdateCartCard((JSONObject) args[1]);
                        default:
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener handleConnection = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for(ChatMessage chatMessage :  Motherbase.getInstance(mContext).getChatMessageTable().selectAllChatByState(0)){
                        sendMessageQueueMEssage(chatMessage);
                    }
                }
            });
        }
    };

    private Emitter.Listener receiveClearCart = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };


    public void sendMessageQueueMEssage(ChatMessage chatMessage){

        mSocket.emit(SocketIOConnections.sendChatMessage, chatMessage.getJSONString(), new Ack() {
            @Override
            public void call(Object... args) {
                switch (args[0].toString()){
                    case "serverReceived" :
                        Motherbase.getInstance(mContext).getChatMessageTable().insertOrReplaceChatMessagesFromMySQL((JSONObject)args[1]);
                        break;
                }
            }
        });
    }

    class getSocketInfo extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return null;
        }
    }

}
