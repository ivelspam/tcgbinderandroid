package com.tcgbinder.app.controller.MainActivity.Traders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcgbinder.app.R;
import com.tcgbinder.app.controller.MainActivity.Traders.Trade.TradeActivity;
import com.tcgbinder.app.model.Constants.MongoKeys;
import com.tcgbinder.app.model.Objects.OpenCart;

public class OpenCartsFragment extends Fragment{
    private static final String TAG = "##OpenCartsFragment";

    OpenCartsAA mOpenCartsAA = new OpenCartsAA();

    RecyclerView vOpenCartsRV;
    TradersFragmentInterface mTradersFragmentInterface;

    public OpenCartsFragment() {

    }

    public static OpenCartsFragment newInstance(String param1, String param2) {
        OpenCartsFragment fragment = new OpenCartsFragment();
        return fragment;
    }

    public void insertInterface(TradersFragmentInterface tradersFragmentInterface){
        mTradersFragmentInterface = tradersFragmentInterface;
    }

    public void updateValues(){
        mOpenCartsAA.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_open_carts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    void build_view(){
        vOpenCartsRV = getView().findViewById(R.id.fragmentOpenCards_openCartsRV);

        vOpenCartsRV.setAdapter(mOpenCartsAA);
        vOpenCartsRV.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    class OpenCartsAA extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_open_cart, parent, false);
            return new traderSimpleHolder(view);
        }

        @Override
        public void onBindViewHolder( RecyclerView.ViewHolder holder, int position) {
            ((traderSimpleHolder) holder).bind(mTradersFragmentInterface.getOpenCarts().get(position));

        }

        @Override
        public int getItemCount() {
            return mTradersFragmentInterface.getOpenCarts().size();
        }

        private class traderSimpleHolder extends RecyclerView .ViewHolder {

            TextView nameTV, givingTV, receivingTV;
            ConstraintLayout parentCL;

            public traderSimpleHolder(View itemView) {
                super(itemView);
                nameTV = itemView.findViewById(R.id.adapterRowOpenCart_nameTV);
                givingTV = itemView.findViewById(R.id.adapterRowOpenCart_givingTV);
                receivingTV = itemView.findViewById(R.id.adapterRowOpenCart_receivingTV);
                parentCL = itemView.findViewById(R.id.adapterRowOpenCart_parentCL);
            }

            public void bind(final OpenCart openCart) {
                nameTV.setText(openCart.getName());
                givingTV.setText("Giving Cards: " + openCart.getGiving());
                receivingTV.setText("Receiving Cards : " + openCart.getReceiving());

                parentCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    Intent  intent = new Intent(getContext(), TradeActivity.class);
                    intent.putExtra(MongoKeys._id, openCart.get_id());
                    intent.putExtra(MongoKeys.name, openCart.getName());
                    startActivityForResult(intent, 0);
                    }
                });
            }
        }
    }
}
