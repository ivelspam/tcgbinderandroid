package com.tcgbinder.app.controller.MainActivity.Traders.Trade;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.tcgbinder.app.model.ServerRequest.ServerAPI;
import com.tcgbinder.app.model.ServerRequest.ServerCalls.Traders_getAllInfo;

import org.json.JSONObject;

public class GetAllInfoATL extends AsyncTaskLoader<TradersActivityValues> {
    private final String TAG = "##" + this.getClass().getSimpleName();

    TradersActivityValues mTradersActivityValues;

    public GetAllInfoATL(Context context, TradersActivityValues tradersActivityValues) {
        super(context);
        mTradersActivityValues = tradersActivityValues;
    }

    @Override
    public TradersActivityValues loadInBackground() {
        JSONObject jsonObject = new ServerAPI(getContext()).serverRequestPOST(new Traders_getAllInfo(getContext(), mTradersActivityValues.getmTrader_id()));
        mTradersActivityValues.putValues(jsonObject);
        return mTradersActivityValues;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
